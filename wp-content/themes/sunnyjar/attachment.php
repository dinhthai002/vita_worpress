<?php
/**
 * Attachment page
 */
get_header(); 

while ( have_posts() ) { the_post();

	// Move sunnyjar_set_post_views to the javascript - counter will work under cache system
	if (sunnyjar_get_custom_option('use_ajax_views_counter')=='no') {
		sunnyjar_set_post_views(get_the_ID());
	}

	sunnyjar_show_post_layout(
		array(
			'layout' => 'attachment',
			'sidebar' => !sunnyjar_param_is_off(sunnyjar_get_custom_option('show_sidebar_main'))
		)
	);

}

get_footer();
?>