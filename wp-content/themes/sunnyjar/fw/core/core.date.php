<?php
/**
 * Sunnyjar Framework: date and time manipulations
 *
 * @package	sunnyjar
 * @since	sunnyjar 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Convert date from MySQL format (YYYY-mm-dd) to Date (dd.mm.YYYY)
if (!function_exists('sunnyjar_sql_to_date')) {
	function sunnyjar_sql_to_date($str) {
		return (trim($str)=='' || trim($str)=='0000-00-00' ? '' : trim(sunnyjar_substr($str,8,2).'.'.sunnyjar_substr($str,5,2).'.'.sunnyjar_substr($str,0,4).' '.sunnyjar_substr($str,11)));
	}
}

// Convert date from Date format (dd.mm.YYYY) to MySQL format (YYYY-mm-dd)
if (!function_exists('sunnyjar_date_to_sql')) {
	function sunnyjar_date_to_sql($str) {
		if (trim($str)=='') return '';
		$str = strtr(trim($str),'/\-,','....');
		if (trim($str)=='00.00.0000' || trim($str)=='00.00.00') return '';
		$pos = sunnyjar_strpos($str,'.');
		$d=trim(sunnyjar_substr($str,0,$pos));
		$str=sunnyjar_substr($str,$pos+1);
		$pos = sunnyjar_strpos($str,'.');
		$m=trim(sunnyjar_substr($str,0,$pos));
		$y=trim(sunnyjar_substr($str,$pos+1));
		$y=($y<50?$y+2000:($y<1900?$y+1900:$y));
		return ''.($y).'-'.(sunnyjar_strlen($m)<2?'0':'').($m).'-'.(sunnyjar_strlen($d)<2?'0':'').($d);
	}
}

// Return difference or date
if (!function_exists('sunnyjar_get_date_or_difference')) {
	function sunnyjar_get_date_or_difference($dt1, $dt2=null, $max_days=-1) {
		static $gmt_offset = 999;
		if ($gmt_offset==999) $gmt_offset = (int) get_option('gmt_offset');
		if ($max_days < 0) $max_days = sunnyjar_get_theme_option('show_date_after', 30);
		if ($dt2 == null) $dt2 = date('Y-m-d H:i:s');
		$dt2n = strtotime($dt2)+$gmt_offset*3600;
		$dt1n = strtotime($dt1);
		if (is_numeric($dt1n) && is_numeric($dt2n)) {
			$diff = $dt2n - $dt1n;
			$days = floor($diff / (24*3600));
			if (abs($days) < $max_days)
				return sprintf($days >= 0 ? esc_html__('%s ago', 'sunnyjar') : esc_html__('in %s', 'sunnyjar'), sunnyjar_get_date_difference($days >= 0 ? $dt1 : $dt2, $days >= 0 ? $dt2 : $dt1));
			else
				return sunnyjar_get_date_translations(date(get_option('date_format'), $dt1n));
		} else
			return sunnyjar_get_date_translations($dt1);
	}
}

// Difference between two dates
if (!function_exists('sunnyjar_get_date_difference')) {
	function sunnyjar_get_date_difference($dt1, $dt2=null, $short=1, $sec = false) {
		static $gmt_offset = 999;
		if ($gmt_offset==999) $gmt_offset = (int) get_option('gmt_offset');
		if ($dt2 == null) $dt2n = time()+$gmt_offset*3600;
		else $dt2n = strtotime($dt2)+$gmt_offset*3600;
		$dt1n = strtotime($dt1);
		if (is_numeric($dt1n) && is_numeric($dt2n)) {
			$diff = $dt2n - $dt1n;
			$days = floor($diff / (24*3600));
			$months = floor($days / 30);
			$diff -= $days * 24 * 3600;
			$hours = floor($diff / 3600);
			$diff -= $hours * 3600;
			$min = floor($diff / 60);
			$diff -= $min * 60;
			$rez = '';
			if ($months > 0 && $short == 2)
				$rez .= ($rez!='' ? ' ' : '') . sprintf($months > 1 ? esc_html__('%s months', 'sunnyjar') : esc_html__('%s month', 'sunnyjar'), $months);
			if ($days > 0 && ($short < 2 || $rez==''))
				$rez .= ($rez!='' ? ' ' : '') . sprintf($days > 1 ? esc_html__('%s days', 'sunnyjar') : esc_html__('%s day', 'sunnyjar'), $days);
			if ((!$short || $rez=='') && $hours > 0)
				$rez .= ($rez!='' ? ' ' : '') . sprintf($hours > 1 ? esc_html__('%s hours', 'sunnyjar') : esc_html__('%s hour', 'sunnyjar'), $hours);
			if ((!$short || $rez=='') && $min > 0)
				$rez .= ($rez!='' ? ' ' : '') . sprintf($min > 1 ? esc_html__('%s minutes', 'sunnyjar') : esc_html__('%s minute', 'sunnyjar'), $min);
			if ($sec || $rez=='')
				$rez .=  $rez!='' || $sec ? (' ' . sprintf($diff > 1 ? esc_html__('%s seconds', 'sunnyjar') : esc_html__('%s second', 'sunnyjar'), $diff)) : esc_html__('less then minute', 'sunnyjar');
			return $rez;
		} else
			return $dt1;
	}
}

// Prepare month names in date for translation
if (!function_exists('sunnyjar_get_date_translations')) {
	function sunnyjar_get_date_translations($dt) {
		return str_replace(
			array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',
				  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'),
			array(
				esc_html__('January', 'sunnyjar'),
				esc_html__('February', 'sunnyjar'),
				esc_html__('March', 'sunnyjar'),
				esc_html__('April', 'sunnyjar'),
				esc_html__('May', 'sunnyjar'),
				esc_html__('June', 'sunnyjar'),
				esc_html__('July', 'sunnyjar'),
				esc_html__('August', 'sunnyjar'),
				esc_html__('September', 'sunnyjar'),
				esc_html__('October', 'sunnyjar'),
				esc_html__('November', 'sunnyjar'),
				esc_html__('December', 'sunnyjar'),
				esc_html__('Jan', 'sunnyjar'),
				esc_html__('Feb', 'sunnyjar'),
				esc_html__('Mar', 'sunnyjar'),
				esc_html__('Apr', 'sunnyjar'),
				esc_html__('May', 'sunnyjar'),
				esc_html__('Jun', 'sunnyjar'),
				esc_html__('Jul', 'sunnyjar'),
				esc_html__('Aug', 'sunnyjar'),
				esc_html__('Sep', 'sunnyjar'),
				esc_html__('Oct', 'sunnyjar'),
				esc_html__('Nov', 'sunnyjar'),
				esc_html__('Dec', 'sunnyjar'),
			),
			$dt);
	}
}
?>