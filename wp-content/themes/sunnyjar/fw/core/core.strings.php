<?php
/**
 * Sunnyjar Framework: strings manipulations
 *
 * @package	sunnyjar
 * @since	sunnyjar 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Check multibyte functions
if ( ! defined( 'SUNNYJAR_MULTIBYTE' ) ) define( 'SUNNYJAR_MULTIBYTE', function_exists('mb_strpos') ? 'UTF-8' : false );

if (!function_exists('sunnyjar_strlen')) {
	function sunnyjar_strlen($text) {
		return SUNNYJAR_MULTIBYTE ? mb_strlen($text) : strlen($text);
	}
}

if (!function_exists('sunnyjar_strpos')) {
	function sunnyjar_strpos($text, $char, $from=0) {
		return SUNNYJAR_MULTIBYTE ? mb_strpos($text, $char, $from) : strpos($text, $char, $from);
	}
}

if (!function_exists('sunnyjar_strrpos')) {
	function sunnyjar_strrpos($text, $char, $from=0) {
		return SUNNYJAR_MULTIBYTE ? mb_strrpos($text, $char, $from) : strrpos($text, $char, $from);
	}
}

if (!function_exists('sunnyjar_substr')) {
	function sunnyjar_substr($text, $from, $len=-999999) {
		if ($len==-999999) { 
			if ($from < 0)
				$len = -$from; 
			else
				$len = sunnyjar_strlen($text)-$from;
		}
		return SUNNYJAR_MULTIBYTE ? mb_substr($text, $from, $len) : substr($text, $from, $len);
	}
}

if (!function_exists('sunnyjar_strtolower')) {
	function sunnyjar_strtolower($text) {
		return SUNNYJAR_MULTIBYTE ? mb_strtolower($text) : strtolower($text);
	}
}

if (!function_exists('sunnyjar_strtoupper')) {
	function sunnyjar_strtoupper($text) {
		return SUNNYJAR_MULTIBYTE ? mb_strtoupper($text) : strtoupper($text);
	}
}

if (!function_exists('sunnyjar_strtoproper')) {
	function sunnyjar_strtoproper($text) { 
		$rez = ''; $last = ' ';
		for ($i=0; $i<sunnyjar_strlen($text); $i++) {
			$ch = sunnyjar_substr($text, $i, 1);
			$rez .= sunnyjar_strpos(' .,:;?!()[]{}+=', $last)!==false ? sunnyjar_strtoupper($ch) : sunnyjar_strtolower($ch);
			$last = $ch;
		}
		return $rez;
	}
}

if (!function_exists('sunnyjar_strrepeat')) {
	function sunnyjar_strrepeat($str, $n) {
		$rez = '';
		for ($i=0; $i<$n; $i++)
			$rez .= $str;
		return $rez;
	}
}

if (!function_exists('sunnyjar_strshort')) {
	function sunnyjar_strshort($str, $maxlength, $add='...') {
	//	if ($add && sunnyjar_substr($add, 0, 1) != ' ')
	//		$add .= ' ';
		if ($maxlength < 0) 
			return $str;
		if ($maxlength == 0) 
			return '';
		if ($maxlength >= sunnyjar_strlen($str)) 
			return strip_tags($str);
		$str = sunnyjar_substr(strip_tags($str), 0, $maxlength - sunnyjar_strlen($add));
		$ch = sunnyjar_substr($str, $maxlength - sunnyjar_strlen($add), 1);
		if ($ch != ' ') {
			for ($i = sunnyjar_strlen($str) - 1; $i > 0; $i--)
				if (sunnyjar_substr($str, $i, 1) == ' ') break;
			$str = trim(sunnyjar_substr($str, 0, $i));
		}
		if (!empty($str) && sunnyjar_strpos(',.:;-', sunnyjar_substr($str, -1))!==false) $str = sunnyjar_substr($str, 0, -1);
		return ($str) . ($add);
	}
}

// Clear string from spaces, line breaks and tags (only around text)
if (!function_exists('sunnyjar_strclear')) {
	function sunnyjar_strclear($text, $tags=array()) {
		if (empty($text)) return $text;
		if (!is_array($tags)) {
			if ($tags != '')
				$tags = explode($tags, ',');
			else
				$tags = array();
		}
		$text = trim(chop($text));
		if (is_array($tags) && count($tags) > 0) {
			foreach ($tags as $tag) {
				$open  = '<'.esc_attr($tag);
				$close = '</'.esc_attr($tag).'>';
				if (sunnyjar_substr($text, 0, sunnyjar_strlen($open))==$open) {
					$pos = sunnyjar_strpos($text, '>');
					if ($pos!==false) $text = sunnyjar_substr($text, $pos+1);
				}
				if (sunnyjar_substr($text, -sunnyjar_strlen($close))==$close) $text = sunnyjar_substr($text, 0, sunnyjar_strlen($text) - sunnyjar_strlen($close));
				$text = trim(chop($text));
			}
		}
		return $text;
	}
}

// Return slug for the any title string
if (!function_exists('sunnyjar_get_slug')) {
	function sunnyjar_get_slug($title) {
		return sunnyjar_strtolower(str_replace(array('\\','/','-',' ','.'), '_', $title));
	}
}

// Replace macros in the string
if (!function_exists('sunnyjar_strmacros')) {
	function sunnyjar_strmacros($str) {
		return str_replace(array("{{", "}}", "((", "))", "||"), array("<i>", "</i>", "<b>", "</b>", "<br>"), $str);
	}
}

// Unserialize string (try replace \n with \r\n)
if (!function_exists('sunnyjar_unserialize')) {
	function sunnyjar_unserialize($str) {
		if ( is_serialized($str) ) {
			try {
				$data = unserialize($str);
			} catch (Exception $e) {
				dcl($e->getMessage());
				$data = false;
			}
			if ($data===false) {
				try {
					$data = @unserialize(str_replace("\n", "\r\n", $str));
				} catch (Exception $e) {
					dcl($e->getMessage());
					$data = false;
				}
			}
			//if ($data===false) $data = @unserialize(str_replace(array("\n", "\r"), array('\\n','\\r'), $str));
			return $data;
		} else
			return $str;
	}
}
?>