<?php
/**
 * Sunnyjar Framework: theme variables storage
 *
 * @package	sunnyjar
 * @since	sunnyjar 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get theme variable
if (!function_exists('sunnyjar_storage_get')) {
	function sunnyjar_storage_get($var_name, $default='') {
		global $SUNNYJAR_STORAGE;
		return isset($SUNNYJAR_STORAGE[$var_name]) ? $SUNNYJAR_STORAGE[$var_name] : $default;
	}
}

// Set theme variable
if (!function_exists('sunnyjar_storage_set')) {
	function sunnyjar_storage_set($var_name, $value) {
		global $SUNNYJAR_STORAGE;
		$SUNNYJAR_STORAGE[$var_name] = $value;
	}
}

// Check if theme variable is empty
if (!function_exists('sunnyjar_storage_empty')) {
	function sunnyjar_storage_empty($var_name, $key='', $key2='') {
		global $SUNNYJAR_STORAGE;
		if (!empty($key) && !empty($key2))
			return empty($SUNNYJAR_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return empty($SUNNYJAR_STORAGE[$var_name][$key]);
		else
			return empty($SUNNYJAR_STORAGE[$var_name]);
	}
}

// Check if theme variable is set
if (!function_exists('sunnyjar_storage_isset')) {
	function sunnyjar_storage_isset($var_name, $key='', $key2='') {
		global $SUNNYJAR_STORAGE;
		if (!empty($key) && !empty($key2))
			return isset($SUNNYJAR_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return isset($SUNNYJAR_STORAGE[$var_name][$key]);
		else
			return isset($SUNNYJAR_STORAGE[$var_name]);
	}
}

// Inc/Dec theme variable with specified value
if (!function_exists('sunnyjar_storage_inc')) {
	function sunnyjar_storage_inc($var_name, $value=1) {
		global $SUNNYJAR_STORAGE;
		if (empty($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = 0;
		$SUNNYJAR_STORAGE[$var_name] += $value;
	}
}

// Concatenate theme variable with specified value
if (!function_exists('sunnyjar_storage_concat')) {
	function sunnyjar_storage_concat($var_name, $value) {
		global $SUNNYJAR_STORAGE;
		if (empty($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = '';
		$SUNNYJAR_STORAGE[$var_name] .= $value;
	}
}

// Get array (one or two dim) element
if (!function_exists('sunnyjar_storage_get_array')) {
	function sunnyjar_storage_get_array($var_name, $key, $key2='', $default='') {
		global $SUNNYJAR_STORAGE;
		if (empty($key2))
			return !empty($var_name) && !empty($key) && isset($SUNNYJAR_STORAGE[$var_name][$key]) ? $SUNNYJAR_STORAGE[$var_name][$key] : $default;
		else
			return !empty($var_name) && !empty($key) && isset($SUNNYJAR_STORAGE[$var_name][$key][$key2]) ? $SUNNYJAR_STORAGE[$var_name][$key][$key2] : $default;
	}
}

// Set array element
if (!function_exists('sunnyjar_storage_set_array')) {
	function sunnyjar_storage_set_array($var_name, $key, $value) {
		global $SUNNYJAR_STORAGE;
		if (!isset($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = array();
		if ($key==='')
			$SUNNYJAR_STORAGE[$var_name][] = $value;
		else
			$SUNNYJAR_STORAGE[$var_name][$key] = $value;
	}
}

// Set two-dim array element
if (!function_exists('sunnyjar_storage_set_array2')) {
	function sunnyjar_storage_set_array2($var_name, $key, $key2, $value) {
		global $SUNNYJAR_STORAGE;
		if (!isset($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = array();
		if (!isset($SUNNYJAR_STORAGE[$var_name][$key])) $SUNNYJAR_STORAGE[$var_name][$key] = array();
		if ($key2==='')
			$SUNNYJAR_STORAGE[$var_name][$key][] = $value;
		else
			$SUNNYJAR_STORAGE[$var_name][$key][$key2] = $value;
	}
}

// Add array element after the key
if (!function_exists('sunnyjar_storage_set_array_after')) {
	function sunnyjar_storage_set_array_after($var_name, $after, $key, $value='') {
		global $SUNNYJAR_STORAGE;
		if (!isset($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = array();
		if (is_array($key))
			sunnyjar_array_insert_after($SUNNYJAR_STORAGE[$var_name], $after, $key);
		else
			sunnyjar_array_insert_after($SUNNYJAR_STORAGE[$var_name], $after, array($key=>$value));
	}
}

// Add array element before the key
if (!function_exists('sunnyjar_storage_set_array_before')) {
	function sunnyjar_storage_set_array_before($var_name, $before, $key, $value='') {
		global $SUNNYJAR_STORAGE;
		if (!isset($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = array();
		if (is_array($key))
			sunnyjar_array_insert_before($SUNNYJAR_STORAGE[$var_name], $before, $key);
		else
			sunnyjar_array_insert_before($SUNNYJAR_STORAGE[$var_name], $before, array($key=>$value));
	}
}

// Push element into array
if (!function_exists('sunnyjar_storage_push_array')) {
	function sunnyjar_storage_push_array($var_name, $key, $value) {
		global $SUNNYJAR_STORAGE;
		if (!isset($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = array();
		if ($key==='')
			array_push($SUNNYJAR_STORAGE[$var_name], $value);
		else {
			if (!isset($SUNNYJAR_STORAGE[$var_name][$key])) $SUNNYJAR_STORAGE[$var_name][$key] = array();
			array_push($SUNNYJAR_STORAGE[$var_name][$key], $value);
		}
	}
}

// Pop element from array
if (!function_exists('sunnyjar_storage_pop_array')) {
	function sunnyjar_storage_pop_array($var_name, $key='', $defa='') {
		global $SUNNYJAR_STORAGE;
		$rez = $defa;
		if ($key==='') {
			if (isset($SUNNYJAR_STORAGE[$var_name]) && is_array($SUNNYJAR_STORAGE[$var_name]) && count($SUNNYJAR_STORAGE[$var_name]) > 0) 
				$rez = array_pop($SUNNYJAR_STORAGE[$var_name]);
		} else {
			if (isset($SUNNYJAR_STORAGE[$var_name][$key]) && is_array($SUNNYJAR_STORAGE[$var_name][$key]) && count($SUNNYJAR_STORAGE[$var_name][$key]) > 0) 
				$rez = array_pop($SUNNYJAR_STORAGE[$var_name][$key]);
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if (!function_exists('sunnyjar_storage_inc_array')) {
	function sunnyjar_storage_inc_array($var_name, $key, $value=1) {
		global $SUNNYJAR_STORAGE;
		if (!isset($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = array();
		if (empty($SUNNYJAR_STORAGE[$var_name][$key])) $SUNNYJAR_STORAGE[$var_name][$key] = 0;
		$SUNNYJAR_STORAGE[$var_name][$key] += $value;
	}
}

// Concatenate array element with specified value
if (!function_exists('sunnyjar_storage_concat_array')) {
	function sunnyjar_storage_concat_array($var_name, $key, $value) {
		global $SUNNYJAR_STORAGE;
		if (!isset($SUNNYJAR_STORAGE[$var_name])) $SUNNYJAR_STORAGE[$var_name] = array();
		if (empty($SUNNYJAR_STORAGE[$var_name][$key])) $SUNNYJAR_STORAGE[$var_name][$key] = '';
		$SUNNYJAR_STORAGE[$var_name][$key] .= $value;
	}
}

// Call object's method
if (!function_exists('sunnyjar_storage_call_obj_method')) {
	function sunnyjar_storage_call_obj_method($var_name, $method, $param=null) {
		global $SUNNYJAR_STORAGE;
		if ($param===null)
			return !empty($var_name) && !empty($method) && isset($SUNNYJAR_STORAGE[$var_name]) ? $SUNNYJAR_STORAGE[$var_name]->$method(): '';
		else
			return !empty($var_name) && !empty($method) && isset($SUNNYJAR_STORAGE[$var_name]) ? $SUNNYJAR_STORAGE[$var_name]->$method($param): '';
	}
}

// Get object's property
if (!function_exists('sunnyjar_storage_get_obj_property')) {
	function sunnyjar_storage_get_obj_property($var_name, $prop, $default='') {
		global $SUNNYJAR_STORAGE;
		return !empty($var_name) && !empty($prop) && isset($SUNNYJAR_STORAGE[$var_name]->$prop) ? $SUNNYJAR_STORAGE[$var_name]->$prop : $default;
	}
}
?>