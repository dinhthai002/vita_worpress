<?php
/**
 * Sunnyjar Framework: messages subsystem
 *
 * @package	sunnyjar
 * @since	sunnyjar 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('sunnyjar_messages_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_messages_theme_setup' );
	function sunnyjar_messages_theme_setup() {
		// Core messages strings
		add_action('sunnyjar_action_add_scripts_inline', 'sunnyjar_messages_add_scripts_inline');
	}
}


/* Session messages
------------------------------------------------------------------------------------- */

if (!function_exists('sunnyjar_get_error_msg')) {
	function sunnyjar_get_error_msg() {
		return sunnyjar_storage_get('error_msg');
	}
}

if (!function_exists('sunnyjar_set_error_msg')) {
	function sunnyjar_set_error_msg($msg) {
		$msg2 = sunnyjar_get_error_msg();
		sunnyjar_storage_set('error_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}

if (!function_exists('sunnyjar_get_success_msg')) {
	function sunnyjar_get_success_msg() {
		return sunnyjar_storage_get('success_msg');
	}
}

if (!function_exists('sunnyjar_set_success_msg')) {
	function sunnyjar_set_success_msg($msg) {
		$msg2 = sunnyjar_get_success_msg();
		sunnyjar_storage_set('success_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}

if (!function_exists('sunnyjar_get_notice_msg')) {
	function sunnyjar_get_notice_msg() {
		return sunnyjar_storage_get('notice_msg');
	}
}

if (!function_exists('sunnyjar_set_notice_msg')) {
	function sunnyjar_set_notice_msg($msg) {
		$msg2 = sunnyjar_get_notice_msg();
		sunnyjar_storage_set('notice_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}


/* System messages (save when page reload)
------------------------------------------------------------------------------------- */
if (!function_exists('sunnyjar_set_system_message')) {
	function sunnyjar_set_system_message($msg, $status='info', $hdr='') {
		update_option('sunnyjar_message', array('message' => $msg, 'status' => $status, 'header' => $hdr));
	}
}

if (!function_exists('sunnyjar_get_system_message')) {
	function sunnyjar_get_system_message($del=false) {
		$msg = get_option('sunnyjar_message', false);
		if (!$msg)
			$msg = array('message' => '', 'status' => '', 'header' => '');
		else if ($del)
			sunnyjar_del_system_message();
		return $msg;
	}
}

if (!function_exists('sunnyjar_del_system_message')) {
	function sunnyjar_del_system_message() {
		delete_option('sunnyjar_message');
	}
}


/* Messages strings
------------------------------------------------------------------------------------- */

if (!function_exists('sunnyjar_messages_add_scripts_inline')) {
	function sunnyjar_messages_add_scripts_inline() {
		echo '<script type="text/javascript">'
			
			. "if (typeof SUNNYJAR_STORAGE == 'undefined') var SUNNYJAR_STORAGE = {};"
			
			// Strings for translation
			. 'SUNNYJAR_STORAGE["strings"] = {'
				. 'ajax_error: 			"' . addslashes(esc_html__('Invalid server answer', 'sunnyjar')) . '",'
				. 'bookmark_add: 		"' . addslashes(esc_html__('Add the bookmark', 'sunnyjar')) . '",'
				. 'bookmark_added:		"' . addslashes(esc_html__('Current page has been successfully added to the bookmarks. You can see it in the right panel on the tab \'Bookmarks\'', 'sunnyjar')) . '",'
				. 'bookmark_del: 		"' . addslashes(esc_html__('Delete this bookmark', 'sunnyjar')) . '",'
				. 'bookmark_title:		"' . addslashes(esc_html__('Enter bookmark title', 'sunnyjar')) . '",'
				. 'bookmark_exists:		"' . addslashes(esc_html__('Current page already exists in the bookmarks list', 'sunnyjar')) . '",'
				. 'search_error:		"' . addslashes(esc_html__('Error occurs in AJAX search! Please, type your query and press search icon for the traditional search way.', 'sunnyjar')) . '",'
				. 'email_confirm:		"' . addslashes(esc_html__('On the e-mail address "%s" we sent a confirmation email. Please, open it and click on the link.', 'sunnyjar')) . '",'
				. 'reviews_vote:		"' . addslashes(esc_html__('Thanks for your vote! New average rating is:', 'sunnyjar')) . '",'
				. 'reviews_error:		"' . addslashes(esc_html__('Error saving your vote! Please, try again later.', 'sunnyjar')) . '",'
				. 'error_like:			"' . addslashes(esc_html__('Error saving your like! Please, try again later.', 'sunnyjar')) . '",'
				. 'error_global:		"' . addslashes(esc_html__('Global error text', 'sunnyjar')) . '",'
				. 'name_empty:			"' . addslashes(esc_html__('The name can\'t be empty', 'sunnyjar')) . '",'
				. 'name_long:			"' . addslashes(esc_html__('Too long name', 'sunnyjar')) . '",'
				. 'email_empty:			"' . addslashes(esc_html__('Too short (or empty) email address', 'sunnyjar')) . '",'
				. 'email_long:			"' . addslashes(esc_html__('Too long email address', 'sunnyjar')) . '",'
				. 'email_not_valid:		"' . addslashes(esc_html__('Invalid email address', 'sunnyjar')) . '",'
				. 'subject_empty:		"' . addslashes(esc_html__('The subject can\'t be empty', 'sunnyjar')) . '",'
				. 'subject_long:		"' . addslashes(esc_html__('Too long subject', 'sunnyjar')) . '",'
				. 'text_empty:			"' . addslashes(esc_html__('The message text can\'t be empty', 'sunnyjar')) . '",'
				. 'text_long:			"' . addslashes(esc_html__('Too long message text', 'sunnyjar')) . '",'
				. 'send_complete:		"' . addslashes(esc_html__("Send message complete!", 'sunnyjar')) . '",'
				. 'send_error:			"' . addslashes(esc_html__('Transmit failed!', 'sunnyjar')) . '",'
				. 'login_empty:			"' . addslashes(esc_html__('The Login field can\'t be empty', 'sunnyjar')) . '",'
				. 'login_long:			"' . addslashes(esc_html__('Too long login field', 'sunnyjar')) . '",'
				. 'login_success:		"' . addslashes(esc_html__('Login success! The page will be reloaded in 3 sec.', 'sunnyjar')) . '",'
				. 'login_failed:		"' . addslashes(esc_html__('Login failed!', 'sunnyjar')) . '",'
				. 'password_empty:		"' . addslashes(esc_html__('The password can\'t be empty and shorter then 4 characters', 'sunnyjar')) . '",'
				. 'password_long:		"' . addslashes(esc_html__('Too long password', 'sunnyjar')) . '",'
				. 'password_not_equal:	"' . addslashes(esc_html__('The passwords in both fields are not equal', 'sunnyjar')) . '",'
				. 'registration_success:"' . addslashes(esc_html__('Registration success! Please log in!', 'sunnyjar')) . '",'
				. 'registration_failed:	"' . addslashes(esc_html__('Registration failed!', 'sunnyjar')) . '",'
				. 'geocode_error:		"' . addslashes(esc_html__('Geocode was not successful for the following reason:', 'sunnyjar')) . '",'
				. 'googlemap_not_avail:	"' . addslashes(esc_html__('Google map API not available!', 'sunnyjar')) . '",'
				. 'editor_save_success:	"' . addslashes(esc_html__("Post content saved!", 'sunnyjar')) . '",'
				. 'editor_save_error:	"' . addslashes(esc_html__("Error saving post data!", 'sunnyjar')) . '",'
				. 'editor_delete_post:	"' . addslashes(esc_html__("You really want to delete the current post?", 'sunnyjar')) . '",'
				. 'editor_delete_post_header:"' . addslashes(esc_html__("Delete post", 'sunnyjar')) . '",'
				. 'editor_delete_success:	"' . addslashes(esc_html__("Post deleted!", 'sunnyjar')) . '",'
				. 'editor_delete_error:		"' . addslashes(esc_html__("Error deleting post!", 'sunnyjar')) . '",'
				. 'editor_caption_cancel:	"' . addslashes(esc_html__('Cancel', 'sunnyjar')) . '",'
				. 'editor_caption_close:	"' . addslashes(esc_html__('Close', 'sunnyjar')) . '",'
				. 'placeholder_widget_search:	"' . addslashes(esc_html__('Enter Keyword', 'sunnyjar')) . '"'
				. '};'
			
			. '</script>';
	}
}
?>