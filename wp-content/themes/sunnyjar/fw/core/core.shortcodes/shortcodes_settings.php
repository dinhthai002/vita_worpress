<?php

// Check if shortcodes settings are now used
if ( !function_exists( 'sunnyjar_shortcodes_is_used' ) ) {
	function sunnyjar_shortcodes_is_used() {
		return sunnyjar_options_is_used() 															// All modes when Theme Options are used
			|| (is_admin() && isset($_POST['action']) 
					&& in_array($_POST['action'], array('vc_edit_form', 'wpb_show_edit_form')))		// AJAX query when save post/page
			|| (is_admin() && sunnyjar_strpos($_SERVER['REQUEST_URI'], 'vc-roles')!==false)			// VC Role Manager
			|| (function_exists('sunnyjar_vc_is_frontend') && sunnyjar_vc_is_frontend());			// VC Frontend editor mode
	}
}

// Width and height params
if ( !function_exists( 'sunnyjar_shortcodes_width' ) ) {
	function sunnyjar_shortcodes_width($w="") {
		return array(
			"title" => esc_html__("Width", 'sunnyjar'),
			"divider" => true,
			"value" => $w,
			"type" => "text"
		);
	}
}
if ( !function_exists( 'sunnyjar_shortcodes_height' ) ) {
	function sunnyjar_shortcodes_height($h='') {
		return array(
			"title" => esc_html__("Height", 'sunnyjar'),
			"desc" => wp_kses_data( __("Width and height of the element", 'sunnyjar') ),
			"value" => $h,
			"type" => "text"
		);
	}
}

// Return sc_param value
if ( !function_exists( 'sunnyjar_get_sc_param' ) ) {
	function sunnyjar_get_sc_param($prm) {
		return sunnyjar_storage_get_array('sc_params', $prm);
	}
}

// Set sc_param value
if ( !function_exists( 'sunnyjar_set_sc_param' ) ) {
	function sunnyjar_set_sc_param($prm, $val) {
		sunnyjar_storage_set_array('sc_params', $prm, $val);
	}
}

// Add sc settings in the sc list
if ( !function_exists( 'sunnyjar_sc_map' ) ) {
	function sunnyjar_sc_map($sc_name, $sc_settings) {
		sunnyjar_storage_set_array('shortcodes', $sc_name, $sc_settings);
	}
}

// Add sc settings in the sc list after the key
if ( !function_exists( 'sunnyjar_sc_map_after' ) ) {
	function sunnyjar_sc_map_after($after, $sc_name, $sc_settings='') {
		sunnyjar_storage_set_array_after('shortcodes', $after, $sc_name, $sc_settings);
	}
}

// Add sc settings in the sc list before the key
if ( !function_exists( 'sunnyjar_sc_map_before' ) ) {
	function sunnyjar_sc_map_before($before, $sc_name, $sc_settings='') {
		sunnyjar_storage_set_array_before('shortcodes', $before, $sc_name, $sc_settings);
	}
}

// Compare two shortcodes by title
if ( !function_exists( 'sunnyjar_compare_sc_title' ) ) {
	function sunnyjar_compare_sc_title($a, $b) {
		return strcmp($a['title'], $b['title']);
	}
}



/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'sunnyjar_shortcodes_settings_theme_setup' ) ) {
//	if ( sunnyjar_vc_is_frontend() )
	if ( (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') || (isset($_GET['vc_action']) && $_GET['vc_action']=='vc_inline') )
		add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_shortcodes_settings_theme_setup', 20 );
	else
		add_action( 'sunnyjar_action_after_init_theme', 'sunnyjar_shortcodes_settings_theme_setup' );
	function sunnyjar_shortcodes_settings_theme_setup() {
		if (sunnyjar_shortcodes_is_used()) {

			// Sort templates alphabetically
			$tmp = sunnyjar_storage_get('registered_templates');
			ksort($tmp);
			sunnyjar_storage_set('registered_templates', $tmp);

			// Prepare arrays 
			sunnyjar_storage_set('sc_params', array(
			
				// Current element id
				'id' => array(
					"title" => esc_html__("Element ID", 'sunnyjar'),
					"desc" => wp_kses_data( __("ID for current element", 'sunnyjar') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
			
				// Current element class
				'class' => array(
					"title" => esc_html__("Element CSS class", 'sunnyjar'),
					"desc" => wp_kses_data( __("CSS class for current element (optional)", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
			
				// Current element style
				'css' => array(
					"title" => esc_html__("CSS styles", 'sunnyjar'),
					"desc" => wp_kses_data( __("Any additional CSS rules (if need)", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
			
			
				// Switcher choises
				'list_styles' => array(
					'ul'	=> esc_html__('Unordered', 'sunnyjar'),
					'ol'	=> esc_html__('Ordered', 'sunnyjar'),
					'iconed'=> esc_html__('Iconed', 'sunnyjar')
				),

				'yes_no'	=> sunnyjar_get_list_yesno(),
				'on_off'	=> sunnyjar_get_list_onoff(),
				'dir' 		=> sunnyjar_get_list_directions(),
				'align'		=> sunnyjar_get_list_alignments(),
				'float'		=> sunnyjar_get_list_floats(),
				'hpos'		=> sunnyjar_get_list_hpos(),
				'show_hide'	=> sunnyjar_get_list_showhide(),
				'sorting' 	=> sunnyjar_get_list_sortings(),
				'ordering' 	=> sunnyjar_get_list_orderings(),
				'shapes'	=> sunnyjar_get_list_shapes(),
				'sizes'		=> sunnyjar_get_list_sizes(),
				'sliders'	=> sunnyjar_get_list_sliders(),
				'controls'	=> sunnyjar_get_list_controls(),
				'categories'=> sunnyjar_get_list_categories(),
				'columns'	=> sunnyjar_get_list_columns(),
				'images'	=> array_merge(array('none'=>"none"), sunnyjar_get_list_files("images/icons", "png")),
				'icons'		=> array_merge(array("inherit", "none"), sunnyjar_get_list_icons()),
				'locations'	=> sunnyjar_get_list_dedicated_locations(),
				'filters'	=> sunnyjar_get_list_portfolio_filters(),
				'formats'	=> sunnyjar_get_list_post_formats_filters(),
				'hovers'	=> sunnyjar_get_list_hovers(true),
				'hovers_dir'=> sunnyjar_get_list_hovers_directions(true),
				'schemes'	=> sunnyjar_get_list_color_schemes(true),
				'animations'		=> sunnyjar_get_list_animations_in(),
				'margins' 			=> sunnyjar_get_list_margins(true),
				'blogger_styles'	=> sunnyjar_get_list_templates_blogger(),
				'forms'				=> sunnyjar_get_list_templates_forms(),
				'posts_types'		=> sunnyjar_get_list_posts_types(),
				'googlemap_styles'	=> sunnyjar_get_list_googlemap_styles(),
				'field_types'		=> sunnyjar_get_list_field_types(),
				'label_positions'	=> sunnyjar_get_list_label_positions()
				)
			);

			// Common params
			sunnyjar_set_sc_param('animation', array(
				"title" => esc_html__("Animation",  'sunnyjar'),
				"desc" => wp_kses_data( __('Select animation while object enter in the visible area of page',  'sunnyjar') ),
				"value" => "none",
				"type" => "select",
				"options" => sunnyjar_get_sc_param('animations')
				)
			);
			sunnyjar_set_sc_param('top', array(
				"title" => esc_html__("Top margin",  'sunnyjar'),
				"divider" => true,
				"value" => "inherit",
				"type" => "select",
				"options" => sunnyjar_get_sc_param('margins')
				)
			);
			sunnyjar_set_sc_param('bottom', array(
				"title" => esc_html__("Bottom margin",  'sunnyjar'),
				"value" => "inherit",
				"type" => "select",
				"options" => sunnyjar_get_sc_param('margins')
				)
			);
			sunnyjar_set_sc_param('left', array(
				"title" => esc_html__("Left margin",  'sunnyjar'),
				"value" => "inherit",
				"type" => "select",
				"options" => sunnyjar_get_sc_param('margins')
				)
			);
			sunnyjar_set_sc_param('right', array(
				"title" => esc_html__("Right margin",  'sunnyjar'),
				"desc" => wp_kses_data( __("Margins around this shortcode", 'sunnyjar') ),
				"value" => "inherit",
				"type" => "select",
				"options" => sunnyjar_get_sc_param('margins')
				)
			);

			sunnyjar_storage_set('sc_params', apply_filters('sunnyjar_filter_shortcodes_params', sunnyjar_storage_get('sc_params')));

			// Shortcodes list
			//------------------------------------------------------------------
			sunnyjar_storage_set('shortcodes', array());
			
			// Register shortcodes
			do_action('sunnyjar_action_shortcodes_list');

			// Sort shortcodes list
			$tmp = sunnyjar_storage_get('shortcodes');
			uasort($tmp, 'sunnyjar_compare_sc_title');
			sunnyjar_storage_set('shortcodes', $tmp);
		}
	}
}
?>