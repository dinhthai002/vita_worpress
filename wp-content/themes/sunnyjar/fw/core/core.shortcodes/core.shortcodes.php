<?php
/**
 * Sunnyjar Framework: shortcodes manipulations
 *
 * @package	sunnyjar
 * @since	sunnyjar 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('sunnyjar_sc_theme_setup')) {
	add_action( 'sunnyjar_action_init_theme', 'sunnyjar_sc_theme_setup', 1 );
	function sunnyjar_sc_theme_setup() {
		// Add sc stylesheets
		add_action('sunnyjar_action_add_styles', 'sunnyjar_sc_add_styles', 1);
	}
}

if (!function_exists('sunnyjar_sc_theme_setup2')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_theme_setup2' );
	function sunnyjar_sc_theme_setup2() {

		if ( !is_admin() || isset($_POST['action']) ) {
			// Enable/disable shortcodes in excerpt
			add_filter('the_excerpt', 					'sunnyjar_sc_excerpt_shortcodes');
	
			// Prepare shortcodes in the content
			if (function_exists('sunnyjar_sc_prepare_content')) sunnyjar_sc_prepare_content();
		}

		// Add init script into shortcodes output in VC frontend editor
		add_filter('sunnyjar_shortcode_output', 'sunnyjar_sc_add_scripts', 10, 4);

		// AJAX: Send contact form data
		add_action('wp_ajax_send_form',			'sunnyjar_sc_form_send');
		add_action('wp_ajax_nopriv_send_form',	'sunnyjar_sc_form_send');

		// Show shortcodes list in admin editor
		add_action('media_buttons',				'sunnyjar_sc_selector_add_in_toolbar', 11);

	}
}


// Register shortcodes styles
if ( !function_exists( 'sunnyjar_sc_add_styles' ) ) {
	//add_action('sunnyjar_action_add_styles', 'sunnyjar_sc_add_styles', 1);
	function sunnyjar_sc_add_styles() {
		// Shortcodes
		sunnyjar_enqueue_style( 'sunnyjar-shortcodes-style',	sunnyjar_get_file_url('shortcodes/theme.shortcodes.css'), array(), null );
	}
}


// Register shortcodes init scripts
if ( !function_exists( 'sunnyjar_sc_add_scripts' ) ) {
	//add_filter('sunnyjar_shortcode_output', 'sunnyjar_sc_add_scripts', 10, 4);
	function sunnyjar_sc_add_scripts($output, $tag='', $atts=array(), $content='') {

		if (sunnyjar_storage_empty('shortcodes_scripts_added')) {
			sunnyjar_storage_set('shortcodes_scripts_added', true);
			//sunnyjar_enqueue_style( 'sunnyjar-shortcodes-style', sunnyjar_get_file_url('shortcodes/theme.shortcodes.css'), array(), null );
			sunnyjar_enqueue_script( 'sunnyjar-shortcodes-script', sunnyjar_get_file_url('shortcodes/theme.shortcodes.js'), array('jquery'), null, true );	
		}
		
		return $output;
	}
}


/* Prepare text for shortcodes
-------------------------------------------------------------------------------- */

// Prepare shortcodes in content
if (!function_exists('sunnyjar_sc_prepare_content')) {
	function sunnyjar_sc_prepare_content() {
		if (function_exists('sunnyjar_sc_clear_around')) {
			$filters = array(
				array('sunnyjar', 'sc', 'clear', 'around'),
				array('widget', 'text'),
				array('the', 'excerpt'),
				array('the', 'content')
			);
			if (function_exists('sunnyjar_exists_woocommerce') && sunnyjar_exists_woocommerce()) {
				$filters[] = array('woocommerce', 'template', 'single', 'excerpt');
				$filters[] = array('woocommerce', 'short', 'description');
			}
			if (is_array($filters) && count($filters) > 0) {
				foreach ($filters as $flt)
					add_filter(join('_', $flt), 'sunnyjar_sc_clear_around', 1);	// Priority 1 to clear spaces before do_shortcodes()
			}
		}
	}
}

// Enable/Disable shortcodes in the excerpt
if (!function_exists('sunnyjar_sc_excerpt_shortcodes')) {
	function sunnyjar_sc_excerpt_shortcodes($content) {
		if (!empty($content)) {
			$content = do_shortcode($content);
			//$content = strip_shortcodes($content);
		}
		return $content;
	}
}



/*
// Remove spaces and line breaks between close and open shortcode brackets ][:
[trx_columns]
	[trx_column_item]Column text ...[/trx_column_item]
	[trx_column_item]Column text ...[/trx_column_item]
	[trx_column_item]Column text ...[/trx_column_item]
[/trx_columns]

convert to

[trx_columns][trx_column_item]Column text ...[/trx_column_item][trx_column_item]Column text ...[/trx_column_item][trx_column_item]Column text ...[/trx_column_item][/trx_columns]
*/
if (!function_exists('sunnyjar_sc_clear_around')) {
	function sunnyjar_sc_clear_around($content) {
		if (!empty($content)) $content = preg_replace("/\](\s|\n|\r)*\[/", "][", $content);
		return $content;
	}
}






/* Shortcodes support utils
---------------------------------------------------------------------- */

// Sunnyjar shortcodes load scripts
if (!function_exists('sunnyjar_sc_load_scripts')) {
	function sunnyjar_sc_load_scripts() {
		sunnyjar_enqueue_script( 'sunnyjar-shortcodes_admin-script', sunnyjar_get_file_url('core/core.shortcodes/shortcodes_admin.js'), array('jquery'), null, true );
		sunnyjar_enqueue_script( 'sunnyjar-selection-script',  sunnyjar_get_file_url('js/jquery.selection.js'), array('jquery'), null, true );
		wp_localize_script( 'sunnyjar-shortcodes_admin-script', 'SUNNYJAR_SHORTCODES_DATA', sunnyjar_storage_get('shortcodes') );
	}
}

// Sunnyjar shortcodes prepare scripts
if (!function_exists('sunnyjar_sc_prepare_scripts')) {
	function sunnyjar_sc_prepare_scripts() {
		if (!sunnyjar_storage_isset('shortcodes_prepared')) {
			sunnyjar_storage_set('shortcodes_prepared', true);
			?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
					SUNNYJAR_STORAGE['shortcodes_cp'] = '<?php echo is_admin() ? (!sunnyjar_storage_empty('to_colorpicker') ? sunnyjar_storage_get('to_colorpicker') : 'wp') : 'custom'; ?>';	// wp | tiny | custom
				});
			</script>
			<?php
		}
	}
}

// Show shortcodes list in admin editor
if (!function_exists('sunnyjar_sc_selector_add_in_toolbar')) {
	//add_action('media_buttons','sunnyjar_sc_selector_add_in_toolbar', 11);
	function sunnyjar_sc_selector_add_in_toolbar(){

		if ( !sunnyjar_options_is_used() ) return;

		sunnyjar_sc_load_scripts();
		sunnyjar_sc_prepare_scripts();

		$shortcodes = sunnyjar_storage_get('shortcodes');
		$shortcodes_list = '<select class="sc_selector"><option value="">&nbsp;'.esc_html__('- Select Shortcode -', 'sunnyjar').'&nbsp;</option>';

		if (is_array($shortcodes) && count($shortcodes) > 0) {
			foreach ($shortcodes as $idx => $sc) {
				$shortcodes_list .= '<option value="'.esc_attr($idx).'" title="'.esc_attr($sc['desc']).'">'.esc_html($sc['title']).'</option>';
			}
		}

		$shortcodes_list .= '</select>';

		sunnyjar_show_layout($shortcodes_list);
	}
}

// Sunnyjar shortcodes builder settings
get_template_part(sunnyjar_get_file_slug('core/core.shortcodes/shortcodes_settings.php'));

// VC shortcodes settings
if ( class_exists('WPBakeryShortCode') ) {
	get_template_part(sunnyjar_get_file_slug('core/core.shortcodes/shortcodes_vc.php'));
}

// Sunnyjar shortcodes implementation
sunnyjar_autoload_folder( 'shortcodes/trx_basic' );
sunnyjar_autoload_folder( 'shortcodes/trx_optional' );
?>