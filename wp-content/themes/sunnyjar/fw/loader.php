<?php
/**
 * Sunnyjar Framework
 *
 * @package sunnyjar
 * @since sunnyjar 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Framework directory path from theme root
if ( ! defined( 'SUNNYJAR_FW_DIR' ) )			define( 'SUNNYJAR_FW_DIR', 'fw' );
if ( ! defined( 'SUNNYJAR_THEME_PATH' ) )	define( 'SUNNYJAR_THEME_PATH',	trailingslashit( get_template_directory() ) );
if ( ! defined( 'SUNNYJAR_FW_PATH' ) )		define( 'SUNNYJAR_FW_PATH',		SUNNYJAR_THEME_PATH . SUNNYJAR_FW_DIR . '/' );

// Include theme variables storage
require_once SUNNYJAR_FW_PATH . 'core/core.storage.php';

// Theme variables storage
sunnyjar_storage_set('options_prefix', 'sunnyjar');	//.'_'.str_replace(' ', '_', trim(strtolower(get_stylesheet()))));	// Prefix for the theme options in the postmeta and wp options
sunnyjar_storage_set('page_template', '');			// Storage for current page template name (used in the inheritance system)
sunnyjar_storage_set('widgets_args', array(			// Arguments to register widgets
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h5 class="widget_title">',
		'after_title'   => '</h5>',
	)
);

/* Theme setup section
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_loader_theme_setup' ) ) {
	add_action( 'after_setup_theme', 'sunnyjar_loader_theme_setup', 20 );
	function sunnyjar_loader_theme_setup() {

		sunnyjar_profiler_add_point(esc_html__('After load theme required files', 'sunnyjar'));

		// Before init theme
		do_action('sunnyjar_action_before_init_theme');

		// Load current values for main theme options
		sunnyjar_load_main_options();

		// Theme core init - only for admin side. In frontend it called from header.php
		if ( is_admin() ) {
			sunnyjar_core_init_theme();
		}
	}
}


/* Include core parts
------------------------------------------------------------------------ */
// Manual load important libraries before load all rest files
// core.strings must be first - we use sunnyjar_str...() in the sunnyjar_get_file_dir()
get_template_part(SUNNYJAR_FW_DIR.'/core/core.strings');
// core.files must be first - we use sunnyjar_get_file_dir() to include all rest parts
get_template_part(SUNNYJAR_FW_DIR.'/core/core.files');

// Include debug and profiler
get_template_part(sunnyjar_get_file_slug('core/core.debug.php'));

// Include custom theme files
sunnyjar_autoload_folder( 'includes' );

// Include core files
sunnyjar_autoload_folder( 'core' );

// Include theme-specific plugins and post types
sunnyjar_autoload_folder( 'plugins' );

// Include theme templates
sunnyjar_autoload_folder( 'templates' );

// Include theme widgets
sunnyjar_autoload_folder( 'widgets' );
?>