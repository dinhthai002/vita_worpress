<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'sunnyjar_template_no_articles_theme_setup' ) ) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_template_no_articles_theme_setup', 1 );
	function sunnyjar_template_no_articles_theme_setup() {
		sunnyjar_add_template(array(
			'layout' => 'no-articles',
			'mode'   => 'internal',
			'title'  => esc_html__('No articles found', 'sunnyjar')
		));
	}
}

// Template output
if ( !function_exists( 'sunnyjar_template_no_articles_output' ) ) {
	function sunnyjar_template_no_articles_output($post_options, $post_data) {
		?>
		<article class="post_item">
			<div class="post_content">
				<h2 class="post_title"><?php esc_html_e('No posts found', 'sunnyjar'); ?></h2>
				<p><?php esc_html_e( 'Sorry, but nothing matched your search criteria.', 'sunnyjar' ); ?></p>
				<p><?php echo wp_kses_data( sprintf(__('Go back, or return to <a href="%s">%s</a> home page to choose a new page.', 'sunnyjar'), esc_url(home_url('/')), get_bloginfo()) ); ?>
				<br><?php esc_html_e('Please report any broken links to our team.', 'sunnyjar'); ?></p>
				<?php sunnyjar_show_layout(sunnyjar_sc_search(array('state'=>"fixed"))); ?>
			</div>	<!-- /.post_content -->
		</article>	<!-- /.post_item -->
		<?php
	}
}
?>