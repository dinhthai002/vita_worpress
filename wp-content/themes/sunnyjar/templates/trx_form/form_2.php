<?php

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'sunnyjar_template_form_2_theme_setup' ) ) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_template_form_2_theme_setup', 1 );
	function sunnyjar_template_form_2_theme_setup() {
		sunnyjar_add_template(array(
			'layout' => 'form_2',
			'mode'   => 'forms',
			'title'  => esc_html__('Contact Form 2', 'sunnyjar')
			));
	}
}

// Template output
if ( !function_exists( 'sunnyjar_template_form_2_output' ) ) {
	function sunnyjar_template_form_2_output($post_options, $post_data) {
        $title = trim(sunnyjar_storage_get('title_form_2'));
        $subtitle = trim(sunnyjar_storage_get('subtitle_form_2'));
        $description = trim(sunnyjar_storage_get('description_form_2'));
        
		$address_1 = sunnyjar_get_theme_option('contact_address_1');
		$address_2 = sunnyjar_get_theme_option('contact_address_2');
		$phone = sunnyjar_get_theme_option('contact_phone');
		$fax = sunnyjar_get_theme_option('contact_fax');
		$email = sunnyjar_get_theme_option('contact_email');
		$open_hours = sunnyjar_get_theme_option('contact_open_hours');
		?>
		<div class="sc_columns">
			<div class="sc_form_fields column-left">
                <?php  if (!empty($title)) { ?><h3 class="sc_form_title sc_item_title"><?php sunnyjar_show_layout($title) ?></h3><?php }?>
                <?php  if (!empty($subtitle)) { ?><h6 class="sc_form_subtitle sc_item_subtitle"><?php sunnyjar_show_layout($subtitle) ?></h6><?php }?>
                <?php  if (!empty($description)){ ?><div class="sc_form_descr sc_item_descr"><?php sunnyjar_show_layout($description) ?></div><?php }?>
				<form <?php echo !empty($post_options['id']) ? ' id="'.esc_attr($post_options['id']).'_form"' : ''; ?> data-formtype="<?php echo esc_attr($post_options['layout']); ?>" method="post" action="<?php echo esc_url($post_options['action'] ? $post_options['action'] : admin_url('admin-ajax.php')); ?>">
					<?php sunnyjar_sc_form_show_fields($post_options['fields']); ?>
					<div class="sc_form_info">
						<div class="sc_form_item sc_form_field label_over"><label class="required" for="sc_form_username"><?php esc_html_e('Name', 'sunnyjar'); ?></label><input id="sc_form_username" type="text" name="username" placeholder="<?php esc_attr_e('Name *', 'sunnyjar'); ?>"></div>
						<div class="sc_form_item sc_form_field label_over"><label class="required" for="sc_form_email"><?php esc_html_e('E-mail', 'sunnyjar'); ?></label><input id="sc_form_email" type="text" name="email" placeholder="<?php esc_attr_e('E-mail *', 'sunnyjar'); ?>"></div>
					</div>
					<div class="sc_form_item sc_form_message label_over"><label class="required" for="sc_form_message"><?php esc_html_e('Message', 'sunnyjar'); ?></label><textarea id="sc_form_message" name="message" placeholder="<?php esc_attr_e('Message', 'sunnyjar'); ?>"></textarea></div>
					<div class="sc_form_item sc_form_button"><button><?php esc_html_e('Send Message', 'sunnyjar'); ?></button></div>
					<div class="result sc_infobox"></div>
				</form>
			</div>
            <div class="sc_form_address column-right">
                <?php  if (!empty($title)) { ?><h3 class="sc_form_title sc_item_title"><?php echo esc_html_e('Find Us', 'sunnyjar'); ?></h3><?php }?>
                <?php  if (!empty($subtitle)) { ?><h6  class="sc_form_subti tle sc_item_subtitle"><?php echo esc_html_e('Contact Info', 'sunnyjar'); ?></h6><?php }?>
                <div class="sc_form_address_field">
                    <span class="sc_form_address_label"><?php esc_html_e('Address:', 'sunnyjar'); ?></span>
                    <span class="sc_form_address_data"><?php sunnyjar_show_layout($address_1) . (!empty($address_1) && !empty($address_2) ? ', ' : '') . $address_2; ?></span>
                </div>
                <div class="sc_form_address_field">
                    <span class="sc_form_address_label"><?php esc_html_e('Phone number:', 'sunnyjar'); ?></span>
                    <span class="sc_form_address_data"><?php sunnyjar_show_layout($phone) . (!empty($phone) && !empty($fax) ? ', ' : '') . $fax; ?></span>
                </div>
                <div class="sc_form_address_field">
                    <span class="sc_form_address_label"><?php esc_html_e('Mail:', 'sunnyjar'); ?></span>
                    <span class="sc_form_address_data"><?php sunnyjar_show_layout($email); ?></span>
                </div>
                <div class="sc_form_address_field">
                    <span class="sc_form_address_label"><?php esc_html_e('We are open:', 'sunnyjar'); ?></span>
                    <span class="sc_form_address_data"><?php sunnyjar_show_layout($open_hours); ?></span>
                </div>
            </div>
		</div>
		<?php
	}
}
?>