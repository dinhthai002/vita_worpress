<?php 
// Get template args
extract(sunnyjar_template_get_args('top-panel-top'));

if (in_array('contact_address', $top_panel_top_components) && sunnyjar_get_custom_option('show_contact_address')=='yes')  {
    $contact_address_1=trim(sunnyjar_get_custom_option('contact_address_1'));
    $contact_address_2=trim(sunnyjar_get_custom_option('contact_address_2'));
    if (!empty($contact_address_1) || !empty($contact_address_2)) {
        ?>
        <div class="top_panel_top_contact_area contact_address">
            <?php
            echo (!empty($contact_address_1) ? force_balance_tags($contact_address_1) : '');
            echo ((!empty($contact_address_2) && !empty($contact_address_2)) ? ' ' : '');
            echo (!empty($contact_address_2) ? force_balance_tags($contact_address_2) : '');
            ?>
        </div>
        <?php
    }
}

if (in_array('contact_info', $top_panel_top_components) && ($contact_info=trim(sunnyjar_get_custom_option('contact_info')))!='') {
	?>
	<div class="top_panel_top_contact_area contact_info">
		<?php echo force_balance_tags($contact_info); ?>
	</div>
	<?php
}
?>

<?php
if (in_array('open_hours', $top_panel_top_components) && ($open_hours=trim(sunnyjar_get_custom_option('contact_open_hours')))!='') {
	?>
	<div class="top_panel_top_open_hours icon-clock"><?php echo force_balance_tags($open_hours); ?></div>
	<?php
}
?>

<?php
if ((in_array('socials', $top_panel_top_components) && sunnyjar_get_custom_option('show_socials')=='yes')
    || (in_array('search', $top_panel_top_components) && sunnyjar_get_custom_option('show_search')=='yes')
    || (in_array('currency', $top_panel_top_components) && function_exists('sunnyjar_is_woocommerce_page') && sunnyjar_is_woocommerce_page() && sunnyjar_get_custom_option('show_currency')=='yes')
    || (in_array('language', $top_panel_top_components) && sunnyjar_get_custom_option('show_languages')=='yes' && function_exists('icl_get_languages'))
    || (in_array('bookmarks', $top_panel_top_components) && sunnyjar_get_custom_option('show_bookmarks')=='yes')
    || (in_array('login', $top_panel_top_components) && sunnyjar_get_custom_option('show_login')=='yes')
    || (in_array('cart', $top_panel_top_components) && function_exists('sunnyjar_exists_woocommerce') && sunnyjar_exists_woocommerce() && (sunnyjar_is_woocommerce_page() && sunnyjar_get_custom_option('show_cart')=='shop' || sunnyjar_get_custom_option('show_cart')=='always') && !(is_checkout() || is_cart() || defined('WOOCOMMERCE_CHECKOUT') || defined('WOOCOMMERCE_CART')))
    || (in_array('contact_phone', $top_panel_top_components) && ($contact_phone=trim(sunnyjar_get_custom_option('contact_phone')))!='' && sunnyjar_get_custom_option('show_contact_phone')=='yes')
    ) { ?>
    <div class="top_panel_top_user_area">
        <?php
        if (in_array('contact_phone', $top_panel_top_components) && ($contact_phone=trim(sunnyjar_get_custom_option('contact_phone')))!='' && sunnyjar_get_custom_option('show_contact_phone')=='yes') {
            $contact_phone_text=trim(sunnyjar_get_custom_option('contact_phone_text'));
            ?>
            <div class="top_panel_top_contact_area contact_phone">
                <?php echo (!empty($contact_phone_text) ? force_balance_tags($contact_phone_text).' ' : '') . '<span>' . force_balance_tags($contact_phone) . '</span>'; ?>
            </div>
            <?php
        }

        if (in_array('socials', $top_panel_top_components) && sunnyjar_get_custom_option('show_socials')=='yes') {
            ?>
            <div class="top_panel_top_socials">
			<?php sunnyjar_show_layout(sunnyjar_sc_socials(array('size'=>'tiny'))); ?>
            </div>
            <?php
        }
    
        if (in_array('search', $top_panel_top_components) && sunnyjar_get_custom_option('show_search')=='yes') {
            ?>
            <div class="top_panel_top_search"><?php sunnyjar_show_layout(sunnyjar_sc_search(array('state'=>'fixed'))); ?></div>
            <?php
        }

        if ((in_array('currency', $top_panel_top_components) && function_exists('sunnyjar_is_woocommerce_page') && sunnyjar_is_woocommerce_page() && sunnyjar_get_custom_option('show_currency')=='yes')
            || (in_array('language', $top_panel_top_components) && sunnyjar_get_custom_option('show_languages')=='yes' && function_exists('icl_get_languages'))
            || (in_array('bookmarks', $top_panel_top_components) && sunnyjar_get_custom_option('show_bookmarks')=='yes')
            || (in_array('login', $top_panel_top_components) && sunnyjar_get_custom_option('show_login')=='yes')
            || (in_array('cart', $top_panel_top_components) && function_exists('sunnyjar_exists_woocommerce') && sunnyjar_exists_woocommerce() && (sunnyjar_is_woocommerce_page() && sunnyjar_get_custom_option('show_cart')=='shop' || sunnyjar_get_custom_option('show_cart')=='always') && !(is_checkout() || is_cart() || defined('WOOCOMMERCE_CHECKOUT') || defined('WOOCOMMERCE_CART')))
        ){
            $menu_user = sunnyjar_get_nav_menu('menu_user');
            if (empty($menu_user)) {
                ?>
                <ul id="menu_user" class="menu_user_nav">
                <?php
            } else {
                $menu = sunnyjar_substr($menu_user, 0, sunnyjar_strlen($menu_user)-5);
                $pos = sunnyjar_strpos($menu, '<ul');
                if ($pos!==false) $menu = sunnyjar_substr($menu, 0, $pos+3) . ' class="menu_user_nav"' . sunnyjar_substr($menu, $pos+3);
                echo str_replace('class=""', '', $menu);
            }


            if (in_array('currency', $top_panel_top_components) && function_exists('sunnyjar_is_woocommerce_page') && sunnyjar_is_woocommerce_page() && sunnyjar_get_custom_option('show_currency')=='yes') {
                ?>
                <li class="menu_user_currency">
                    <a href="#">$</a>
                    <ul>
                        <li><a href="#"><b>&#36;</b> <?php esc_html_e('Dollar', 'sunnyjar'); ?></a></li>
                        <li><a href="#"><b>&euro;</b> <?php esc_html_e('Euro', 'sunnyjar'); ?></a></li>
                        <li><a href="#"><b>&pound;</b> <?php esc_html_e('Pounds', 'sunnyjar'); ?></a></li>
                    </ul>
                </li>
                <?php
            }

            if (in_array('language', $top_panel_top_components) && sunnyjar_get_custom_option('show_languages')=='yes' && function_exists('icl_get_languages')) {
                $languages = icl_get_languages('skip_missing=1');
                if (!empty($languages) && is_array($languages)) {
                    $lang_list = '';
                    $lang_active = '';
                    foreach ($languages as $lang) {
                        $lang_title = esc_attr($lang['translated_name']);	//esc_attr($lang['native_name']);
                        if ($lang['active']) {
                            $lang_active = $lang_title;
                        }
                        $lang_list .= "\n"
                            .'<li><a rel="alternate" hreflang="' . esc_attr($lang['language_code']) . '" href="' . esc_url(apply_filters('WPML_filter_link', $lang['url'], $lang)) . '">'
                            .'<img src="' . esc_url($lang['country_flag_url']) . '" alt="' . esc_attr($lang_title) . '" title="' . esc_attr($lang_title) . '" />'
                            . ($lang_title)
                            .'</a></li>';
                    }
                    ?>
                    <li class="menu_user_language">
				<a href="#"><span><?php sunnyjar_show_layout($lang_active); ?></span></a>
				<ul><?php sunnyjar_show_layout($lang_list); ?></ul>
                    </li>
                    <?php
                }
            }

            if (in_array('bookmarks', $top_panel_top_components) && sunnyjar_get_custom_option('show_bookmarks')=='yes') {
                // Load core messages
                sunnyjar_enqueue_messages();
                ?>
                <li class="menu_user_bookmarks"><a href="#" class="bookmarks_show icon-star" title="<?php esc_attr_e('Show bookmarks', 'sunnyjar'); ?>"><?php esc_html_e('Bookmarks', 'sunnyjar'); ?></a>
                    <?php
                    $list = sunnyjar_get_value_gpc('sunnyjar_bookmarks', '');
                    if (!empty($list)) $list = json_decode($list, true);
                    ?>
                    <ul class="bookmarks_list">
                        <li><a href="#" class="bookmarks_add icon-star-empty" title="<?php esc_attr_e('Add the current page into bookmarks', 'sunnyjar'); ?>"><?php esc_html_e('Add bookmark', 'sunnyjar'); ?></a></li>
                        <?php
                        if (!empty($list) && is_array($list)) {
                            foreach ($list as $bm) {
                                echo '<li><a href="'.esc_url($bm['url']).'" class="bookmarks_item">'.($bm['title']).'<span class="bookmarks_delete icon-cancel" title="'.esc_attr__('Delete this bookmark', 'sunnyjar').'"></span></a></li>';
                            }
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }

            if (in_array('login', $top_panel_top_components) && sunnyjar_get_custom_option('show_login')=='yes') {
                if ( !is_user_logged_in() ) {
                    // Load core messages
                    sunnyjar_enqueue_messages();
			// Load Popup engine
			sunnyjar_enqueue_popup();
                    // Anyone can register ?
                    if ( (int) get_option('users_can_register') > 0) {
				?><li class="menu_user_register"><?php do_action('trx_utils_action_register'); ?></li><?php
                    }
			?><li class="menu_user_login"><?php do_action('trx_utils_action_login'); ?></li><?php 
                } else {
                    $current_user = wp_get_current_user();
                    ?>
                    <li class="menu_user_controls">
                        <a href="#"><?php
                            $user_avatar = '';
                            $mult = sunnyjar_get_retina_multiplier();
                            if ($current_user->user_email) $user_avatar = get_avatar($current_user->user_email, 16*$mult);
                            if ($user_avatar) {
						?><span class="user_avatar"><?php sunnyjar_show_layout($user_avatar); ?></span><?php
					}?><span class="user_name"><?php sunnyjar_show_layout($current_user->display_name); ?></span></a>
                        <ul>
                            <?php if (current_user_can('publish_posts')) { ?>
                                <li><a href="<?php echo esc_url(home_url('/')); ?>/wp-admin/post-new.php?post_type=post" class="icon icon-doc"><?php esc_html_e('New post', 'sunnyjar'); ?></a></li>
                            <?php } ?>
                            <li><a href="<?php echo get_edit_user_link(); ?>" class="icon icon-cog"><?php esc_html_e('Settings', 'sunnyjar'); ?></a></li>
                        </ul>
                    </li>
                    <li class="menu_user_logout"><a href="<?php echo esc_url(wp_logout_url(home_url('/'))); ?>" class="icon icon-logout"><?php esc_html_e('Logout', 'sunnyjar'); ?></a></li>
                    <?php
                }
            }

            if (in_array('cart', $top_panel_top_components) && function_exists('sunnyjar_exists_woocommerce') && sunnyjar_exists_woocommerce() && (sunnyjar_is_woocommerce_page() && sunnyjar_get_custom_option('show_cart')=='shop' || sunnyjar_get_custom_option('show_cart')=='always') && !(is_checkout() || is_cart() || defined('WOOCOMMERCE_CHECKOUT') || defined('WOOCOMMERCE_CART'))) {
                ?>
                <li class="menu_user_cart">
                    <?php get_template_part(sunnyjar_get_file_slug('templates/headers/_parts/contact-info-cart.php')); ?>
                </li>
                <?php
            }
            ?>

            </ul>
        <?php } ?>

    
    </div>
<?php } ?>