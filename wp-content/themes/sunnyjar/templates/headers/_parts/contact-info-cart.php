<?php
$cart_items = WC()->cart->get_cart_contents_count();
$cart_summa = strip_tags(WC()->cart->get_cart_subtotal());
if(is_checkout() || is_cart() || defined('WOOCOMMERCE_CHECKOUT') || defined('WOOCOMMERCE_CART')){
    $tag = "span";
} else {
    $tag = "a";
}
?>
<<?php echo esc_attr($tag); ?> href="#" class="top_panel_cart_button" data-items="<?php echo esc_attr($cart_items); ?>" data-summa="<?php echo esc_attr($cart_summa); ?>">
	<div class="contact_icon icon-icon_cart"></div>
    <div>
        <span class="contact_label contact_cart_label"><?php esc_html_e('Cart: ', 'sunnyjar'); ?></span>
        <span class="contact_cart_totals contact_cart_label">
            <span class="cart_items"><?php
                echo esc_html($cart_items) . ' ' . ($cart_items == 1 ? esc_html__('Item', 'sunnyjar') : esc_html__('Items', 'sunnyjar'));
            ?>
        </span>
	    </span>
        <div class="cart_summa"><?php sunnyjar_show_layout($cart_summa); ?></div>
    </div>

</<?php echo esc_attr($tag); ?>>
<ul class="widget_area sidebar_cart sidebar"><li>
	<?php
	do_action( 'before_sidebar' );
	sunnyjar_storage_set('current_sidebar', 'cart');
    if ( !dynamic_sidebar( 'sidebar-cart' ) ) {
		the_widget( 'WC_Widget_Cart', 'title=&hide_if_empty=1' );
	}
	?>
</li></ul>