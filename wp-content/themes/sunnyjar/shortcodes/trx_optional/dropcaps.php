<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_dropcaps_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_dropcaps_theme_setup' );
	function sunnyjar_sc_dropcaps_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_dropcaps_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_dropcaps_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

//[trx_dropcaps id="unique_id" style="1-6"]paragraph text[/trx_dropcaps]

if (!function_exists('sunnyjar_sc_dropcaps')) {	
	function sunnyjar_sc_dropcaps($atts, $content=null){
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "1",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= sunnyjar_get_css_dimensions_from_values($width, $height);
		$style = min(3, max(1, $style));
		$content = do_shortcode(str_replace(array('[vc_column_text]', '[/vc_column_text]'), array('', ''), $content));
		$output = sunnyjar_substr($content, 0, 1) == '<' 
			? $content 
			: '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_dropcaps sc_dropcaps_style_' . esc_attr($style) . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
				. ($css ? ' style="'.esc_attr($css).'"' : '')
				. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
				. '>' 
					. '<span class="sc_dropcaps_item">' . trim(sunnyjar_substr($content, 0, 1)) . '</span>' . trim(sunnyjar_substr($content, 1))
			. '</div>';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_dropcaps', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_dropcaps', 'sunnyjar_sc_dropcaps');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_dropcaps_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_dropcaps_reg_shortcodes');
	function sunnyjar_sc_dropcaps_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_dropcaps", array(
			"title" => esc_html__("Dropcaps", 'sunnyjar'),
			"desc" => wp_kses_data( __("Make first letter as dropcaps", 'sunnyjar') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"style" => array(
					"title" => esc_html__("Style", 'sunnyjar'),
					"desc" => wp_kses_data( __("Dropcaps style", 'sunnyjar') ),
					"value" => "1",
					"type" => "checklist",
					"options" => sunnyjar_get_list_styles(1, 3)
				),
				"_content_" => array(
					"title" => esc_html__("Paragraph content", 'sunnyjar'),
					"desc" => wp_kses_data( __("Paragraph with dropcaps content", 'sunnyjar') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"width" => sunnyjar_shortcodes_width(),
				"height" => sunnyjar_shortcodes_height(),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_dropcaps_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_dropcaps_reg_shortcodes_vc');
	function sunnyjar_sc_dropcaps_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_dropcaps",
			"name" => esc_html__("Dropcaps", 'sunnyjar'),
			"description" => wp_kses_data( __("Make first letter of the text as dropcaps", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_dropcaps',
			"class" => "trx_sc_container trx_sc_dropcaps",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Style", 'sunnyjar'),
					"description" => wp_kses_data( __("Dropcaps style", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(sunnyjar_get_list_styles(1, 3)),
					"type" => "dropdown"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_vc_width(),
				sunnyjar_vc_height(),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			)
		
		) );
		
		class WPBakeryShortCode_Trx_Dropcaps extends SUNNYJAR_VC_ShortCodeContainer {}
	}
}
?>