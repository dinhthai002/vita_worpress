<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_tooltip_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_tooltip_theme_setup' );
	function sunnyjar_sc_tooltip_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_tooltip_reg_shortcodes');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_tooltip id="unique_id" title="Tooltip text here"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/tooltip]
*/

if (!function_exists('sunnyjar_sc_tooltip')) {	
	function sunnyjar_sc_tooltip($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_tooltip_parent'. (!empty($class) ? ' '.esc_attr($class) : '').'"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
					. '>'
						. do_shortcode($content)
						. '<span class="sc_tooltip">' . ($title) . '</span>'
					. '</span>';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_tooltip', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_tooltip', 'sunnyjar_sc_tooltip');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_tooltip_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_tooltip_reg_shortcodes');
	function sunnyjar_sc_tooltip_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_tooltip", array(
			"title" => esc_html__("Tooltip", 'sunnyjar'),
			"desc" => wp_kses_data( __("Create tooltip for selected text", 'sunnyjar') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"title" => array(
					"title" => esc_html__("Title", 'sunnyjar'),
					"desc" => wp_kses_data( __("Tooltip title (required)", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Tipped content", 'sunnyjar'),
					"desc" => wp_kses_data( __("Highlighted content with tooltip", 'sunnyjar') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}
?>