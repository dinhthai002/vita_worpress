<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_gap_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_gap_theme_setup' );
	function sunnyjar_sc_gap_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_gap_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_gap_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

//[trx_gap]Fullwidth content[/trx_gap]

if (!function_exists('sunnyjar_sc_gap')) {	
	function sunnyjar_sc_gap($atts, $content = null) {
		if (sunnyjar_in_shortcode_blogger()) return '';
		$output = sunnyjar_gap_start() . do_shortcode($content) . sunnyjar_gap_end();
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_gap', $atts, $content);
	}
	sunnyjar_require_shortcode("trx_gap", "sunnyjar_sc_gap");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_gap_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_gap_reg_shortcodes');
	function sunnyjar_sc_gap_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_gap", array(
			"title" => esc_html__("Gap", 'sunnyjar'),
			"desc" => wp_kses_data( __("Insert gap (fullwidth area) in the post content. Attention! Use the gap only in the posts (pages) without left or right sidebar", 'sunnyjar') ),
			"decorate" => true,
			"container" => true,
			"params" => array(
				"_content_" => array(
					"title" => esc_html__("Gap content", 'sunnyjar'),
					"desc" => wp_kses_data( __("Gap inner content", 'sunnyjar') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				)
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_gap_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_gap_reg_shortcodes_vc');
	function sunnyjar_sc_gap_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_gap",
			"name" => esc_html__("Gap", 'sunnyjar'),
			"description" => wp_kses_data( __("Insert gap (fullwidth area) in the post content", 'sunnyjar') ),
			"category" => esc_html__('Structure', 'sunnyjar'),
			'icon' => 'icon_trx_gap',
			"class" => "trx_sc_collection trx_sc_gap",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => false,
			"params" => array(
				/*
				array(
					"param_name" => "content",
					"heading" => esc_html__("Gap content", 'sunnyjar'),
					"description" => wp_kses_data( __("Gap inner content", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				)
				*/
			)
		) );
		
		class WPBakeryShortCode_Trx_Gap extends SUNNYJAR_VC_ShortCodeCollection {}
	}
}
?>