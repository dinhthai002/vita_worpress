<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_button_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_button_theme_setup' );
	function sunnyjar_sc_button_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_button_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_button_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_button id="unique_id" type="square|round" fullsize="0|1" style="global|light|dark" size="mini|medium|big|huge|banner" icon="icon-name" link='#' target='']Button caption[/trx_button]
*/

if (!function_exists('sunnyjar_sc_button')) {	
	function sunnyjar_sc_button($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "square",
			"style" => "filled",
			"size" => "small",
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			"link" => "",
			"target" => "",
			"align" => "",
			"rel" => "",
			"popup" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= sunnyjar_get_css_dimensions_from_values($width, $height)
			. ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
			. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) . '; border-color:'. esc_attr($bg_color) .';' : '');
		if (sunnyjar_param_is_on($popup)) sunnyjar_enqueue_popup('magnific');
		$output = '<a href="' . (empty($link) ? '#' : $link) . '"'
			. (!empty($target) ? ' target="'.esc_attr($target).'"' : '')
			. (!empty($rel) ? ' rel="'.esc_attr($rel).'"' : '')
			. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
			. ' class="sc_button sc_button_' . esc_attr($type) 
					. ' sc_button_style_' . esc_attr($style) 
					. ' sc_button_size_' . esc_attr($size)
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($icon!='' ? '  sc_button_iconed '. esc_attr($icon) : '') 
					. (sunnyjar_param_is_on($popup) ? ' sc_popup_link' : '') 
					. '"'
			. ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
			. do_shortcode($content)
			. '</a>';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_button', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_button', 'sunnyjar_sc_button');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_button_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_button_reg_shortcodes');
	function sunnyjar_sc_button_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_button", array(
			"title" => esc_html__("Button", 'sunnyjar'),
			"desc" => wp_kses_data( __("Button with link", 'sunnyjar') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"_content_" => array(
					"title" => esc_html__("Caption", 'sunnyjar'),
					"desc" => wp_kses_data( __("Button caption", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"type" => array(
					"title" => esc_html__("Button's shape", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select button's shape", 'sunnyjar') ),
					"value" => "square",
					"size" => "medium",
					"options" => array(
						'square' => esc_html__('Square', 'sunnyjar'),
						'round' => esc_html__('Round', 'sunnyjar')
					),
					"type" => "switch"
				), 
				"style" => array(
					"title" => esc_html__("Button's style", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select button's style", 'sunnyjar') ),
					"value" => "default",
					"dir" => "horizontal",
					"options" => array(
						'filled' => esc_html__('Filled', 'sunnyjar'),
                        'filled_2' => esc_html__('Filled 2', 'sunnyjar'),
                        'border' => esc_html__('Border', 'sunnyjar')
                    ),
					"type" => "checklist"
				), 
				"size" => array(
					"title" => esc_html__("Button's size", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select button's size", 'sunnyjar') ),
					"value" => "small",
					"dir" => "horizontal",
					"options" => array(
						'small' => esc_html__('Small', 'sunnyjar'),
						'medium' => esc_html__('Medium', 'sunnyjar'),
						'large' => esc_html__('Large', 'sunnyjar')
					),
					"type" => "checklist"
				), 
				"icon" => array(
					"title" => esc_html__("Button's icon",  'sunnyjar'),
					"desc" => wp_kses_data( __('Select icon for the title from Fontello icons set',  'sunnyjar') ),
					"value" => "",
					"type" => "icons",
					"options" => sunnyjar_get_sc_param('icons')
				),
				"color" => array(
					"title" => esc_html__("Button's text color", 'sunnyjar'),
					"desc" => wp_kses_data( __("Any color for button's caption", 'sunnyjar') ),
					"std" => "",
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Button's backcolor", 'sunnyjar'),
					"desc" => wp_kses_data( __("Any color for button's background", 'sunnyjar') ),
					"value" => "",
					"type" => "color"
				),
				"align" => array(
					"title" => esc_html__("Button's alignment", 'sunnyjar'),
					"desc" => wp_kses_data( __("Align button to left, center or right", 'sunnyjar') ),
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('align')
				), 
				"link" => array(
					"title" => esc_html__("Link URL", 'sunnyjar'),
					"desc" => wp_kses_data( __("URL for link on button click", 'sunnyjar') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"target" => array(
					"title" => esc_html__("Link target", 'sunnyjar'),
					"desc" => wp_kses_data( __("Target for link on button click", 'sunnyjar') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "",
					"type" => "text"
				),
				"popup" => array(
					"title" => esc_html__("Open link in popup", 'sunnyjar'),
					"desc" => wp_kses_data( __("Open link target in popup window", 'sunnyjar') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "no",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				), 
				"rel" => array(
					"title" => esc_html__("Rel attribute", 'sunnyjar'),
					"desc" => wp_kses_data( __("Rel attribute for button's link (if need)", 'sunnyjar') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "",
					"type" => "text"
				),
				"width" => sunnyjar_shortcodes_width(),
				"height" => sunnyjar_shortcodes_height(),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_button_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_button_reg_shortcodes_vc');
	function sunnyjar_sc_button_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_button",
			"name" => esc_html__("Button", 'sunnyjar'),
			"description" => wp_kses_data( __("Button with link", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_button',
			"class" => "trx_sc_single trx_sc_button",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "content",
					"heading" => esc_html__("Caption", 'sunnyjar'),
					"description" => wp_kses_data( __("Button caption", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "type",
					"heading" => esc_html__("Button's shape", 'sunnyjar'),
					"description" => wp_kses_data( __("Select button's shape", 'sunnyjar') ),
					"class" => "",
					"value" => array(
						esc_html__('Square', 'sunnyjar') => 'square',
						esc_html__('Round', 'sunnyjar') => 'round'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "style",
					"heading" => esc_html__("Button's style", 'sunnyjar'),
					"description" => wp_kses_data( __("Select button's style", 'sunnyjar') ),
					"class" => "",
					"value" => array(
						esc_html__('Filled', 'sunnyjar') => 'filled',
                        esc_html__('Filled 2', 'sunnyjar') => 'filled_2',
                        esc_html__('Border', 'sunnyjar') => 'border'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "size",
					"heading" => esc_html__("Button's size", 'sunnyjar'),
					"description" => wp_kses_data( __("Select button's size", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Small', 'sunnyjar') => 'small',
						esc_html__('Medium', 'sunnyjar') => 'medium',
						esc_html__('Large', 'sunnyjar') => 'large'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Button's icon", 'sunnyjar'),
					"description" => wp_kses_data( __("Select icon for the title from Fontello icons set", 'sunnyjar') ),
					"class" => "",
					"value" => sunnyjar_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Button's text color", 'sunnyjar'),
					"description" => wp_kses_data( __("Any color for button's caption", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Button's backcolor", 'sunnyjar'),
					"description" => wp_kses_data( __("Any color for button's background", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Button's alignment", 'sunnyjar'),
					"description" => wp_kses_data( __("Align button to left, center or right", 'sunnyjar') ),
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link URL", 'sunnyjar'),
					"description" => wp_kses_data( __("URL for the link on button click", 'sunnyjar') ),
					"class" => "",
					"group" => esc_html__('Link', 'sunnyjar'),
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "target",
					"heading" => esc_html__("Link target", 'sunnyjar'),
					"description" => wp_kses_data( __("Target for the link on button click", 'sunnyjar') ),
					"class" => "",
					"group" => esc_html__('Link', 'sunnyjar'),
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "popup",
					"heading" => esc_html__("Open link in popup", 'sunnyjar'),
					"description" => wp_kses_data( __("Open link target in popup window", 'sunnyjar') ),
					"class" => "",
					"group" => esc_html__('Link', 'sunnyjar'),
					"value" => array(esc_html__('Open in popup', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "rel",
					"heading" => esc_html__("Rel attribute", 'sunnyjar'),
					"description" => wp_kses_data( __("Rel attribute for the button's link (if need", 'sunnyjar') ),
					"class" => "",
					"group" => esc_html__('Link', 'sunnyjar'),
					"value" => "",
					"type" => "textfield"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_vc_width(),
				sunnyjar_vc_height(),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Button extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>