<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_search_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_search_theme_setup' );
	function sunnyjar_sc_search_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_search_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_search_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_search id="unique_id" open="yes|no"]
*/

if (!function_exists('sunnyjar_sc_search')) {	
	function sunnyjar_sc_search($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "regular",
			"state" => "fixed",
			"scheme" => "original",
			"ajax" => "",
			"title" => esc_html__('Search', 'sunnyjar'),
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		if (empty($ajax)) $ajax = sunnyjar_get_theme_option('use_ajax_search');
		// Load core messages
		sunnyjar_enqueue_messages();
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') . ' class="search_wrap search_style_'.esc_attr($style).' search_state_'.esc_attr($state)
						. (sunnyjar_param_is_on($ajax) ? ' search_ajax' : '')
						. ($class ? ' '.esc_attr($class) : '')
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
					. '>
						<div class="search_form_wrap">
							<form role="search" method="get" class="search_form" action="' . esc_url(home_url('/')) . '">
								<button type="submit" class="search_submit icon-search-light" title="' . ($state=='closed' ? esc_attr__('Open search', 'sunnyjar') : esc_attr__('Start search', 'sunnyjar')) . '"></button>
								<input type="text" class="search_field" placeholder="' . esc_attr($title) . '" value="' . esc_attr(get_search_query()) . '" name="s" />
							</form>
						</div>
						<div class="search_results widget_area' . ($scheme && !sunnyjar_param_is_off($scheme) && !sunnyjar_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') . '"><a class="search_results_close icon-cancel"></a><div class="search_results_content"></div></div>
				</div>';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_search', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_search', 'sunnyjar_sc_search');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_search_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_search_reg_shortcodes');
	function sunnyjar_sc_search_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_search", array(
			"title" => esc_html__("Search", 'sunnyjar'),
			"desc" => wp_kses_data( __("Show search form", 'sunnyjar') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"style" => array(
					"title" => esc_html__("Style", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select style to display search field", 'sunnyjar') ),
					"value" => "regular",
					"options" => array(
						"regular" => esc_html__('Regular', 'sunnyjar'),
						"rounded" => esc_html__('Rounded', 'sunnyjar')
					),
					"type" => "checklist"
				),
				"state" => array(
					"title" => esc_html__("State", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select search field initial state", 'sunnyjar') ),
					"value" => "fixed",
					"options" => array(
						"fixed"  => esc_html__('Fixed',  'sunnyjar'),
						"opened" => esc_html__('Opened', 'sunnyjar'),
						"closed" => esc_html__('Closed', 'sunnyjar')
					),
					"type" => "checklist"
				),
				"title" => array(
					"title" => esc_html__("Title", 'sunnyjar'),
					"desc" => wp_kses_data( __("Title (placeholder) for the search field", 'sunnyjar') ),
					"value" => esc_html__("Search &hellip;", 'sunnyjar'),
					"type" => "text"
				),
				"ajax" => array(
					"title" => esc_html__("AJAX", 'sunnyjar'),
					"desc" => wp_kses_data( __("Search via AJAX or reload page", 'sunnyjar') ),
					"value" => "yes",
					"options" => sunnyjar_get_sc_param('yes_no'),
					"type" => "switch"
				),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_search_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_search_reg_shortcodes_vc');
	function sunnyjar_sc_search_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_search",
			"name" => esc_html__("Search form", 'sunnyjar'),
			"description" => wp_kses_data( __("Insert search form", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_search',
			"class" => "trx_sc_single trx_sc_search",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Style", 'sunnyjar'),
					"description" => wp_kses_data( __("Select style to display search field", 'sunnyjar') ),
					"class" => "",
					"value" => array(
						esc_html__('Regular', 'sunnyjar') => "regular",
						esc_html__('Flat', 'sunnyjar') => "flat"
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "state",
					"heading" => esc_html__("State", 'sunnyjar'),
					"description" => wp_kses_data( __("Select search field initial state", 'sunnyjar') ),
					"class" => "",
					"value" => array(
						esc_html__('Fixed', 'sunnyjar')  => "fixed",
						esc_html__('Opened', 'sunnyjar') => "opened",
						esc_html__('Closed', 'sunnyjar') => "closed"
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'sunnyjar'),
					"description" => wp_kses_data( __("Title (placeholder) for the search field", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => esc_html__("Search &hellip;", 'sunnyjar'),
					"type" => "textfield"
				),
				array(
					"param_name" => "ajax",
					"heading" => esc_html__("AJAX", 'sunnyjar'),
					"description" => wp_kses_data( __("Search via AJAX or reload page", 'sunnyjar') ),
					"class" => "",
					"value" => array(esc_html__('Use AJAX search', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Search extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>