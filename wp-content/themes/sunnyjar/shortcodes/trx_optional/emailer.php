<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_emailer_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_emailer_theme_setup' );
	function sunnyjar_sc_emailer_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_emailer_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_emailer_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

//[trx_emailer group=""]

if (!function_exists('sunnyjar_sc_emailer')) {	
	function sunnyjar_sc_emailer($atts, $content = null) {
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"group" => "",
			"open" => "yes",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => "",
			"width" => "",
			"height" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= sunnyjar_get_css_dimensions_from_values($width, $height);
		// Load core messages
		sunnyjar_enqueue_messages();
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '')
					. ' class="sc_emailer' . ($align && $align!='none' ? ' align' . esc_attr($align) : '') . (sunnyjar_param_is_on($open) ? ' sc_emailer_opened' : '') . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
					. ($css ? ' style="'.esc_attr($css).'"' : '') 
					. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
					. '>'
				. '<form class="sc_emailer_form">'
				. '<input type="text" class="sc_emailer_input" name="email" value="" placeholder="'.esc_attr__('Enter you email', 'sunnyjar').'">'
				. '<a href="#" class="sc_emailer_button" title="'.esc_attr__('Submit', 'sunnyjar').'" data-group="'.esc_attr($group ? $group : esc_html__('E-mailer subscription', 'sunnyjar')).'"><span class="icon-mail"></span>' . esc_html__('Submit', 'sunnyjar') . '</a>'
				. '</form>'
			. '</div>';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_emailer', $atts, $content);
	}
	sunnyjar_require_shortcode("trx_emailer", "sunnyjar_sc_emailer");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_emailer_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_emailer_reg_shortcodes');
	function sunnyjar_sc_emailer_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_emailer", array(
			"title" => esc_html__("E-mail collector", 'sunnyjar'),
			"desc" => wp_kses_data( __("Collect the e-mail address into specified group", 'sunnyjar') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"group" => array(
					"title" => esc_html__("Group", 'sunnyjar'),
					"desc" => wp_kses_data( __("The name of group to collect e-mail address", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"open" => array(
					"title" => esc_html__("Open", 'sunnyjar'),
					"desc" => wp_kses_data( __("Initially open the input field on show object", 'sunnyjar') ),
					"divider" => true,
					"value" => "yes",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'sunnyjar'),
					"desc" => wp_kses_data( __("Align object to left, center or right", 'sunnyjar') ),
					"divider" => true,
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('align')
				), 
				"width" => sunnyjar_shortcodes_width(),
				"height" => sunnyjar_shortcodes_height(),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_emailer_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_emailer_reg_shortcodes_vc');
	function sunnyjar_sc_emailer_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_emailer",
			"name" => esc_html__("E-mail collector", 'sunnyjar'),
			"description" => wp_kses_data( __("Collect e-mails into specified group", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_emailer',
			"class" => "trx_sc_single trx_sc_emailer",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "group",
					"heading" => esc_html__("Group", 'sunnyjar'),
					"description" => wp_kses_data( __("The name of group to collect e-mail address", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "open",
					"heading" => esc_html__("Opened", 'sunnyjar'),
					"description" => wp_kses_data( __("Initially open the input field on show object", 'sunnyjar') ),
					"class" => "",
					"value" => array(esc_html__('Initially opened', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'sunnyjar'),
					"description" => wp_kses_data( __("Align field to left, center or right", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('align')),
					"type" => "dropdown"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_vc_width(),
				sunnyjar_vc_height(),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Emailer extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>