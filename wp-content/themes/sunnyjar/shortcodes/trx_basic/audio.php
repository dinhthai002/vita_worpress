<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_audio_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_audio_theme_setup' );
	function sunnyjar_sc_audio_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_audio_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_audio_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_audio url="http://trex2.themerex.dnw/wp-content/uploads/2014/12/Dream-Music-Relax.mp3" image="http://trex2.themerex.dnw/wp-content/uploads/2014/10/post_audio.jpg" title="Insert Audio Title Here" author="Lily Hunter" controls="show" autoplay="off"]
*/

if (!function_exists('sunnyjar_sc_audio')) {	
	function sunnyjar_sc_audio($atts, $content = null) {
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"author" => "",
			"image" => "",
			"mp3" => '',
			"wav" => '',
			"src" => '',
			"url" => '',
			"align" => '',
			"controls" => "",
			"autoplay" => "",
			"frame" => "on",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => '',
			"height" => '',
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		if ($src=='' && $url=='' && isset($atts[0])) {
			$src = $atts[0];
		}
		if ($src=='') {
			if ($url) $src = $url;
			else if ($mp3) $src = $mp3;
			else if ($wav) $src = $wav;
		}
		if ($image > 0) {
			$attach = wp_get_attachment_image_src( $image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$image = $attach[0];
		}
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$data = ($title != ''  ? ' data-title="'.esc_attr($title).'"'   : '')
				. ($author != '' ? ' data-author="'.esc_attr($author).'"' : '')
				. ($image != ''  ? ' data-image="'.esc_url($image).'"'   : '')
				. ($align && $align!='none' ? ' data-align="'.esc_attr($align).'"' : '')
				. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '');
		$audio = '<audio'
			. ($id ? ' id="'.esc_attr($id).'"' : '')
			. ' class="sc_audio' . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. ' src="'.esc_url($src).'"'
			. (sunnyjar_param_is_on($controls) ? ' controls="controls"' : '')
			. (sunnyjar_param_is_on($autoplay) && is_single() ? ' autoplay="autoplay"' : '')
			. (!empty($width) ? ' width="'.esc_attr($width).'"' : '')
			. (!empty($height) ? ' height="'.esc_attr($height).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. ($data)
			. '></audio>';
		if ( sunnyjar_get_custom_option('substitute_audio')=='no') {
			if (sunnyjar_param_is_on($frame)) {
				$audio = sunnyjar_get_audio_frame($audio, $image, $s);
			}
		} else {
			if ((isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')) {
				$audio = sunnyjar_substitute_audio($audio, false);
			}
		}
		if (sunnyjar_get_theme_option('use_mediaelement')=='yes')
			sunnyjar_enqueue_script('wp-mediaelement');
		return apply_filters('sunnyjar_shortcode_output', $audio, 'trx_audio', $atts, $content);
	}
	sunnyjar_require_shortcode("trx_audio", "sunnyjar_sc_audio");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_audio_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_audio_reg_shortcodes');
	function sunnyjar_sc_audio_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_audio", array(
			"title" => esc_html__("Audio", 'sunnyjar'),
			"desc" => wp_kses_data( __("Insert audio player", 'sunnyjar') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"url" => array(
					"title" => esc_html__("URL for audio file", 'sunnyjar'),
					"desc" => wp_kses_data( __("URL for audio file", 'sunnyjar') ),
					"readonly" => false,
					"value" => "",
					"type" => "media",
					"before" => array(
						'title' => esc_html__('Choose audio', 'sunnyjar'),
						'action' => 'media_upload',
						'type' => 'audio',
						'multiple' => false,
						'linked_field' => '',
						'captions' => array( 	
							'choose' => esc_html__('Choose audio file', 'sunnyjar'),
							'update' => esc_html__('Select audio file', 'sunnyjar')
						)
					),
					"after" => array(
						'icon' => 'icon-cancel',
						'action' => 'media_reset'
					)
				),
				"image" => array(
					"title" => esc_html__("Cover image", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site for audio cover", 'sunnyjar') ),
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"title" => array(
					"title" => esc_html__("Title", 'sunnyjar'),
					"desc" => wp_kses_data( __("Title of the audio file", 'sunnyjar') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"author" => array(
					"title" => esc_html__("Author", 'sunnyjar'),
					"desc" => wp_kses_data( __("Author of the audio file", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"controls" => array(
					"title" => esc_html__("Show controls", 'sunnyjar'),
					"desc" => wp_kses_data( __("Show controls in audio player", 'sunnyjar') ),
					"divider" => true,
					"size" => "medium",
					"value" => "show",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('show_hide')
				),
				"autoplay" => array(
					"title" => esc_html__("Autoplay audio", 'sunnyjar'),
					"desc" => wp_kses_data( __("Autoplay audio on page load", 'sunnyjar') ),
					"value" => "off",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('on_off')
				),
				"align" => array(
					"title" => esc_html__("Align", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select block alignment", 'sunnyjar') ),
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('align')
				),
				"width" => sunnyjar_shortcodes_width(),
				"height" => sunnyjar_shortcodes_height(),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_audio_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_audio_reg_shortcodes_vc');
	function sunnyjar_sc_audio_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_audio",
			"name" => esc_html__("Audio", 'sunnyjar'),
			"description" => wp_kses_data( __("Insert audio player", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_audio',
			"class" => "trx_sc_single trx_sc_audio",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "url",
					"heading" => esc_html__("URL for audio file", 'sunnyjar'),
					"description" => wp_kses_data( __("Put here URL for audio file", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "image",
					"heading" => esc_html__("Cover image", 'sunnyjar'),
					"description" => wp_kses_data( __("Select or upload image or write URL from other site for audio cover", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'sunnyjar'),
					"description" => wp_kses_data( __("Title of the audio file", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "author",
					"heading" => esc_html__("Author", 'sunnyjar'),
					"description" => wp_kses_data( __("Author of the audio file", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "controls",
					"heading" => esc_html__("Controls", 'sunnyjar'),
					"description" => wp_kses_data( __("Show/hide controls", 'sunnyjar') ),
					"class" => "",
					"value" => array("Hide controls" => "hide" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "autoplay",
					"heading" => esc_html__("Autoplay", 'sunnyjar'),
					"description" => wp_kses_data( __("Autoplay audio on page load", 'sunnyjar') ),
					"class" => "",
					"value" => array("Autoplay" => "on" ),
					"type" => "checkbox"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'sunnyjar'),
					"description" => wp_kses_data( __("Select block alignment", 'sunnyjar') ),
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('align')),
					"type" => "dropdown"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_vc_width(),
				sunnyjar_vc_height(),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			),
		) );
		
		class WPBakeryShortCode_Trx_Audio extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>