<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_infobox_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_infobox_theme_setup' );
	function sunnyjar_sc_infobox_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_infobox_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_infobox_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_infobox id="unique_id" style="regular|info|success|error|result" static="0|1"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_infobox]
*/

if (!function_exists('sunnyjar_sc_infobox')) {	
	function sunnyjar_sc_infobox($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"style" => "regular",
			"closeable" => "no",
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
			. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) .';' : '');
		if (empty($icon)) {
			if ($icon=='none')
				$icon = '';
			else if ($style=='regular')
				$icon = 'icon-cog';
			else if ($style=='success')
				$icon = 'icon-check';
			else if ($style=='error')
				$icon = 'icon-attention';
			else if ($style=='info')
				$icon = 'icon-info';
		}
		$content = do_shortcode($content);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_infobox sc_infobox_style_' . esc_attr($style) 
					. (sunnyjar_param_is_on($closeable) ? ' sc_infobox_closeable' : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. ($icon!='' && !sunnyjar_param_is_inherit($icon) ? ' sc_infobox_iconed '. esc_attr($icon) : '') 
					. '"'
				. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>'
				. trim($content)
				. '</div>';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_infobox', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_infobox', 'sunnyjar_sc_infobox');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_infobox_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_infobox_reg_shortcodes');
	function sunnyjar_sc_infobox_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_infobox", array(
			"title" => esc_html__("Infobox", 'sunnyjar'),
			"desc" => wp_kses_data( __("Insert infobox into your post (page)", 'sunnyjar') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"style" => array(
					"title" => esc_html__("Style", 'sunnyjar'),
					"desc" => wp_kses_data( __("Infobox style", 'sunnyjar') ),
					"value" => "regular",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => array(
						'regular' => esc_html__('Regular', 'sunnyjar'),
						'info' => esc_html__('Info', 'sunnyjar'),
						'success' => esc_html__('Success', 'sunnyjar'),
						'error' => esc_html__('Error', 'sunnyjar')
					)
				),
				"closeable" => array(
					"title" => esc_html__("Closeable box", 'sunnyjar'),
					"desc" => wp_kses_data( __("Create closeable box (with close button)", 'sunnyjar') ),
					"value" => "no",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				),
				"icon" => array(
					"title" => esc_html__("Custom icon",  'sunnyjar'),
					"desc" => wp_kses_data( __('Select icon for the infobox from Fontello icons set. If empty - use default icon',  'sunnyjar') ),
					"value" => "",
					"type" => "icons",
					"options" => sunnyjar_get_sc_param('icons')
				),
				"color" => array(
					"title" => esc_html__("Text color", 'sunnyjar'),
					"desc" => wp_kses_data( __("Any color for text and headers", 'sunnyjar') ),
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Background color", 'sunnyjar'),
					"desc" => wp_kses_data( __("Any background color for this infobox", 'sunnyjar') ),
					"value" => "",
					"type" => "color"
				),
				"_content_" => array(
					"title" => esc_html__("Infobox content", 'sunnyjar'),
					"desc" => wp_kses_data( __("Content for infobox", 'sunnyjar') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_infobox_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_infobox_reg_shortcodes_vc');
	function sunnyjar_sc_infobox_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_infobox",
			"name" => esc_html__("Infobox", 'sunnyjar'),
			"description" => wp_kses_data( __("Box with info or error message", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_infobox',
			"class" => "trx_sc_container trx_sc_infobox",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Style", 'sunnyjar'),
					"description" => wp_kses_data( __("Infobox style", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
							esc_html__('Regular', 'sunnyjar') => 'regular',
							esc_html__('Info', 'sunnyjar') => 'info',
							esc_html__('Success', 'sunnyjar') => 'success',
							esc_html__('Error', 'sunnyjar') => 'error',
							esc_html__('Result', 'sunnyjar') => 'result'
						),
					"type" => "dropdown"
				),
				array(
					"param_name" => "closeable",
					"heading" => esc_html__("Closeable", 'sunnyjar'),
					"description" => wp_kses_data( __("Create closeable box (with close button)", 'sunnyjar') ),
					"class" => "",
					"value" => array(esc_html__('Close button', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Custom icon", 'sunnyjar'),
					"description" => wp_kses_data( __("Select icon for the infobox from Fontello icons set. If empty - use default icon", 'sunnyjar') ),
					"class" => "",
					"value" => sunnyjar_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Text color", 'sunnyjar'),
					"description" => wp_kses_data( __("Any color for the text and headers", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Background color", 'sunnyjar'),
					"description" => wp_kses_data( __("Any background color for this infobox", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				/*
				array(
					"param_name" => "content",
					"heading" => esc_html__("Message text", 'sunnyjar'),
					"description" => wp_kses_data( __("Message for the infobox", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				*/
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextContainerView'
		) );
		
		class WPBakeryShortCode_Trx_Infobox extends SUNNYJAR_VC_ShortCodeContainer {}
	}
}
?>