<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_reviews_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_reviews_theme_setup' );
	function sunnyjar_sc_reviews_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_reviews_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_reviews_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_reviews]
*/

if (!function_exists('sunnyjar_sc_reviews')) {	
	function sunnyjar_sc_reviews($atts, $content = null) {
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"align" => "right",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$output = sunnyjar_param_is_off(sunnyjar_get_custom_option('show_sidebar_main'))
			? '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
						. ' class="sc_reviews'
							. ($align && $align!='none' ? ' align'.esc_attr($align) : '')
							. ($class ? ' '.esc_attr($class) : '')
							. '"'
						. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
						. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
						. '>'
					. trim(sunnyjar_get_reviews_placeholder())
					. '</div>'
			: '';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_reviews', $atts, $content);
	}
	sunnyjar_require_shortcode("trx_reviews", "sunnyjar_sc_reviews");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_reviews_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_reviews_reg_shortcodes');
	function sunnyjar_sc_reviews_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_reviews", array(
			"title" => esc_html__("Reviews", 'sunnyjar'),
			"desc" => wp_kses_data( __("Insert reviews block in the single post", 'sunnyjar') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"align" => array(
					"title" => esc_html__("Alignment", 'sunnyjar'),
					"desc" => wp_kses_data( __("Align counter to left, center or right", 'sunnyjar') ),
					"divider" => true,
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('align')
				), 
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_reviews_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_reviews_reg_shortcodes_vc');
	function sunnyjar_sc_reviews_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_reviews",
			"name" => esc_html__("Reviews", 'sunnyjar'),
			"description" => wp_kses_data( __("Insert reviews block in the single post", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_reviews',
			"class" => "trx_sc_single trx_sc_reviews",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'sunnyjar'),
					"description" => wp_kses_data( __("Align counter to left, center or right", 'sunnyjar') ),
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('align')),
					"type" => "dropdown"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Reviews extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>