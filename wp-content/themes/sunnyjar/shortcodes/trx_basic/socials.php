<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_socials_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_socials_theme_setup' );
	function sunnyjar_sc_socials_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_socials_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_socials_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_socials id="unique_id" size="small"]
	[trx_social_item name="facebook" url="profile url" icon="path for the icon"]
	[trx_social_item name="twitter" url="profile url"]
[/trx_socials]
*/

if (!function_exists('sunnyjar_sc_socials')) {	
	function sunnyjar_sc_socials($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"size" => "small",		// tiny | small | medium | large
			"shape" => "square",	// round | square
			"type" => sunnyjar_get_theme_setting('socials_type'),	// icons | images
			"socials" => "",
			"custom" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		sunnyjar_storage_set('sc_social_data', array(
			'icons' => false,
            'type' => $type
            )
        );
		if (!empty($socials)) {
			$allowed = explode('|', $socials);
			$list = array();
			for ($i=0; $i<count($allowed); $i++) {
				$s = explode('=', $allowed[$i]);
				if (!empty($s[1])) {
					$list[] = array(
						'icon'	=> $type=='images' ? sunnyjar_get_socials_url($s[0]) : 'icon-'.trim($s[0]),
						'url'	=> $s[1]
						);
				}
			}
			if (count($list) > 0) sunnyjar_storage_set_array('sc_social_data', 'icons', $list);
		} else if (sunnyjar_param_is_off($custom))
			$content = do_shortcode($content);
		if (sunnyjar_storage_get_array('sc_social_data', 'icons')===false) sunnyjar_storage_set_array('sc_social_data', 'icons', sunnyjar_get_custom_option('social_icons'));
		$output = sunnyjar_prepare_socials(sunnyjar_storage_get_array('sc_social_data', 'icons'));
		$output = $output
			? '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_socials sc_socials_type_' . esc_attr($type) . ' sc_socials_shape_' . esc_attr($shape) . ' sc_socials_size_' . esc_attr($size) . (!empty($class) ? ' '.esc_attr($class) : '') . '"' 
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
				. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
				. '>' 
				. ($output)
				. '</div>'
			: '';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_socials', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_socials', 'sunnyjar_sc_socials');
}


if (!function_exists('sunnyjar_sc_social_item')) {	
	function sunnyjar_sc_social_item($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"name" => "",
			"url" => "",
			"icon" => ""
		), $atts)));
		if (!empty($name) && empty($icon)) {
			$type = sunnyjar_storage_get_array('sc_social_data', 'type');
			if ($type=='images') {
				if (file_exists(sunnyjar_get_socials_dir($name.'.png')))
					$icon = sunnyjar_get_socials_url($name.'.png');
			} else
				$icon = 'icon-'.esc_attr($name);
		}
		if (!empty($icon) && !empty($url)) {
			if (sunnyjar_storage_get_array('sc_social_data', 'icons')===false) sunnyjar_storage_set_array('sc_social_data', 'icons', array());
			sunnyjar_storage_set_array2('sc_social_data', 'icons', '', array(
				'icon' => $icon,
				'url' => $url
				)
			);
		}
		return '';
	}
	sunnyjar_require_shortcode('trx_social_item', 'sunnyjar_sc_social_item');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_socials_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_socials_reg_shortcodes');
	function sunnyjar_sc_socials_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_socials", array(
			"title" => esc_html__("Social icons", 'sunnyjar'),
			"desc" => wp_kses_data( __("List of social icons (with hovers)", 'sunnyjar') ),
			"decorate" => true,
			"container" => false,
			"params" => array(
				"type" => array(
					"title" => esc_html__("Icon's type", 'sunnyjar'),
					"desc" => wp_kses_data( __("Type of the icons - images or font icons", 'sunnyjar') ),
					"value" => sunnyjar_get_theme_setting('socials_type'),
					"options" => array(
						'icons' => esc_html__('Icons', 'sunnyjar'),
						'images' => esc_html__('Images', 'sunnyjar')
					),
					"type" => "checklist"
				), 
				"size" => array(
					"title" => esc_html__("Icon's size", 'sunnyjar'),
					"desc" => wp_kses_data( __("Size of the icons", 'sunnyjar') ),
					"value" => "small",
					"options" => sunnyjar_get_sc_param('sizes'),
					"type" => "checklist"
				), 
				"shape" => array(
					"title" => esc_html__("Icon's shape", 'sunnyjar'),
					"desc" => wp_kses_data( __("Shape of the icons", 'sunnyjar') ),
					"value" => "square",
					"options" => sunnyjar_get_sc_param('shapes'),
					"type" => "checklist"
				), 
				"socials" => array(
					"title" => esc_html__("Manual socials list", 'sunnyjar'),
					"desc" => wp_kses_data( __("Custom list of social networks. For example: twitter=http://twitter.com/my_profile|facebook=http://facebook.com/my_profile. If empty - use socials from Theme options.", 'sunnyjar') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"custom" => array(
					"title" => esc_html__("Custom socials", 'sunnyjar'),
					"desc" => wp_kses_data( __("Make custom icons from inner shortcodes (prepare it on tabs)", 'sunnyjar') ),
					"divider" => true,
					"value" => "no",
					"options" => sunnyjar_get_sc_param('yes_no'),
					"type" => "switch"
				),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			),
			"children" => array(
				"name" => "trx_social_item",
				"title" => esc_html__("Custom social item", 'sunnyjar'),
				"desc" => wp_kses_data( __("Custom social item: name, profile url and icon url", 'sunnyjar') ),
				"decorate" => false,
				"container" => false,
				"params" => array(
					"name" => array(
						"title" => esc_html__("Social name", 'sunnyjar'),
						"desc" => wp_kses_data( __("Name (slug) of the social network (twitter, facebook, linkedin, etc.)", 'sunnyjar') ),
						"value" => "",
						"type" => "text"
					),
					"url" => array(
						"title" => esc_html__("Your profile URL", 'sunnyjar'),
						"desc" => wp_kses_data( __("URL of your profile in specified social network", 'sunnyjar') ),
						"value" => "",
						"type" => "text"
					),
					"icon" => array(
						"title" => esc_html__("URL (source) for icon file", 'sunnyjar'),
						"desc" => wp_kses_data( __("Select or upload image or write URL from other site for the current social icon", 'sunnyjar') ),
						"readonly" => false,
						"value" => "",
						"type" => "media"
					)
				)
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_socials_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_socials_reg_shortcodes_vc');
	function sunnyjar_sc_socials_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_socials",
			"name" => esc_html__("Social icons", 'sunnyjar'),
			"description" => wp_kses_data( __("Custom social icons", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_socials',
			"class" => "trx_sc_collection trx_sc_socials",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"as_parent" => array('only' => 'trx_social_item'),
			"params" => array_merge(array(
				array(
					"param_name" => "type",
					"heading" => esc_html__("Icon's type", 'sunnyjar'),
					"description" => wp_kses_data( __("Type of the icons - images or font icons", 'sunnyjar') ),
					"class" => "",
					"std" => sunnyjar_get_theme_setting('socials_type'),
					"value" => array(
						esc_html__('Icons', 'sunnyjar') => 'icons',
						esc_html__('Images', 'sunnyjar') => 'images'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "size",
					"heading" => esc_html__("Icon's size", 'sunnyjar'),
					"description" => wp_kses_data( __("Size of the icons", 'sunnyjar') ),
					"class" => "",
					"std" => "small",
					"value" => array_flip(sunnyjar_get_sc_param('sizes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "shape",
					"heading" => esc_html__("Icon's shape", 'sunnyjar'),
					"description" => wp_kses_data( __("Shape of the icons", 'sunnyjar') ),
					"class" => "",
					"std" => "square",
					"value" => array_flip(sunnyjar_get_sc_param('shapes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "socials",
					"heading" => esc_html__("Manual socials list", 'sunnyjar'),
					"description" => wp_kses_data( __("Custom list of social networks. For example: twitter=http://twitter.com/my_profile|facebook=http://facebook.com/my_profile. If empty - use socials from Theme options.", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "custom",
					"heading" => esc_html__("Custom socials", 'sunnyjar'),
					"description" => wp_kses_data( __("Make custom icons from inner shortcodes (prepare it on tabs)", 'sunnyjar') ),
					"class" => "",
					"value" => array(esc_html__('Custom socials', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			))
		) );
		
		
		vc_map( array(
			"base" => "trx_social_item",
			"name" => esc_html__("Custom social item", 'sunnyjar'),
			"description" => wp_kses_data( __("Custom social item: name, profile url and icon url", 'sunnyjar') ),
			"show_settings_on_create" => true,
			"content_element" => true,
			"is_container" => false,
			'icon' => 'icon_trx_social_item',
			"class" => "trx_sc_single trx_sc_social_item",
			"as_child" => array('only' => 'trx_socials'),
			"as_parent" => array('except' => 'trx_socials'),
			"params" => array(
				array(
					"param_name" => "name",
					"heading" => esc_html__("Social name", 'sunnyjar'),
					"description" => wp_kses_data( __("Name (slug) of the social network (twitter, facebook, linkedin, etc.)", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "url",
					"heading" => esc_html__("Your profile URL", 'sunnyjar'),
					"description" => wp_kses_data( __("URL of your profile in specified social network", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("URL (source) for icon file", 'sunnyjar'),
					"description" => wp_kses_data( __("Select or upload image or write URL from other site for the current social icon", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				)
			)
		) );
		
		class WPBakeryShortCode_Trx_Socials extends SUNNYJAR_VC_ShortCodeCollection {}
		class WPBakeryShortCode_Trx_Social_Item extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>