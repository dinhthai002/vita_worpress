<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_section_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_section_theme_setup' );
	function sunnyjar_sc_section_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_section_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_section_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_section id="unique_id" class="class_name" style="css-styles" dedicated="yes|no"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_section]
*/

sunnyjar_storage_set('sc_section_dedicated', '');

if (!function_exists('sunnyjar_sc_section')) {	
	function sunnyjar_sc_section($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"dedicated" => "no",
			"align" => "none",
			"columns" => "none",
			"pan" => "no",
			"scroll" => "no",
			"scroll_dir" => "horizontal",
			"scroll_controls" => "hide",
			"color" => "",
			"scheme" => "",
			"bg_color" => "",
			"bg_image" => "",
			"bg_overlay" => "",
			"bg_texture" => "",
			"bg_tile" => "no",
			"bg_padding" => "yes",
			"font_size" => "",
			"font_weight" => "",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"position_caption" => "",
			"link_caption" => esc_html__('Learn more', 'sunnyjar'),
			"link" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		if ($bg_image > 0) {
			$attach = wp_get_attachment_image_src( $bg_image, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$bg_image = $attach[0];
		}
	
		if ($bg_overlay > 0) {
			if ($bg_color=='') $bg_color = sunnyjar_get_scheme_color('bg');
			$rgb = sunnyjar_hex2rgb($bg_color);
		}
	
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= ($color !== '' ? 'color:' . esc_attr($color) . ';' : '')
			.($bg_color !== '' && $bg_overlay==0 ? 'background-color:' . esc_attr($bg_color) . ';' : '')
			.($bg_image !== '' ? 'background-image:url(' . esc_url($bg_image) . ');'.(sunnyjar_param_is_on($bg_tile) ? 'background-repeat:repeat;' : 'background-repeat:no-repeat;background-size:cover;') : '')
			.(!sunnyjar_param_is_off($pan) ? 'position:relative;' : '')
			.($font_size != '' ? 'font-size:' . esc_attr(sunnyjar_prepare_css_value($font_size)) . '; line-height: 1.3em;' : '')
			.($font_weight != '' && !sunnyjar_param_is_inherit($font_weight) ? 'font-weight:' . esc_attr($font_weight) . ';' : '');
		$css_dim = sunnyjar_get_css_dimensions_from_values($width, $height);
		if ($bg_image == '' && $bg_color == '' && $bg_overlay==0 && $bg_texture==0 && sunnyjar_strlen($bg_texture)<2) $css .= $css_dim;
		
		$width  = sunnyjar_prepare_css_value($width);
		$height = sunnyjar_prepare_css_value($height);
	
		if ((!sunnyjar_param_is_off($scroll) || !sunnyjar_param_is_off($pan)) && empty($id)) $id = 'sc_section_'.str_replace('.', '', mt_rand());
	
		if (!sunnyjar_param_is_off($scroll)) sunnyjar_enqueue_slider();
	
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_section' 
					. ($position_caption ? ' align_captions_left' : '')
					. ($class ? ' ' . esc_attr($class) : '')
					. ($scheme && !sunnyjar_param_is_off($scheme) && !sunnyjar_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '')
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($columns) && $columns!='none' ? ' column-'.esc_attr($columns) : '') 
					. (sunnyjar_param_is_on($scroll) && !sunnyjar_param_is_off($scroll_controls) ? ' sc_scroll_controls sc_scroll_controls_'.esc_attr($scroll_dir).' sc_scroll_controls_type_'.esc_attr($scroll_controls) : '')
					. '"'
				. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
				. ($css!='' || $css_dim!='' ? ' style="'.esc_attr($css.$css_dim).'"' : '')
				.'>' 
				. '<div class="sc_section_inner">'
					. ($bg_image !== '' || $bg_color !== '' || $bg_overlay>0 || $bg_texture>0 || sunnyjar_strlen($bg_texture)>2
						? '<div class="sc_section_overlay'.($bg_texture>0 ? ' texture_bg_'.esc_attr($bg_texture) : '') . '"'
							. ' style="' . ($bg_overlay>0 ? 'background-color:rgba('.(int)$rgb['r'].','.(int)$rgb['g'].','.(int)$rgb['b'].','.min(1, max(0, $bg_overlay)).');' : '')
								. (sunnyjar_strlen($bg_texture)>2 ? 'background-image:url('.esc_url($bg_texture).');' : '')
								. '"'
								. ($bg_overlay > 0 ? ' data-overlay="'.esc_attr($bg_overlay).'" data-bg_color="'.esc_attr($bg_color).'"' : '')
								. '>'
								. '<div class="sc_section_content' . (sunnyjar_param_is_on($bg_padding) ? ' padding_on' : ' padding_off') . '"'
									. ' style="'.esc_attr($css_dim).'"'
									. '>'
						: '')
					. (sunnyjar_param_is_on($scroll) 
						? '<div id="'.esc_attr($id).'_scroll" class="sc_scroll sc_scroll_'.esc_attr($scroll_dir).' swiper-slider-container scroll-container"'
							. ' style="'.($height != '' ? 'height:'.esc_attr($height).';' : '') . ($width != '' ? 'width:'.esc_attr($width).';' : '').'"'
							. '>'
							. '<div class="sc_scroll_wrapper swiper-wrapper">' 
							. '<div class="sc_scroll_slide swiper-slide">' 
						: '')
					. (sunnyjar_param_is_on($pan) 
						? '<div id="'.esc_attr($id).'_pan" class="sc_pan sc_pan_'.esc_attr($scroll_dir).'">' 
						: '')
                            . (!empty($title) ? '<h3 class="sc_section_title sc_item_title">' . trim(sunnyjar_strmacros($title)) . '</h3>' : '')
                            . (!empty($subtitle) ? '<h6 class="sc_section_subtitle sc_item_subtitle">' . trim(sunnyjar_strmacros($subtitle)) . '</h6>' : '')
							. (!empty($description) ? '<div class="sc_section_descr sc_item_descr">' . trim(sunnyjar_strmacros($description)) . '</div>' : '')
							. '<div class="sc_section_content_wrap">' . do_shortcode($content) . '</div>'
							. (!empty($link) ? '<div class="sc_section_button sc_item_button">'.sunnyjar_do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
					. (sunnyjar_param_is_on($pan) ? '</div>' : '')
					. (sunnyjar_param_is_on($scroll) 
						? '</div></div><div id="'.esc_attr($id).'_scroll_bar" class="sc_scroll_bar sc_scroll_bar_'.esc_attr($scroll_dir).' '.esc_attr($id).'_scroll_bar"></div></div>'
							. (!sunnyjar_param_is_off($scroll_controls) ? '<div class="sc_scroll_controls_wrap"><a class="sc_scroll_prev" href="#"></a><a class="sc_scroll_next" href="#"></a></div>' : '')
						: '')
					. ($bg_image !== '' || $bg_color !== '' || $bg_overlay > 0 || $bg_texture>0 || sunnyjar_strlen($bg_texture)>2 ? '</div></div>' : '')
					. '</div>'
				. '</div>';
		if (sunnyjar_param_is_on($dedicated)) {
			if (sunnyjar_storage_get('sc_section_dedicated')=='') {
				sunnyjar_storage_set('sc_section_dedicated', $output);
			}
			$output = '';
		}
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_section', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_section', 'sunnyjar_sc_section');
}

if (!function_exists('sunnyjar_sc_block')) {	
	function sunnyjar_sc_block($atts, $content=null) {
		if (empty($atts)) $atts = array();
		$atts['class'] = (!empty($atts['class']) ? ' ' : '') . 'sc_section_block';
		return apply_filters('sunnyjar_shortcode_output', sunnyjar_sc_section($atts, $content), 'trx_block', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_block', 'sunnyjar_sc_block');
}


/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_section_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_section_reg_shortcodes');
	function sunnyjar_sc_section_reg_shortcodes() {
	
		$sc = array(
			"title" => esc_html__("Block container", 'sunnyjar'),
			"desc" => wp_kses_data( __("Container for any block ([trx_section] analog - to enable nesting)", 'sunnyjar') ),
			"decorate" => true,
			"container" => true,
			"params" => array(
				"title" => array(
					"title" => esc_html__("Title", 'sunnyjar'),
					"desc" => wp_kses_data( __("Title for the block", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"subtitle" => array(
					"title" => esc_html__("Subtitle", 'sunnyjar'),
					"desc" => wp_kses_data( __("Subtitle for the block", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"description" => array(
					"title" => esc_html__("Description", 'sunnyjar'),
					"desc" => wp_kses_data( __("Short description for the block", 'sunnyjar') ),
					"value" => "",
					"type" => "textarea"
				),
                "position_caption" => array(
                    "title" => esc_html__("Align Captions", 'sunnyjar'),
                    "desc" => wp_kses_data( __("Align Captions left. (If not aligned on the center)", 'sunnyjar') ),
                    "value" => "no",
                    "type" => "switch",
                    "options" => sunnyjar_get_sc_param('yes_no')
                ),
				"link" => array(
					"title" => esc_html__("Button URL", 'sunnyjar'),
					"desc" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"link_caption" => array(
					"title" => esc_html__("Button caption", 'sunnyjar'),
					"desc" => wp_kses_data( __("Caption for the button at the bottom of the block", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"dedicated" => array(
					"title" => esc_html__("Dedicated", 'sunnyjar'),
					"desc" => wp_kses_data( __("Use this block as dedicated content - show it before post title on single page", 'sunnyjar') ),
					"value" => "no",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				),
				"align" => array(
					"title" => esc_html__("Align", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select block alignment", 'sunnyjar') ),
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('align')
				),
				"columns" => array(
					"title" => esc_html__("Columns emulation", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select width for columns emulation", 'sunnyjar') ),
					"value" => "none",
					"type" => "checklist",
					"options" => sunnyjar_get_sc_param('columns')
				), 
				"pan" => array(
					"title" => esc_html__("Use pan effect", 'sunnyjar'),
					"desc" => wp_kses_data( __("Use pan effect to show section content", 'sunnyjar') ),
					"divider" => true,
					"value" => "no",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				),
				"scroll" => array(
					"title" => esc_html__("Use scroller", 'sunnyjar'),
					"desc" => wp_kses_data( __("Use scroller to show section content", 'sunnyjar') ),
					"divider" => true,
					"value" => "no",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				),
				"scroll_dir" => array(
					"title" => esc_html__("Scroll and Pan direction", 'sunnyjar'),
					"desc" => wp_kses_data( __("Scroll and Pan direction (if Use scroller = yes or Pan = yes)", 'sunnyjar') ),
					"dependency" => array(
						'pan' => array('yes'),
						'scroll' => array('yes')
					),
					"value" => "horizontal",
					"type" => "switch",
					"size" => "big",
					"options" => sunnyjar_get_sc_param('dir')
				),
				"scroll_controls" => array(
					"title" => esc_html__("Scroll controls", 'sunnyjar'),
					"desc" => wp_kses_data( __("Show scroll controls (if Use scroller = yes)", 'sunnyjar') ),
					"dependency" => array(
						'scroll' => array('yes')
					),
					"value" => "hide",
					"type" => "checklist",
					"options" => sunnyjar_get_sc_param('controls')
				),
				"scheme" => array(
					"title" => esc_html__("Color scheme", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select color scheme for this block", 'sunnyjar') ),
					"value" => "",
					"type" => "checklist",
					"options" => sunnyjar_get_sc_param('schemes')
				),
				"color" => array(
					"title" => esc_html__("Fore color", 'sunnyjar'),
					"desc" => wp_kses_data( __("Any color for objects in this section", 'sunnyjar') ),
					"divider" => true,
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Background color", 'sunnyjar'),
					"desc" => wp_kses_data( __("Any background color for this section", 'sunnyjar') ),
					"value" => "",
					"type" => "color"
				),
				"bg_image" => array(
					"title" => esc_html__("Background image URL", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site for the background", 'sunnyjar') ),
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"bg_tile" => array(
					"title" => esc_html__("Tile background image", 'sunnyjar'),
					"desc" => wp_kses_data( __("Do you want tile background image or image cover whole block?", 'sunnyjar') ),
					"value" => "no",
					"dependency" => array(
						'bg_image' => array('not_empty')
					),
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				),
				"bg_overlay" => array(
					"title" => esc_html__("Overlay", 'sunnyjar'),
					"desc" => wp_kses_data( __("Overlay color opacity (from 0.0 to 1.0)", 'sunnyjar') ),
					"min" => "0",
					"max" => "1",
					"step" => "0.1",
					"value" => "0",
					"type" => "spinner"
				),
				"bg_texture" => array(
					"title" => esc_html__("Texture", 'sunnyjar'),
					"desc" => wp_kses_data( __("Predefined texture style from 1 to 11. 0 - without texture.", 'sunnyjar') ),
					"min" => "0",
					"max" => "11",
					"step" => "1",
					"value" => "0",
					"type" => "spinner"
				),
				"bg_padding" => array(
					"title" => esc_html__("Paddings around content", 'sunnyjar'),
					"desc" => wp_kses_data( __("Add paddings around content in this section (only if bg_color or bg_image enabled).", 'sunnyjar') ),
					"value" => "yes",
					"dependency" => array(
						'compare' => 'or',
						'bg_color' => array('not_empty'),
						'bg_texture' => array('not_empty'),
						'bg_image' => array('not_empty')
					),
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				),
				"font_size" => array(
					"title" => esc_html__("Font size", 'sunnyjar'),
					"desc" => wp_kses_data( __("Font size of the text (default - in pixels, allows any CSS units of measure)", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"font_weight" => array(
					"title" => esc_html__("Font weight", 'sunnyjar'),
					"desc" => wp_kses_data( __("Font weight of the text", 'sunnyjar') ),
					"value" => "",
					"type" => "select",
					"size" => "medium",
					"options" => array(
						'100' => esc_html__('Thin (100)', 'sunnyjar'),
						'300' => esc_html__('Light (300)', 'sunnyjar'),
						'400' => esc_html__('Normal (400)', 'sunnyjar'),
						'700' => esc_html__('Bold (700)', 'sunnyjar')
					)
				),
				"_content_" => array(
					"title" => esc_html__("Container content", 'sunnyjar'),
					"desc" => wp_kses_data( __("Content for section container", 'sunnyjar') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"width" => sunnyjar_shortcodes_width(),
				"height" => sunnyjar_shortcodes_height(),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		);
		sunnyjar_sc_map("trx_block", $sc);
		$sc["title"] = esc_html__("Section container", 'sunnyjar');
		$sc["desc"] = esc_html__("Container for any section ([trx_block] analog - to enable nesting)", 'sunnyjar');
		sunnyjar_sc_map("trx_section", $sc);
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_section_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_section_reg_shortcodes_vc');
	function sunnyjar_sc_section_reg_shortcodes_vc() {
	
		$sc = array(
			"base" => "trx_block",
			"name" => esc_html__("Block container", 'sunnyjar'),
			"description" => wp_kses_data( __("Container for any block ([trx_section] analog - to enable nesting)", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_block',
			"class" => "trx_sc_collection trx_sc_block",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "dedicated",
					"heading" => esc_html__("Dedicated", 'sunnyjar'),
					"description" => wp_kses_data( __("Use this block as dedicated content - show it before post title on single page", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(esc_html__('Use as dedicated content', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'sunnyjar'),
					"description" => wp_kses_data( __("Select block alignment", 'sunnyjar') ),
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "columns",
					"heading" => esc_html__("Columns emulation", 'sunnyjar'),
					"description" => wp_kses_data( __("Select width for columns emulation", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('columns')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'sunnyjar'),
					"description" => wp_kses_data( __("Title for the block", 'sunnyjar') ),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle", 'sunnyjar'),
					"description" => wp_kses_data( __("Subtitle for the block", 'sunnyjar') ),
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Description", 'sunnyjar'),
					"description" => wp_kses_data( __("Description for the block", 'sunnyjar') ),
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textarea"
				),
                array(
                    "param_name" => "position_caption",
                    "heading" => esc_html__("Align Captions", 'sunnyjar'),
                    "description" => wp_kses_data( __("Align Captions left. (If not aligned on the center)", 'sunnyjar') ),
                    "group" => esc_html__('Captions', 'sunnyjar'),
                    "class" => "",
                    "value" => array(esc_html__('Left', 'sunnyjar') => 'yes'),
                    "type" => "checkbox"
                ),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Button URL", 'sunnyjar'),
					"description" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'sunnyjar') ),
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_caption",
					"heading" => esc_html__("Button caption", 'sunnyjar'),
					"description" => wp_kses_data( __("Caption for the button at the bottom of the block", 'sunnyjar') ),
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "pan",
					"heading" => esc_html__("Use pan effect", 'sunnyjar'),
					"description" => wp_kses_data( __("Use pan effect to show section content", 'sunnyjar') ),
					"group" => esc_html__('Scroll', 'sunnyjar'),
					"class" => "",
					"value" => array(esc_html__('Content scroller', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "scroll",
					"heading" => esc_html__("Use scroller", 'sunnyjar'),
					"description" => wp_kses_data( __("Use scroller to show section content", 'sunnyjar') ),
					"group" => esc_html__('Scroll', 'sunnyjar'),
					"admin_label" => true,
					"class" => "",
					"value" => array(esc_html__('Content scroller', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "scroll_dir",
					"heading" => esc_html__("Scroll direction", 'sunnyjar'),
					"description" => wp_kses_data( __("Scroll direction (if Use scroller = yes)", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"group" => esc_html__('Scroll', 'sunnyjar'),
					"value" => array_flip(sunnyjar_get_sc_param('dir')),
					'dependency' => array(
						'element' => 'scroll',
						'not_empty' => true
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "scroll_controls",
					"heading" => esc_html__("Scroll controls", 'sunnyjar'),
					"description" => wp_kses_data( __("Show scroll controls (if Use scroller = yes)", 'sunnyjar') ),
					"class" => "",
					"group" => esc_html__('Scroll', 'sunnyjar'),
					'dependency' => array(
						'element' => 'scroll',
						'not_empty' => true
					),
					"value" => array_flip(sunnyjar_get_sc_param('controls')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'sunnyjar'),
					"description" => wp_kses_data( __("Select color scheme for this block", 'sunnyjar') ),
					"group" => esc_html__('Colors and Images', 'sunnyjar'),
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('schemes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Fore color", 'sunnyjar'),
					"description" => wp_kses_data( __("Any color for objects in this section", 'sunnyjar') ),
					"group" => esc_html__('Colors and Images', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Background color", 'sunnyjar'),
					"description" => wp_kses_data( __("Any background color for this section", 'sunnyjar') ),
					"group" => esc_html__('Colors and Images', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_image",
					"heading" => esc_html__("Background image URL", 'sunnyjar'),
					"description" => wp_kses_data( __("Select background image from library for this section", 'sunnyjar') ),
					"group" => esc_html__('Colors and Images', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "bg_tile",
					"heading" => esc_html__("Tile background image", 'sunnyjar'),
					"description" => wp_kses_data( __("Do you want tile background image or image cover whole block?", 'sunnyjar') ),
					"group" => esc_html__('Colors and Images', 'sunnyjar'),
					"class" => "",
					'dependency' => array(
						'element' => 'bg_image',
						'not_empty' => true
					),
					"std" => "no",
					"value" => array(esc_html__('Tile background image', 'sunnyjar') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "bg_overlay",
					"heading" => esc_html__("Overlay", 'sunnyjar'),
					"description" => wp_kses_data( __("Overlay color opacity (from 0.0 to 1.0)", 'sunnyjar') ),
					"group" => esc_html__('Colors and Images', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "bg_texture",
					"heading" => esc_html__("Texture", 'sunnyjar'),
					"description" => wp_kses_data( __("Texture style from 1 to 11. Empty or 0 - without texture.", 'sunnyjar') ),
					"group" => esc_html__('Colors and Images', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "bg_padding",
					"heading" => esc_html__("Paddings around content", 'sunnyjar'),
					"description" => wp_kses_data( __("Add paddings around content in this section (only if bg_color or bg_image enabled).", 'sunnyjar') ),
					"group" => esc_html__('Colors and Images', 'sunnyjar'),
					"class" => "",
					'dependency' => array(
						'element' => array('bg_color','bg_texture','bg_image'),
						'not_empty' => true
					),
					"std" => "yes",
					"value" => array(esc_html__('Disable padding around content in this block', 'sunnyjar') => 'no'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "font_size",
					"heading" => esc_html__("Font size", 'sunnyjar'),
					"description" => wp_kses_data( __("Font size of the text (default - in pixels, allows any CSS units of measure)", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "font_weight",
					"heading" => esc_html__("Font weight", 'sunnyjar'),
					"description" => wp_kses_data( __("Font weight of the text", 'sunnyjar') ),
					"class" => "",
					"value" => array(
						esc_html__('Default', 'sunnyjar') => 'inherit',
						esc_html__('Thin (100)', 'sunnyjar') => '100',
						esc_html__('Light (300)', 'sunnyjar') => '300',
						esc_html__('Normal (400)', 'sunnyjar') => '400',
						esc_html__('Bold (700)', 'sunnyjar') => '700'
					),
					"type" => "dropdown"
				),
				/*
				array(
					"param_name" => "content",
					"heading" => esc_html__("Container content", 'sunnyjar'),
					"description" => wp_kses_data( __("Content for section container", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				*/
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_vc_width(),
				sunnyjar_vc_height(),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			)
		);
		
		// Block
		vc_map($sc);
		
		// Section
		$sc["base"] = 'trx_section';
		$sc["name"] = esc_html__("Section container", 'sunnyjar');
		$sc["description"] = wp_kses_data( __("Container for any section ([trx_block] analog - to enable nesting)", 'sunnyjar') );
		$sc["class"] = "trx_sc_collection trx_sc_section";
		$sc["icon"] = 'icon_trx_section';
		vc_map($sc);
		
		class WPBakeryShortCode_Trx_Block extends SUNNYJAR_VC_ShortCodeCollection {}
		class WPBakeryShortCode_Trx_Section extends SUNNYJAR_VC_ShortCodeCollection {}
	}
}
?>