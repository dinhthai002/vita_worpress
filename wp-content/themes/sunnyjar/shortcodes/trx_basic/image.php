<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_image_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_image_theme_setup' );
	function sunnyjar_sc_image_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_image_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_image_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_image id="unique_id" src="image_url" width="width_in_pixels" height="height_in_pixels" title="image's_title" align="left|right"]
*/

if (!function_exists('sunnyjar_sc_image')) {	
	function sunnyjar_sc_image($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"align" => "",
			"shape" => "square",
			"src" => "",
			"url" => "",
			"icon" => "",
			"link" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => "",
			"width" => "",
			"height" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= sunnyjar_get_css_dimensions_from_values($width, $height);
		$src = $src!='' ? $src : $url;
		if ($src > 0) {
			$attach = wp_get_attachment_image_src( $src, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$src = $attach[0];
		}
		if (!empty($width) || !empty($height)) {
			$w = !empty($width) && strlen(intval($width)) == strlen($width) ? $width : null;
			$h = !empty($height) && strlen(intval($height)) == strlen($height) ? $height : null;
			if ($w || $h) $src = sunnyjar_get_resized_image_url($src, $w, $h);
		}
		if (trim($link)) sunnyjar_enqueue_popup();
		$output = empty($src) ? '' : ('<figure' . ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ' class="sc_image ' . ($align && $align!='none' ? ' align' . esc_attr($align) : '') . (!empty($shape) ? ' sc_image_shape_'.esc_attr($shape) : '') . (!empty($class) ? ' '.esc_attr($class) : '') . '"'
			. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
			. '>'
				. (trim($link) ? '<a href="'.esc_url($link).'">' : '')
				. '<img src="'.esc_url($src).'" alt="" />'
                . (trim($link) ? (trim($title) || trim($icon) ? '<div class="figcaption"><span'.($icon ? ' class="'.esc_attr($icon).'"' : '').'></span>' . ($title) . '</div>' : '') : '')
                . (trim($link) ? '</a>' : '')
				. (trim($title) || trim($icon) ? '<figcaption><span'.($icon ? ' class="'.esc_attr($icon).'"' : '').'></span> ' . ($title) . '</figcaption>' : '')
			. '</figure>');
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_image', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_image', 'sunnyjar_sc_image');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_image_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_image_reg_shortcodes');
	function sunnyjar_sc_image_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_image", array(
			"title" => esc_html__("Image", 'sunnyjar'),
			"desc" => wp_kses_data( __("Insert image into your post (page)", 'sunnyjar') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"url" => array(
					"title" => esc_html__("URL for image file", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site", 'sunnyjar') ),
					"readonly" => false,
					"value" => "",
					"type" => "media",
					"before" => array(
						'sizes' => true		// If you want allow user select thumb size for image. Otherwise, thumb size is ignored - image fullsize used
					)
				),
				"title" => array(
					"title" => esc_html__("Title", 'sunnyjar'),
					"desc" => wp_kses_data( __("Image title (if need)", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"icon" => array(
					"title" => esc_html__("Icon before title",  'sunnyjar'),
					"desc" => wp_kses_data( __('Select icon for the title from Fontello icons set',  'sunnyjar') ),
					"value" => "",
					"type" => "icons",
					"options" => sunnyjar_get_sc_param('icons')
				),
				"align" => array(
					"title" => esc_html__("Float image", 'sunnyjar'),
					"desc" => wp_kses_data( __("Float image to left or right side", 'sunnyjar') ),
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('float')
				), 
				"shape" => array(
					"title" => esc_html__("Image Shape", 'sunnyjar'),
					"desc" => wp_kses_data( __("Shape of the image: square (rectangle) or round", 'sunnyjar') ),
					"value" => "square",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => array(
						"square" => esc_html__('Square', 'sunnyjar'),
						"round" => esc_html__('Round', 'sunnyjar')
					)
				), 
				"link" => array(
					"title" => esc_html__("Link", 'sunnyjar'),
					"desc" => wp_kses_data( __("The link URL from the image", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"width" => sunnyjar_shortcodes_width(),
				"height" => sunnyjar_shortcodes_height(),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_image_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_image_reg_shortcodes_vc');
	function sunnyjar_sc_image_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_image",
			"name" => esc_html__("Image", 'sunnyjar'),
			"description" => wp_kses_data( __("Insert image", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_image',
			"class" => "trx_sc_single trx_sc_image",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "url",
					"heading" => esc_html__("Select image", 'sunnyjar'),
					"description" => wp_kses_data( __("Select image from library", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Image alignment", 'sunnyjar'),
					"description" => wp_kses_data( __("Align image to left or right side", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('float')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "shape",
					"heading" => esc_html__("Image shape", 'sunnyjar'),
					"description" => wp_kses_data( __("Shape of the image: square or round", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Square', 'sunnyjar') => 'square',
						esc_html__('Round', 'sunnyjar') => 'round'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'sunnyjar'),
					"description" => wp_kses_data( __("Image's title", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Title's icon", 'sunnyjar'),
					"description" => wp_kses_data( __("Select icon for the title from Fontello icons set", 'sunnyjar') ),
					"class" => "",
					"value" => sunnyjar_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link", 'sunnyjar'),
					"description" => wp_kses_data( __("The link URL from the image", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_vc_width(),
				sunnyjar_vc_height(),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Image extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>