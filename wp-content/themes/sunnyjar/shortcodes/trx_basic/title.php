<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_title_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_title_theme_setup' );
	function sunnyjar_sc_title_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_title_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_title_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_title id="unique_id" style='regular|iconed' icon='' image='' background="on|off" type="1-6"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_title]
*/

if (!function_exists('sunnyjar_sc_title')) {	
	function sunnyjar_sc_title($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "1",
			"style" => "regular",
			"align" => "",
			"font_weight" => "",
			"font_size" => "",
			"color" => "",
			"icon" => "",
			"image" => "",
			"picture" => "",
			"image_size" => "small",
			"position" => "left",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= sunnyjar_get_css_dimensions_from_values($width)
			.($align && $align!='none' && !sunnyjar_param_is_inherit($align) ? 'text-align:' . esc_attr($align) .';' : '')
			.($color ? 'color:' . esc_attr($color) .';' : '')
			.($font_weight && !sunnyjar_param_is_inherit($font_weight) ? 'font-weight:' . esc_attr($font_weight) .';' : '')
			.($font_size   ? 'font-size:' . esc_attr($font_size) .';' : '')
			;
		$type = min(6, max(1, $type));
		if ($picture > 0) {
			$attach = wp_get_attachment_image_src( $picture, 'full' );
			if (isset($attach[0]) && $attach[0]!='')
				$picture = $attach[0];
		}
		$pic = $style!='iconed' 
			? '' 
			: '<span class="sc_title_icon sc_title_icon_'.esc_attr($position).'  sc_title_icon_'.esc_attr($image_size).($icon!='' && $icon!='none' ? ' '.esc_attr($icon) : '').'"'.'>'
				.($picture ? '<img src="'.esc_url($picture).'" alt="" />' : '')
				.(empty($picture) && $image && $image!='none' ? '<img src="'.esc_url(sunnyjar_strpos($image, 'http:')!==false ? $image : sunnyjar_get_file_url('images/icons/'.($image).'.png')).'" alt="" />' : '')
				.'</span>';
		$output = '<h' . esc_attr($type) . ($id ? ' id="'.esc_attr($id).'"' : '')
				. ' class="sc_title sc_title_'.esc_attr($style)
					.($align && $align!='none' && !sunnyjar_param_is_inherit($align) ? ' sc_align_' . esc_attr($align) : '')
					.(!empty($class) ? ' '.esc_attr($class) : '')
					.'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
				. '>'
					. ($pic)
					. ($style=='divider' ? '<span class="sc_title_divider_before"'.($color ? ' style="background-color: '.esc_attr($color).'"' : '').'></span>' : '')
					. do_shortcode($content)
					. ($style=='divider' ? '<span class="sc_title_divider_after"'.($color ? ' style="background-color: '.esc_attr($color).'"' : '').'></span>' : '')
				. '</h' . esc_attr($type) . '>';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_title', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_title', 'sunnyjar_sc_title');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_title_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_title_reg_shortcodes');
	function sunnyjar_sc_title_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_title", array(
			"title" => esc_html__("Title", 'sunnyjar'),
			"desc" => wp_kses_data( __("Create header tag (1-6 level) with many styles", 'sunnyjar') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"_content_" => array(
					"title" => esc_html__("Title content", 'sunnyjar'),
					"desc" => wp_kses_data( __("Title content", 'sunnyjar') ),
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"type" => array(
					"title" => esc_html__("Title type", 'sunnyjar'),
					"desc" => wp_kses_data( __("Title type (header level)", 'sunnyjar') ),
					"divider" => true,
					"value" => "1",
					"type" => "select",
					"options" => array(
						'1' => esc_html__('Header 1', 'sunnyjar'),
						'2' => esc_html__('Header 2', 'sunnyjar'),
						'3' => esc_html__('Header 3', 'sunnyjar'),
						'4' => esc_html__('Header 4', 'sunnyjar'),
						'5' => esc_html__('Header 5', 'sunnyjar'),
						'6' => esc_html__('Header 6', 'sunnyjar'),
					)
				),
				"style" => array(
					"title" => esc_html__("Title style", 'sunnyjar'),
					"desc" => wp_kses_data( __("Title style", 'sunnyjar') ),
					"value" => "regular",
					"type" => "select",
					"options" => array(
						'regular' => esc_html__('Regular', 'sunnyjar'),
						'underline' => esc_html__('Underline', 'sunnyjar'),
						'divider' => esc_html__('Divider', 'sunnyjar'),
						'iconed' => esc_html__('With icon (image)', 'sunnyjar')
					)
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'sunnyjar'),
					"desc" => wp_kses_data( __("Title text alignment", 'sunnyjar') ),
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('align')
				), 
				"font_size" => array(
					"title" => esc_html__("Font_size", 'sunnyjar'),
					"desc" => wp_kses_data( __("Custom font size. If empty - use theme default", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"font_weight" => array(
					"title" => esc_html__("Font weight", 'sunnyjar'),
					"desc" => wp_kses_data( __("Custom font weight. If empty or inherit - use theme default", 'sunnyjar') ),
					"value" => "",
					"type" => "select",
					"size" => "medium",
					"options" => array(
						'inherit' => esc_html__('Default', 'sunnyjar'),
						'100' => esc_html__('Thin (100)', 'sunnyjar'),
						'300' => esc_html__('Light (300)', 'sunnyjar'),
						'400' => esc_html__('Normal (400)', 'sunnyjar'),
						'600' => esc_html__('Semibold (600)', 'sunnyjar'),
						'700' => esc_html__('Bold (700)', 'sunnyjar'),
						'900' => esc_html__('Black (900)', 'sunnyjar')
					)
				),
				"color" => array(
					"title" => esc_html__("Title color", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select color for the title", 'sunnyjar') ),
					"value" => "",
					"type" => "color"
				),
				"icon" => array(
					"title" => esc_html__('Title font icon',  'sunnyjar'),
					"desc" => wp_kses_data( __("Select font icon for the title from Fontello icons set (if style=iconed)",  'sunnyjar') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "",
					"type" => "icons",
					"options" => sunnyjar_get_sc_param('icons')
				),
				"image" => array(
					"title" => esc_html__('or image icon',  'sunnyjar'),
					"desc" => wp_kses_data( __("Select image icon for the title instead icon above (if style=iconed)",  'sunnyjar') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "",
					"type" => "images",
					"size" => "small",
					"options" => sunnyjar_get_sc_param('images')
				),
				"picture" => array(
					"title" => esc_html__('or URL for image file', 'sunnyjar'),
					"desc" => wp_kses_data( __("Select or upload image or write URL from other site (if style=iconed)", 'sunnyjar') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"image_size" => array(
					"title" => esc_html__('Image (picture) size', 'sunnyjar'),
					"desc" => wp_kses_data( __("Select image (picture) size (if style='iconed')", 'sunnyjar') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "small",
					"type" => "checklist",
					"options" => array(
						'small' => esc_html__('Small', 'sunnyjar'),
						'medium' => esc_html__('Medium', 'sunnyjar'),
						'large' => esc_html__('Large', 'sunnyjar')
					)
				),
				"position" => array(
					"title" => esc_html__('Icon (image) position', 'sunnyjar'),
					"desc" => wp_kses_data( __("Select icon (image) position (if style=iconed)", 'sunnyjar') ),
					"dependency" => array(
						'style' => array('iconed')
					),
					"value" => "left",
					"type" => "checklist",
					"options" => array(
						'top' => esc_html__('Top', 'sunnyjar'),
						'left' => esc_html__('Left', 'sunnyjar')
					)
				),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_title_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_title_reg_shortcodes_vc');
	function sunnyjar_sc_title_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_title",
			"name" => esc_html__("Title", 'sunnyjar'),
			"description" => wp_kses_data( __("Create header tag (1-6 level) with many styles", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_title',
			"class" => "trx_sc_single trx_sc_title",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "content",
					"heading" => esc_html__("Title content", 'sunnyjar'),
					"description" => wp_kses_data( __("Title content", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				array(
					"param_name" => "type",
					"heading" => esc_html__("Title type", 'sunnyjar'),
					"description" => wp_kses_data( __("Title type (header level)", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Header 1', 'sunnyjar') => '1',
						esc_html__('Header 2', 'sunnyjar') => '2',
						esc_html__('Header 3', 'sunnyjar') => '3',
						esc_html__('Header 4', 'sunnyjar') => '4',
						esc_html__('Header 5', 'sunnyjar') => '5',
						esc_html__('Header 6', 'sunnyjar') => '6'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "style",
					"heading" => esc_html__("Title style", 'sunnyjar'),
					"description" => wp_kses_data( __("Title style: only text (regular) or with icon/image (iconed)", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Regular', 'sunnyjar') => 'regular',
						esc_html__('Underline', 'sunnyjar') => 'underline',
						esc_html__('Divider', 'sunnyjar') => 'divider',
						esc_html__('With icon (image)', 'sunnyjar') => 'iconed'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'sunnyjar'),
					"description" => wp_kses_data( __("Title text alignment", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "font_size",
					"heading" => esc_html__("Font size", 'sunnyjar'),
					"description" => wp_kses_data( __("Custom font size. If empty - use theme default", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "font_weight",
					"heading" => esc_html__("Font weight", 'sunnyjar'),
					"description" => wp_kses_data( __("Custom font weight. If empty or inherit - use theme default", 'sunnyjar') ),
					"class" => "",
					"value" => array(
						esc_html__('Default', 'sunnyjar') => 'inherit',
						esc_html__('Thin (100)', 'sunnyjar') => '100',
						esc_html__('Light (300)', 'sunnyjar') => '300',
						esc_html__('Normal (400)', 'sunnyjar') => '400',
						esc_html__('Semibold (600)', 'sunnyjar') => '600',
						esc_html__('Bold (700)', 'sunnyjar') => '700',
						esc_html__('Black (900)', 'sunnyjar') => '900'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Title color", 'sunnyjar'),
					"description" => wp_kses_data( __("Select color for the title", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Title font icon", 'sunnyjar'),
					"description" => wp_kses_data( __("Select font icon for the title from Fontello icons set (if style=iconed)", 'sunnyjar') ),
					"class" => "",
					"group" => esc_html__('Icon &amp; Image', 'sunnyjar'),
					'dependency' => array(
						'element' => 'style',
						'value' => array('iconed')
					),
					"value" => sunnyjar_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "image",
					"heading" => esc_html__("or image icon", 'sunnyjar'),
					"description" => wp_kses_data( __("Select image icon for the title instead icon above (if style=iconed)", 'sunnyjar') ),
					"class" => "",
					"group" => esc_html__('Icon &amp; Image', 'sunnyjar'),
					'dependency' => array(
						'element' => 'style',
						'value' => array('iconed')
					),
					"value" => sunnyjar_get_sc_param('images'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "picture",
					"heading" => esc_html__("or select uploaded image", 'sunnyjar'),
					"description" => wp_kses_data( __("Select or upload image or write URL from other site (if style=iconed)", 'sunnyjar') ),
					"group" => esc_html__('Icon &amp; Image', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "image_size",
					"heading" => esc_html__("Image (picture) size", 'sunnyjar'),
					"description" => wp_kses_data( __("Select image (picture) size (if style=iconed)", 'sunnyjar') ),
					"group" => esc_html__('Icon &amp; Image', 'sunnyjar'),
					"class" => "",
					"value" => array(
						esc_html__('Small', 'sunnyjar') => 'small',
						esc_html__('Medium', 'sunnyjar') => 'medium',
						esc_html__('Large', 'sunnyjar') => 'large'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "position",
					"heading" => esc_html__("Icon (image) position", 'sunnyjar'),
					"description" => wp_kses_data( __("Select icon (image) position (if style=iconed)", 'sunnyjar') ),
					"group" => esc_html__('Icon &amp; Image', 'sunnyjar'),
					"class" => "",
					"std" => "left",
					"value" => array(
						esc_html__('Top', 'sunnyjar') => 'top',
						esc_html__('Left', 'sunnyjar') => 'left'
					),
					"type" => "dropdown"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Title extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>