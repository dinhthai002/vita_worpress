<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_br_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_br_theme_setup' );
	function sunnyjar_sc_br_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_br_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_br_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_br clear="left|right|both"]
*/

if (!function_exists('sunnyjar_sc_br')) {	
	function sunnyjar_sc_br($atts, $content = null) {
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			"clear" => ""
		), $atts)));
		$output = in_array($clear, array('left', 'right', 'both', 'all')) 
			? '<div class="clearfix" style="clear:' . str_replace('all', 'both', $clear) . '"></div>'
			: '<br />';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_br', $atts, $content);
	}
	sunnyjar_require_shortcode("trx_br", "sunnyjar_sc_br");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_br_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_br_reg_shortcodes');
	function sunnyjar_sc_br_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_br", array(
			"title" => esc_html__("Break", 'sunnyjar'),
			"desc" => wp_kses_data( __("Line break with clear floating (if need)", 'sunnyjar') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"clear" => 	array(
					"title" => esc_html__("Clear floating", 'sunnyjar'),
					"desc" => wp_kses_data( __("Clear floating (if need)", 'sunnyjar') ),
					"value" => "",
					"type" => "checklist",
					"options" => array(
						'none' => esc_html__('None', 'sunnyjar'),
						'left' => esc_html__('Left', 'sunnyjar'),
						'right' => esc_html__('Right', 'sunnyjar'),
						'both' => esc_html__('Both', 'sunnyjar')
					)
				)
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_br_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_br_reg_shortcodes_vc');
	function sunnyjar_sc_br_reg_shortcodes_vc() {
/*
		vc_map( array(
			"base" => "trx_br",
			"name" => esc_html__("Line break", 'sunnyjar'),
			"description" => wp_kses_data( __("Line break or Clear Floating", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_br',
			"class" => "trx_sc_single trx_sc_br",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "clear",
					"heading" => esc_html__("Clear floating", 'sunnyjar'),
					"description" => wp_kses_data( __("Select clear side (if need)", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"value" => array(
						esc_html__('None', 'sunnyjar') => 'none',
						esc_html__('Left', 'sunnyjar') => 'left',
						esc_html__('Right', 'sunnyjar') => 'right',
						esc_html__('Both', 'sunnyjar') => 'both'
					),
					"type" => "dropdown"
				)
			)
		) );
		
		class WPBakeryShortCode_Trx_Br extends SUNNYJAR_VC_ShortCodeSingle {}
*/
	}
}
?>