<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_anchor_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_anchor_theme_setup' );
	function sunnyjar_sc_anchor_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_anchor_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_anchor_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_anchor id="unique_id" description="Anchor description" title="Short Caption" icon="icon-class"]
*/

if (!function_exists('sunnyjar_sc_anchor')) {	
	function sunnyjar_sc_anchor($atts, $content = null) {
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"description" => '',
			"icon" => '',
			"url" => "",
			"separator" => "no",
			// Common params
			"id" => ""
		), $atts)));
		$output = $id 
			? '<a id="'.esc_attr($id).'"'
				. ' class="sc_anchor"' 
				. ' title="' . ($title ? esc_attr($title) : '') . '"'
				. ' data-description="' . ($description ? esc_attr(sunnyjar_strmacros($description)) : ''). '"'
				. ' data-icon="' . ($icon ? $icon : '') . '"' 
				. ' data-url="' . ($url ? esc_attr($url) : '') . '"' 
				. ' data-separator="' . (sunnyjar_param_is_on($separator) ? 'yes' : 'no') . '"'
				. '></a>'
			: '';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_anchor', $atts, $content);
	}
	sunnyjar_require_shortcode("trx_anchor", "sunnyjar_sc_anchor");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_anchor_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_anchor_reg_shortcodes');
	function sunnyjar_sc_anchor_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_anchor", array(
			"title" => esc_html__("Anchor", 'sunnyjar'),
			"desc" => wp_kses_data( __("Insert anchor for the TOC (table of content)", 'sunnyjar') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"icon" => array(
					"title" => esc_html__("Anchor's icon",  'sunnyjar'),
					"desc" => wp_kses_data( __('Select icon for the anchor from Fontello icons set',  'sunnyjar') ),
					"value" => "",
					"type" => "icons",
					"options" => sunnyjar_get_sc_param('icons')
				),
				"title" => array(
					"title" => esc_html__("Short title", 'sunnyjar'),
					"desc" => wp_kses_data( __("Short title of the anchor (for the table of content)", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"description" => array(
					"title" => esc_html__("Long description", 'sunnyjar'),
					"desc" => wp_kses_data( __("Description for the popup (then hover on the icon). You can use:<br>'{{' and '}}' - to make the text italic,<br>'((' and '))' - to make the text bold,<br>'||' - to insert line break", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"url" => array(
					"title" => esc_html__("External URL", 'sunnyjar'),
					"desc" => wp_kses_data( __("External URL for this TOC item", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"separator" => array(
					"title" => esc_html__("Add separator", 'sunnyjar'),
					"desc" => wp_kses_data( __("Add separator under item in the TOC", 'sunnyjar') ),
					"value" => "no",
					"type" => "switch",
					"options" => sunnyjar_get_sc_param('yes_no')
				),
				"id" => sunnyjar_get_sc_param('id')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_anchor_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_anchor_reg_shortcodes_vc');
	function sunnyjar_sc_anchor_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_anchor",
			"name" => esc_html__("Anchor", 'sunnyjar'),
			"description" => wp_kses_data( __("Insert anchor for the TOC (table of content)", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_anchor',
			"class" => "trx_sc_single trx_sc_anchor",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Anchor's icon", 'sunnyjar'),
					"description" => wp_kses_data( __("Select icon for the anchor from Fontello icons set", 'sunnyjar') ),
					"class" => "",
					"value" => sunnyjar_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Short title", 'sunnyjar'),
					"description" => wp_kses_data( __("Short title of the anchor (for the table of content)", 'sunnyjar') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Long description", 'sunnyjar'),
					"description" => wp_kses_data( __("Description for the popup (then hover on the icon). You can use:<br>'{{' and '}}' - to make the text italic,<br>'((' and '))' - to make the text bold,<br>'||' - to insert line break", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "url",
					"heading" => esc_html__("External URL", 'sunnyjar'),
					"description" => wp_kses_data( __("External URL for this TOC item", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "separator",
					"heading" => esc_html__("Add separator", 'sunnyjar'),
					"description" => wp_kses_data( __("Add separator under item in the TOC", 'sunnyjar') ),
					"class" => "",
					"value" => array("Add separator" => "yes" ),
					"type" => "checkbox"
				),
				sunnyjar_get_vc_param('id')
			),
		) );
		
		class WPBakeryShortCode_Trx_Anchor extends SUNNYJAR_VC_ShortCodeSingle {}
	}
}
?>