<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_hide_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_hide_theme_setup' );
	function sunnyjar_sc_hide_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_hide_reg_shortcodes');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_hide selector="unique_id"]
*/

if (!function_exists('sunnyjar_sc_hide')) {	
	function sunnyjar_sc_hide($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"selector" => "",
			"hide" => "on",
			"delay" => 0
		), $atts)));
		$selector = trim(chop($selector));
		$output = $selector == '' ? '' : 
			'<script type="text/javascript">
				jQuery(document).ready(function() {
					'.($delay>0 ? 'setTimeout(function() {' : '').'
					jQuery("'.esc_attr($selector).'").' . ($hide=='on' ? 'hide' : 'show') . '();
					'.($delay>0 ? '},'.($delay).');' : '').'
				});
			</script>';
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_hide', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_hide', 'sunnyjar_sc_hide');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_hide_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_hide_reg_shortcodes');
	function sunnyjar_sc_hide_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_hide", array(
			"title" => esc_html__("Hide/Show any block", 'sunnyjar'),
			"desc" => wp_kses_data( __("Hide or Show any block with desired CSS-selector", 'sunnyjar') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"selector" => array(
					"title" => esc_html__("Selector", 'sunnyjar'),
					"desc" => wp_kses_data( __("Any block's CSS-selector", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"hide" => array(
					"title" => esc_html__("Hide or Show", 'sunnyjar'),
					"desc" => wp_kses_data( __("New state for the block: hide or show", 'sunnyjar') ),
					"value" => "yes",
					"size" => "small",
					"options" => sunnyjar_get_sc_param('yes_no'),
					"type" => "switch"
				)
			)
		));
	}
}
?>