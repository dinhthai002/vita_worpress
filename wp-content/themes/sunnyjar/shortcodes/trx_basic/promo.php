<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('sunnyjar_sc_promo_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_sc_promo_theme_setup' );
	function sunnyjar_sc_promo_theme_setup() {
		add_action('sunnyjar_action_shortcodes_list', 		'sunnyjar_sc_promo_reg_shortcodes');
		if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
			add_action('sunnyjar_action_shortcodes_list_vc','sunnyjar_sc_promo_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */


if (!function_exists('sunnyjar_sc_promo')) {	
	function sunnyjar_sc_promo($atts, $content=null){	
		if (sunnyjar_in_shortcode_blogger()) return '';
		extract(sunnyjar_html_decode(shortcode_atts(array(
			// Individual params
			"size" => "large",
			"align" => "none",
			"image" => "",
			"image_position" => "left",
			"image_width" => "50%",
			"text_margins" => '',
			"text_align" => "left",
			"scheme" => "",
			"title" => "",
			"subtitle" => "",
			"description" => "",
			"link_caption" => esc_html__('Learn more', 'sunnyjar'),
			"link" => '',
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
	
		if ($image > 0) {
			$attach = wp_get_attachment_image_src($image, 'full');
			if (isset($attach[0]) && $attach[0]!='')
				$image = $attach[0];
		}
		if ($image == '') {
			$image_width = '0%';
			$text_margins = '';
		}
		
		$width  = sunnyjar_prepare_css_value($width);
		$height = sunnyjar_prepare_css_value($height);
		
		$class .= ($class ? ' ' : '') . sunnyjar_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= sunnyjar_get_css_dimensions_from_values($width, $height);
		
		$css_image = (!empty($image) ? 'background-image:url(' . esc_url($image) . ');' : '')
				     . (!empty($image_width) ? 'width:'.trim($image_width).';' : '')
				     . (!empty($image_position) ? $image_position.': 0;' : '');
	
		$text_width = sunnyjar_strpos($image_width, '%')!==false
						? (100 - (int) str_replace('%', '', $image_width)).'%'
						: 'calc(100%-'.trim($image_width).')';
		$css_text = 'width: '.esc_attr($text_width).'; float: '.($image_position=='left' ? 'right' : 'left').';'.(!empty($text_margins) ? ' padding:'.esc_attr($text_margins).';' : '');
		
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_promo' 
						. ($class ? ' ' . esc_attr($class) : '') 
						. ($scheme && !sunnyjar_param_is_off($scheme) && !sunnyjar_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
						. ($size ? ' sc_promo_size_'.esc_attr($size) : '') 
						. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
						. (empty($image) ? ' no_image' : '')
						. '"'
					. (!sunnyjar_param_is_off($animation) ? ' data-animation="'.esc_attr(sunnyjar_get_animation_classes($animation)).'"' : '')
					. ($css ? 'style="'.esc_attr($css).'"' : '')
					.'>'

					. '<div class="sc_promo_inner">'
						. '<div class="sc_promo_image" style="'.esc_attr($css_image).'"></div>'
                        . '<div class="content_wrap">'
                            . '<div class="sc_promo_block sc_align_'.esc_attr($text_align) . ($image_position=='left' ? ' right' : ' left').'" style="'.esc_attr($css_text).'">'
                            . '<div class="sc_promo_block_inner">'
                            . (!empty($title) ? '<h3 class="sc_promo_title sc_item_title">' . trim(sunnyjar_strmacros($title)) . '</h3>' : '')
                            . (!empty($subtitle) ? '<h6 class="sc_promo_subtitle sc_item_subtitle">' . trim(sunnyjar_strmacros($subtitle)) . '</h6>' : '')
                            . (!empty($description) ? '<div class="sc_promo_descr sc_item_descr">' . trim(sunnyjar_strmacros($description)) . '</div>' : '')
                            . (!empty($content) ? '<div class="sc_promo_content">'.do_shortcode($content).'</div>' : '')
                            . (!empty($link) ? '<div class="sc_promo_button sc_item_button">'.sunnyjar_do_shortcode('[trx_button link="'.esc_url($link).'" icon="icon-right"]'.esc_html($link_caption).'[/trx_button]').'</div>' : '')
                            . '</div>'
                        . '</div>'
                    . '</div>'
                    . '</div>'
				. '</div>';
	
	
	
		return apply_filters('sunnyjar_shortcode_output', $output, 'trx_promo', $atts, $content);
	}
	sunnyjar_require_shortcode('trx_promo', 'sunnyjar_sc_promo');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_promo_reg_shortcodes' ) ) {
	//add_action('sunnyjar_action_shortcodes_list', 'sunnyjar_sc_promo_reg_shortcodes');
	function sunnyjar_sc_promo_reg_shortcodes() {
	
		sunnyjar_sc_map("trx_promo", array(
			"title" => esc_html__("Promo", 'sunnyjar'),
			"desc" => wp_kses_data( __("Insert promo diagramm in your page (post)", 'sunnyjar') ),
			"decorate" => true,
			"container" => false,
			"params" => array(
				"align" => array(
					"title" => esc_html__("Alignment of the promo block", 'sunnyjar'),
					"desc" => wp_kses_data( __("Align whole promo block to left or right side of the page or parent container", 'sunnyjar') ),
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('float')
				), 
				"size" => array(
					"title" => esc_html__("Size of the promo block", 'sunnyjar'),
					"desc" => wp_kses_data( __("Size of the promo block: large - one in the row, small - insize two or greater columns", 'sunnyjar') ),
					"value" => "large",
					"type" => "switch",
					"options" => array(
						'small' => esc_html__('Small', 'sunnyjar'),
						'large' => esc_html__('Large', 'sunnyjar')
					)
				), 
				"image" => array(
					"title" => esc_html__("Image URL", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select the promo image from the library for this section", 'sunnyjar') ),
					"readonly" => false,
					"value" => "",
					"type" => "media"
				),
				"image_position" => array(
					"title" => esc_html__("Image position", 'sunnyjar'),
					"desc" => wp_kses_data( __("Place the image to the left or to the right from the text block", 'sunnyjar') ),
					"value" => "left",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('hpos')
				),
				"text_margins" => array(
					"title" => esc_html__("Text margins", 'sunnyjar'),
					"desc" => wp_kses_data( __("Margins for the all sides of the text block (Example: 30px 10px 40px 30px = top right botton left OR 30px = equal for all sides)", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"text_align" => array(
					"title" => esc_html__("Text alignment", 'sunnyjar'),
					"desc" => wp_kses_data( __("Align the text inside the block", 'sunnyjar') ),
					"value" => "left",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => sunnyjar_get_sc_param('align')
				),
				"scheme" => array(
					"title" => esc_html__("Color scheme", 'sunnyjar'),
					"desc" => wp_kses_data( __("Select color scheme for the section with text", 'sunnyjar') ),
					"value" => "",
					"type" => "checklist",
					"options" => sunnyjar_get_sc_param('schemes')
				),
				"title" => array(
					"title" => esc_html__("Title", 'sunnyjar'),
					"desc" => wp_kses_data( __("Title for the block", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"subtitle" => array(
					"title" => esc_html__("Subtitle", 'sunnyjar'),
					"desc" => wp_kses_data( __("Subtitle for the block", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"description" => array(
					"title" => esc_html__("Description", 'sunnyjar'),
					"desc" => wp_kses_data( __("Short description for the block", 'sunnyjar') ),
					"value" => "",
					"type" => "textarea"
				),
				"link" => array(
					"title" => esc_html__("Button URL", 'sunnyjar'),
					"desc" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				),
				"link_caption" => array(
					"title" => esc_html__("Button caption", 'sunnyjar'),
					"desc" => wp_kses_data( __("Caption for the button at the bottom of the block", 'sunnyjar') ),
					"value" => "",
					"type" => "text"
				), 
				"width" => sunnyjar_shortcodes_width(),
				"height" => sunnyjar_shortcodes_height(),
				"top" => sunnyjar_get_sc_param('top'),
				"bottom" => sunnyjar_get_sc_param('bottom'),
				"left" => sunnyjar_get_sc_param('left'),
				"right" => sunnyjar_get_sc_param('right'),
				"id" => sunnyjar_get_sc_param('id'),
				"class" => sunnyjar_get_sc_param('class'),
				"animation" => sunnyjar_get_sc_param('animation'),
				"css" => sunnyjar_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'sunnyjar_sc_promo_reg_shortcodes_vc' ) ) {
	//add_action('sunnyjar_action_shortcodes_list_vc', 'sunnyjar_sc_promo_reg_shortcodes_vc');
	function sunnyjar_sc_promo_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_promo",
			"name" => esc_html__("Promo", 'sunnyjar'),
			"description" => wp_kses_data( __("Insert promo block", 'sunnyjar') ),
			"category" => esc_html__('Content', 'sunnyjar'),
			'icon' => 'icon_trx_promo',
			"class" => "trx_sc_collection trx_sc_promo",
			"content_element" => true,
			"is_container" => true,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment of the promo block", 'sunnyjar'),
					"description" => wp_kses_data( __("Align whole promo block to left or right side of the page or parent container", 'sunnyjar') ),
					"class" => "",
					"std" => 'none',
					"value" => array_flip(sunnyjar_get_sc_param('float')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "size",
					"heading" => esc_html__("Size of the promo block", 'sunnyjar'),
					"description" => wp_kses_data( __("Size of the promo block: large - one in the row, small - insize two or greater columns", 'sunnyjar') ),
					"class" => "",
					"value" => array(esc_html__('Use small block', 'sunnyjar') => 'small'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "image",
					"heading" => esc_html__("Image URL", 'sunnyjar'),
					"description" => wp_kses_data( __("Select the promo image from the library for this section", 'sunnyjar') ),
					"class" => "",
					"value" => "",
					"type" => "attach_image"
				),
				array(
					"param_name" => "image_position",
					"heading" => esc_html__("Image position", 'sunnyjar'),
					"description" => wp_kses_data( __("Place the image to the left or to the right from the text block", 'sunnyjar') ),
					"class" => "",
					"std" => 'left',
					"value" => array_flip(sunnyjar_get_sc_param('hpos')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "text_margins",
					"heading" => esc_html__("Text margins", 'sunnyjar'),
					"description" => wp_kses_data( __("Margins for the all sides of the text block (Example: 140px 35px 160px 35px = top right botton left OR 30px = equal for all sides)", 'sunnyjar') ),
					"value" => '',
					"type" => "textfield"
				),
				array(
					"param_name" => "text_align",
					"heading" => esc_html__("Text alignment", 'sunnyjar'),
					"description" => wp_kses_data( __("Align text to the left or to the right side inside the block", 'sunnyjar') ),
					"class" => "",
					"std" => 'left',
					"value" => array_flip(sunnyjar_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'sunnyjar'),
					"description" => wp_kses_data( __("Select color scheme for the section with text", 'sunnyjar') ),
					"class" => "",
					"value" => array_flip(sunnyjar_get_sc_param('schemes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'sunnyjar'),
					"description" => wp_kses_data( __("Title for the block", 'sunnyjar') ),
					"admin_label" => true,
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "subtitle",
					"heading" => esc_html__("Subtitle", 'sunnyjar'),
					"description" => wp_kses_data( __("Subtitle for the block", 'sunnyjar') ),
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Description", 'sunnyjar'),
					"description" => wp_kses_data( __("Description for the block", 'sunnyjar') ),
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textarea"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Button URL", 'sunnyjar'),
					"description" => wp_kses_data( __("Link URL for the button at the bottom of the block", 'sunnyjar') ),
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_caption",
					"heading" => esc_html__("Button caption", 'sunnyjar'),
					"description" => wp_kses_data( __("Caption for the button at the bottom of the block", 'sunnyjar') ),
					"group" => esc_html__('Captions', 'sunnyjar'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				sunnyjar_get_vc_param('id'),
				sunnyjar_get_vc_param('class'),
				sunnyjar_get_vc_param('animation'),
				sunnyjar_get_vc_param('css'),
				sunnyjar_vc_width(),
				sunnyjar_vc_height(),
				sunnyjar_get_vc_param('margin_top'),
				sunnyjar_get_vc_param('margin_bottom'),
				sunnyjar_get_vc_param('margin_left'),
				sunnyjar_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Promo extends SUNNYJAR_VC_ShortCodeCollection {}
	}
}
?>