<?php
/**
 * Single post
 */
get_header(); 

$single_style = sunnyjar_storage_get('single_style');
if (empty($single_style)) $single_style = sunnyjar_get_custom_option('single_style');

while ( have_posts() ) { the_post();
	sunnyjar_show_post_layout(
		array(
			'layout' => $single_style,
			'sidebar' => !sunnyjar_param_is_off(sunnyjar_get_custom_option('show_sidebar_main')),
			'content' => sunnyjar_get_template_property($single_style, 'need_content'),
			'terms_list' => sunnyjar_get_template_property($single_style, 'need_terms')
		)
	);
}

get_footer();
?>