<?php
/**
 * Theme sprecific functions and definitions
 */

/* Theme setup section
------------------------------------------------------------------- */

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) $content_width = 1170; /* pixels */

// Add theme specific actions and filters
// Attention! Function were add theme specific actions and filters handlers must have priority 1
if ( !function_exists( 'sunnyjar_theme_setup' ) ) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_theme_setup', 1 );
	function sunnyjar_theme_setup() {

		// Register theme menus
		add_filter( 'sunnyjar_filter_add_theme_menus',		'sunnyjar_add_theme_menus' );

		// Register theme sidebars
		add_filter( 'sunnyjar_filter_add_theme_sidebars',	'sunnyjar_add_theme_sidebars' );

		// Set options for importer
		add_filter( 'sunnyjar_filter_importer_options',		'sunnyjar_set_importer_options' );

		// Add theme required plugins
		add_filter( 'sunnyjar_filter_required_plugins',		'sunnyjar_add_required_plugins' );

		// Add theme specified classes into the body
		add_filter( 'body_class', 'sunnyjar_body_classes' );

		// Set list of the theme required plugins
		sunnyjar_storage_set('required_plugins', array(
                'essgrids',
                'instagram_widget',
                'revslider',
                'trx_utils',
                'visual_composer',
                'woocommerce',
                'vc_extensions_bundle'
			)
		);
		
		if ( is_dir(SUNNYJAR_THEME_PATH . 'demo/') ) {
			sunnyjar_storage_set('demo_data_url',  SUNNYJAR_THEME_PATH . 'demo/');
		} else {
			sunnyjar_storage_set('demo_data_url',  esc_url(sunnyjar_get_protocol().'://sunnyjar.ancorathemes.com/demo') ); // Demo-site domain
		}
		
	}
}


// Add/Remove theme nav menus
if ( !function_exists( 'sunnyjar_add_theme_menus' ) ) {
	//add_filter( 'sunnyjar_filter_add_theme_menus', 'sunnyjar_add_theme_menus' );
	function sunnyjar_add_theme_menus($menus) {
		//For example:
		//$menus['menu_footer'] = esc_html__('Footer Menu', 'sunnyjar');
		//if (isset($menus['menu_panel'])) unset($menus['menu_panel']);
		return $menus;
	}
}


// Add theme specific widgetized areas
if ( !function_exists( 'sunnyjar_add_theme_sidebars' ) ) {
	//add_filter( 'sunnyjar_filter_add_theme_sidebars',	'sunnyjar_add_theme_sidebars' );
	function sunnyjar_add_theme_sidebars($sidebars=array()) {
		if (is_array($sidebars)) {
			$theme_sidebars = array(
				'sidebar_main'		=> esc_html__( 'Main Sidebar', 'sunnyjar' ),
				'sidebar_footer'	=> esc_html__( 'Footer Sidebar', 'sunnyjar' )
			);
			if (function_exists('sunnyjar_exists_woocommerce') && sunnyjar_exists_woocommerce()) {
				$theme_sidebars['sidebar_cart']  = esc_html__( 'WooCommerce Cart Sidebar', 'sunnyjar' );
			}
			$sidebars = array_merge($theme_sidebars, $sidebars);
		}
		return $sidebars;
	}
}


// Add theme required plugins
if ( !function_exists( 'sunnyjar_add_required_plugins' ) ) {
	//add_filter( 'sunnyjar_filter_required_plugins',		'sunnyjar_add_required_plugins' );
	function sunnyjar_add_required_plugins($plugins) {
		$plugins[] = array(
			'name' 		=> esc_html__('Sunnyjar Utilities', 'sunnyjar'),
			'version'	=> '2.7',					// Minimal required version
			'slug' 		=> 'trx_utils',
			'source'	=> sunnyjar_get_file_dir('plugins/install/trx_utils.zip'),
			'required' 	=> true
		);
		return $plugins;
	}
}


// Add theme specified classes into the body
if ( !function_exists('sunnyjar_body_classes') ) {
	//add_filter( 'body_class', 'sunnyjar_body_classes' );
	function sunnyjar_body_classes( $classes ) {

		$classes[] = 'sunnyjar_body';
		$classes[] = 'body_style_' . trim(sunnyjar_get_custom_option('body_style'));
		$classes[] = 'body_' . (sunnyjar_get_custom_option('body_filled')=='yes' ? 'filled' : 'transparent');
		$classes[] = 'theme_skin_' . trim(sunnyjar_get_custom_option('theme_skin'));
		$classes[] = 'article_style_' . trim(sunnyjar_get_custom_option('article_style'));
		
		$blog_style = sunnyjar_get_custom_option(is_singular() && !sunnyjar_storage_get('blog_streampage') ? 'single_style' : 'blog_style');
		$classes[] = 'layout_' . trim($blog_style);
		$classes[] = 'template_' . trim(sunnyjar_get_template_name($blog_style));
		
		$body_scheme = sunnyjar_get_custom_option('body_scheme');
		if (empty($body_scheme)  || sunnyjar_is_inherit_option($body_scheme)) $body_scheme = 'original';
		$classes[] = 'scheme_' . $body_scheme;

		$top_panel_position = sunnyjar_get_custom_option('top_panel_position');
		if (!sunnyjar_param_is_off($top_panel_position)) {
			$classes[] = 'top_panel_show';
			$classes[] = 'top_panel_' . trim($top_panel_position);
		} else 
			$classes[] = 'top_panel_hide';
		$classes[] = sunnyjar_get_sidebar_class();

		if (sunnyjar_get_custom_option('show_video_bg')=='yes' && (sunnyjar_get_custom_option('video_bg_youtube_code')!='' || sunnyjar_get_custom_option('video_bg_url')!=''))
			$classes[] = 'video_bg_show';

		if (sunnyjar_get_theme_option('page_preloader')!='')
			$classes[] = 'preloader';

		return $classes;
	}
}

function sunnyjar_set_importer_options($options=array()) {
	if (is_array($options)) {
		// Default demo
		$options['demo_url'] = sunnyjar_storage_get('demo_data_url');
		$options['files']['default']['title'] = esc_html__('Sunnyjar Demo', 'sunnyjar');
		$options['files']['default']['domain_dev'] = '';    // Developers domain
		$options['files']['default']['domain_demo']= esc_url(sunnyjar_get_protocol().'://sunnyjar.ancorathemes.com');        // Demo-site domain
	}
	return $options;
}

// Theme options
if ( !function_exists( 'sunnyjar_theme_options' ) ) {
    function sunnyjar_theme_options(){

        $theme_init = array();
        $theme_init['theme_skin'] = sunnyjar_esc(sunnyjar_get_custom_option('theme_skin'));
        $theme_init['body_scheme'] = sunnyjar_get_custom_option('body_scheme');
        if (empty($theme_init['body_scheme']) || sunnyjar_is_inherit_option($theme_init['body_scheme'])) $theme_init['body_scheme'] = 'original';
        $theme_init['blog_style'] = sunnyjar_get_custom_option(is_singular() && !sunnyjar_storage_get('blog_streampage') ? 'single_style' : 'blog_style');
        $theme_init['body_style'] = sunnyjar_get_custom_option('body_style');
        $theme_init['article_style'] = sunnyjar_get_custom_option('article_style');
        $theme_init['top_panel_style'] = sunnyjar_get_custom_option('top_panel_style');
        $theme_init['top_panel_position'] = sunnyjar_get_custom_option('top_panel_position');
        $theme_init['top_panel_scheme'] = sunnyjar_get_custom_option('top_panel_scheme');
        $theme_init['video_bg_show'] = sunnyjar_get_custom_option('show_video_bg') == 'yes' && (sunnyjar_get_custom_option('video_bg_youtube_code') != '' || sunnyjar_get_custom_option('video_bg_url') != '');

        return $theme_init;
    }
}


// Page preloader options
if ( !function_exists( 'sunnyjar_page_preloader_style_css' ) ) {
    function sunnyjar_page_preloader_style_css()    {
        if (($preloader = sunnyjar_get_theme_option('page_preloader')) != '') {
            $clr = sunnyjar_get_scheme_color('bg_color');
            ?>
            <style type="text/css">
                <!--
                #page_preloader {
                    background-color: <?php echo esc_attr($clr); ?>;
                    background-image: url(<?php echo esc_url($preloader); ?>);
                    background-position: center;
                    background-repeat: no-repeat;
                    position: fixed;
                    z-index: 1000000;
                    left: 0;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    opacity: 0.8;
                }
                -->
            </style>
            <?php
        }
    }
}


// Add TOC items 'Home' and "To top"
if ( !function_exists( 'sunnyjar_add_toc' ) ) {
    function sunnyjar_add_toc()    {
        if (sunnyjar_get_custom_option('menu_toc_home')=='yes')
	        sunnyjar_show_layout(sunnyjar_sc_anchor(array(
                    'id' => "toc_home",
                    'title' => esc_html__('Home', 'sunnyjar'),
                    'description' => esc_html__('{{Return to Home}} - ||navigate to home page of the site', 'sunnyjar'),
                    'icon' => "icon-home",
                    'separator' => "yes",
                    'url' => esc_url(home_url('/'))
                )
            ));
        if (sunnyjar_get_custom_option('menu_toc_top')=='yes')
	        sunnyjar_show_layout(sunnyjar_sc_anchor(array(
                    'id' => "toc_top",
                    'title' => esc_html__('To Top', 'sunnyjar'),
                    'description' => esc_html__('{{Back to top}} - ||scroll to top of the page', 'sunnyjar'),
                    'icon' => "icon-double-up",
                    'separator' => "yes")
            ));
    }
}


/* Include framework core files
------------------------------------------------------------------- */
// If now is WP Heartbeat call - skip loading theme core files (to reduce server and DB uploads)
    require_once trailingslashit( get_template_directory() ) . 'fw/loader.php';
?>