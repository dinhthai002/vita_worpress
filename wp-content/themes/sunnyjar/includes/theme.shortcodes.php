<?php
if (!function_exists('sunnyjar_theme_shortcodes_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_theme_shortcodes_setup', 1 );
	function sunnyjar_theme_shortcodes_setup() {
		add_filter('sunnyjar_filter_googlemap_styles', 'sunnyjar_theme_shortcodes_googlemap_styles');
	}
}


// Add theme-specific Google map styles
if ( !function_exists( 'sunnyjar_theme_shortcodes_googlemap_styles' ) ) {
	function sunnyjar_theme_shortcodes_googlemap_styles($list) {
		$list['light']		= esc_html__('Light', 'sunnyjar');
		$list['simple']		= esc_html__('Simple', 'sunnyjar');
		$list['greyscale']	= esc_html__('Greyscale', 'sunnyjar');
		$list['inverse']	= esc_html__('Inverse', 'sunnyjar');
		return $list;
	}
}
?>