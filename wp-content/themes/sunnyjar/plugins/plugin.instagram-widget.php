<?php
/* Instagram Widget support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('sunnyjar_instagram_widget_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_instagram_widget_theme_setup', 1 );
	function sunnyjar_instagram_widget_theme_setup() {
		if (sunnyjar_exists_instagram_widget()) {
			add_action( 'sunnyjar_action_add_styles', 						'sunnyjar_instagram_widget_frontend_scripts' );
		}
		if (is_admin()) {
			add_filter( 'sunnyjar_filter_importer_required_plugins',		'sunnyjar_instagram_widget_importer_required_plugins', 10, 2 );
			add_filter( 'sunnyjar_filter_required_plugins',					'sunnyjar_instagram_widget_required_plugins' );
		}
	}
}

// Check if Instagram Widget installed and activated
if ( !function_exists( 'sunnyjar_exists_instagram_widget' ) ) {
	function sunnyjar_exists_instagram_widget() {
		return function_exists('wpiw_init');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'sunnyjar_instagram_widget_required_plugins' ) ) {
	//add_filter('sunnyjar_filter_required_plugins',	'sunnyjar_instagram_widget_required_plugins');
	function sunnyjar_instagram_widget_required_plugins($list=array()) {
		if (in_array('instagram_widget', sunnyjar_storage_get('required_plugins')))
			$list[] = array(
					'name' 		=> esc_html__('Instagram Widget', 'sunnyjar'),
					'slug' 		=> 'wp-instagram-widget',
					'required' 	=> false
				);
		return $list;
	}
}

// Enqueue custom styles
if ( !function_exists( 'sunnyjar_instagram_widget_frontend_scripts' ) ) {
	//add_action( 'sunnyjar_action_add_styles', 'sunnyjar_instagram_widget_frontend_scripts' );
	function sunnyjar_instagram_widget_frontend_scripts() {
		if (file_exists(sunnyjar_get_file_dir('css/plugin.instagram-widget.css')))
			sunnyjar_enqueue_style( 'sunnyjar-plugin.instagram-widget-style',  sunnyjar_get_file_url('css/plugin.instagram-widget.css'), array(), null );
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check Instagram Widget in the required plugins
if ( !function_exists( 'sunnyjar_instagram_widget_importer_required_plugins' ) ) {
	//add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_instagram_widget_importer_required_plugins', 10, 2 );
	function sunnyjar_instagram_widget_importer_required_plugins($not_installed='', $list='') {
		if (sunnyjar_strpos($list, 'instagram_widget')!==false && !sunnyjar_exists_instagram_widget() )
			$not_installed .= '<br>' . esc_html__('WP Instagram Widget', 'sunnyjar');
		return $not_installed;
	}
}
?>