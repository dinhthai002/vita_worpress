<?php
/* HTML5 jQuery Audio Player support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('sunnyjar_html5_jquery_audio_player_theme_setup')) {
    add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_html5_jquery_audio_player_theme_setup' );
    function sunnyjar_html5_jquery_audio_player_theme_setup() {
        // Add shortcode in the shortcodes list
        if (sunnyjar_exists_html5_jquery_audio_player()) {
			add_action('sunnyjar_action_add_styles',					'sunnyjar_html5_jquery_audio_player_frontend_scripts' );
            add_action('sunnyjar_action_shortcodes_list',				'sunnyjar_html5_jquery_audio_player_reg_shortcodes');
			if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
	            add_action('sunnyjar_action_shortcodes_list_vc',		'sunnyjar_html5_jquery_audio_player_reg_shortcodes_vc');
            if (is_admin()) {
                add_filter( 'sunnyjar_filter_importer_options',			'sunnyjar_html5_jquery_audio_player_importer_set_options', 10, 1 );
                add_action( 'sunnyjar_action_importer_params',			'sunnyjar_html5_jquery_audio_player_importer_show_params', 10, 1 );
                add_action( 'sunnyjar_action_importer_import',			'sunnyjar_html5_jquery_audio_player_importer_import', 10, 2 );
				add_action( 'sunnyjar_action_importer_import_fields',	'sunnyjar_html5_jquery_audio_player_importer_import_fields', 10, 1 );
                add_action( 'sunnyjar_action_importer_export',			'sunnyjar_html5_jquery_audio_player_importer_export', 10, 1 );
                add_action( 'sunnyjar_action_importer_export_fields',	'sunnyjar_html5_jquery_audio_player_importer_export_fields', 10, 1 );
            }
        }
        if (is_admin()) {
            add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_html5_jquery_audio_player_importer_required_plugins', 10, 2 );
            add_filter( 'sunnyjar_filter_required_plugins',				'sunnyjar_html5_jquery_audio_player_required_plugins' );
        }
    }
}

// Check if plugin installed and activated
if ( !function_exists( 'sunnyjar_exists_html5_jquery_audio_player' ) ) {
	function sunnyjar_exists_html5_jquery_audio_player() {
		return function_exists('hmp_db_create');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_required_plugins' ) ) {
	//add_filter('sunnyjar_filter_required_plugins',	'sunnyjar_html5_jquery_audio_player_required_plugins');
	function sunnyjar_html5_jquery_audio_player_required_plugins($list=array()) {
        if (in_array('html5_jquery_audio_player', sunnyjar_storage_get('required_plugins')))
            $list[] = array(
					'name' 		=> esc_html__('HTML5 jQuery Audio Player', 'sunnyjar'),
					'slug' 		=> 'html5-jquery-audio-player',
					'required' 	=> false
				);
		    return $list;
	}
}

// Enqueue custom styles
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_frontend_scripts' ) ) {
	//add_action( 'sunnyjar_action_add_styles', 'sunnyjar_html5_jquery_audio_player_frontend_scripts' );
	function sunnyjar_html5_jquery_audio_player_frontend_scripts() {
		if (file_exists(sunnyjar_get_file_dir('css/plugin.html5-jquery-audio-player.css'))) {
			sunnyjar_enqueue_style( 'sunnyjar-plugin.html5-jquery-audio-player-style',  sunnyjar_get_file_url('css/plugin.html5-jquery-audio-player.css'), array(), null );
		}
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check HTML5 jQuery Audio Player in the required plugins
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_importer_required_plugins' ) ) {
	//add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_html5_jquery_audio_player_importer_required_plugins', 10, 2 );
	function sunnyjar_html5_jquery_audio_player_importer_required_plugins($not_installed='', $list=null) {
		if (sunnyjar_strpos($list, 'html5_jquery_audio_player')!==false && !sunnyjar_exists_html5_jquery_audio_player() )
			$not_installed .= '<br>' . esc_html__('HTML5 jQuery Audio Player', 'sunnyjar');
		return $not_installed;
	}
}


// Set options for one-click importer
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_importer_set_options' ) ) {
    //add_filter( 'sunnyjar_filter_importer_options',	'sunnyjar_html5_jquery_audio_player_importer_set_options', 10, 1 );
    function sunnyjar_html5_jquery_audio_player_importer_set_options($options=array()) {
		if ( in_array('html5_jquery_audio_player', sunnyjar_storage_get('required_plugins')) && sunnyjar_exists_html5_jquery_audio_player() ) {
			if (is_array($options['files']) && count($options['files']) > 0) {
				foreach ($options['files'] as $k => $v) {
					$options['files'][$k]['file_with_html5_jquery_audio_player'] = str_replace('name.ext', 'html5_jquery_audio_player.txt', $v['file_with_']);
				}
			}
			// Add option's slugs to export options for this plugin
            $options['additional_options'][] = 'showbuy';
            $options['additional_options'][] = 'buy_text';
            $options['additional_options'][] = 'showlist';
            $options['additional_options'][] = 'autoplay';
            $options['additional_options'][] = 'tracks';
            $options['additional_options'][] = 'currency';
            $options['additional_options'][] = 'color';
            $options['additional_options'][] = 'tcolor';
        }
        return $options;
    }
}

// Add checkbox to the one-click importer
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_importer_show_params' ) ) {
    //add_action( 'sunnyjar_action_importer_params',	'sunnyjar_html5_jquery_audio_player_importer_show_params', 10, 1 );
    function sunnyjar_html5_jquery_audio_player_importer_show_params($importer) {
		$importer->show_importer_params(array(
			'slug' => 'html5_jquery_audio_player',
			'title' => esc_html__('Import HTML5 jQuery Audio Player', 'sunnyjar'),
			'part' => 0
			));
    }
}


// Import posts
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_importer_import' ) ) {
    //add_action( 'sunnyjar_action_importer_import',	'sunnyjar_html5_jquery_audio_player_importer_import', 10, 2 );
    function sunnyjar_html5_jquery_audio_player_importer_import($importer, $action) {
		if ( $action == 'import_html5_jquery_audio_player' ) {
			$importer->response['start_from_id'] = 0;
            $importer->import_dump('html5_jquery_audio_player', esc_html__('HTML5 jQuery Audio Player', 'sunnyjar'));
        }
    }
}

// Display import progress
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_importer_import_fields' ) ) {
	//add_action( 'sunnyjar_action_importer_import_fields',	'sunnyjar_html5_jquery_audio_player_importer_import_fields', 10, 1 );
	function sunnyjar_html5_jquery_audio_player_importer_import_fields($importer) {
		$importer->show_importer_fields(array(
			'slug' => 'html5_jquery_audio_player',
			'title' => esc_html__('HTML5 jQuery Audio Player', 'sunnyjar')
			));
	}
}


// Export posts
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_importer_export' ) ) {
    //add_action( 'sunnyjar_action_importer_export',	'sunnyjar_html5_jquery_audio_player_importer_export', 10, 1 );
    function sunnyjar_html5_jquery_audio_player_importer_export($importer) {
		sunnyjar_fpc(sunnyjar_get_file_dir('core/core.importer/export/html5_jquery_audio_player.txt'), serialize( array(
			'hmp_playlist'	=> $importer->export_dump('hmp_playlist'),
			'hmp_rating'	=> $importer->export_dump('hmp_rating')
			) )
		);
    }
}


// Display exported data in the fields
if ( !function_exists( 'sunnyjar_html5_jquery_audio_player_importer_export_fields' ) ) {
    //add_action( 'sunnyjar_action_importer_export_fields',	'sunnyjar_html5_jquery_audio_player_importer_export_fields', 10, 1 );
    function sunnyjar_html5_jquery_audio_player_importer_export_fields($importer) {
		$importer->show_exporter_fields(array(
			'slug' => 'html5_jquery_audio_player',
			'title' => esc_html__('HTML5 jQuery Audio Player', 'sunnyjar')
			));
    }
}





// Shortcodes
//------------------------------------------------------------------------

// Register shortcode in the shortcodes list
if (!function_exists('sunnyjar_html5_jquery_audio_player_reg_shortcodes')) {
    //add_filter('sunnyjar_action_shortcodes_list',	'sunnyjar_html5_jquery_audio_player_reg_shortcodes');
    function sunnyjar_html5_jquery_audio_player_reg_shortcodes() {
		if (sunnyjar_storage_isset('shortcodes')) {
			sunnyjar_sc_map_after('trx_audio', 'hmp_player', array(
                "title" => esc_html__("HTML5 jQuery Audio Player", 'sunnyjar'),
                "desc" => esc_html__("Insert HTML5 jQuery Audio Player", 'sunnyjar'),
                "decorate" => true,
                "container" => false,
				"params" => array()
				)
            );
        }
    }
}


// Register shortcode in the VC shortcodes list
if (!function_exists('sunnyjar_hmp_player_reg_shortcodes_vc')) {
    add_filter('sunnyjar_action_shortcodes_list_vc',	'sunnyjar_hmp_player_reg_shortcodes_vc');
    function sunnyjar_hmp_player_reg_shortcodes_vc() {

        // Sunnyjar HTML5 jQuery Audio Player
        vc_map( array(
            "base" => "hmp_player",
            "name" => esc_html__("HTML5 jQuery Audio Player", 'sunnyjar'),
            "description" => esc_html__("Insert HTML5 jQuery Audio Player", 'sunnyjar'),
            "category" => esc_html__('Content', 'sunnyjar'),
            'icon' => 'icon_trx_audio',
            "class" => "trx_sc_single trx_sc_hmp_player",
            "content_element" => true,
            "is_container" => false,
            "show_settings_on_create" => false,
            "params" => array()
        ) );

        class WPBakeryShortCode_Hmp_Player extends SUNNYJAR_VC_ShortCodeSingle {}

    }
}
?>