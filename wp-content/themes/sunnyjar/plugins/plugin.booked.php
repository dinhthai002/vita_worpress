<?php
/* Booked Appointments support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('sunnyjar_booked_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_booked_theme_setup', 1 );
	function sunnyjar_booked_theme_setup() {
		// Register shortcode in the shortcodes list
		if (sunnyjar_exists_booked()) {
			add_action('sunnyjar_action_add_styles', 					'sunnyjar_booked_frontend_scripts');
			add_action('sunnyjar_action_shortcodes_list',				'sunnyjar_booked_reg_shortcodes');
			if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
				add_action('sunnyjar_action_shortcodes_list_vc',		'sunnyjar_booked_reg_shortcodes_vc');
			if (is_admin()) {
				add_filter( 'sunnyjar_filter_importer_options',			'sunnyjar_booked_importer_set_options' );
				add_filter( 'sunnyjar_filter_importer_import_row',		'sunnyjar_booked_importer_check_row', 9, 4);
			}
		}
		if (is_admin()) {
			add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_booked_importer_required_plugins', 10, 2);
			add_filter( 'sunnyjar_filter_required_plugins',				'sunnyjar_booked_required_plugins' );
		}
	}
}


// Check if plugin installed and activated
if ( !function_exists( 'sunnyjar_exists_booked' ) ) {
	function sunnyjar_exists_booked() {
		return class_exists('booked_plugin');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'sunnyjar_booked_required_plugins' ) ) {
	//add_filter('sunnyjar_filter_required_plugins',	'sunnyjar_booked_required_plugins');
	function sunnyjar_booked_required_plugins($list=array()) {
		if (in_array('booked', sunnyjar_storage_get('required_plugins'))) {
			$path = sunnyjar_get_file_dir('plugins/install/booked.zip');
			if (file_exists($path)) {
				$list[] = array(
					'name' 		=> esc_html__('Booked', 'sunnyjar'),
					'slug' 		=> 'booked',
					'source'	=> $path,
					'required' 	=> false
					);
			}
		}
		return $list;
	}
}

// Enqueue custom styles
if ( !function_exists( 'sunnyjar_booked_frontend_scripts' ) ) {
	//add_action( 'sunnyjar_action_add_styles', 'sunnyjar_booked_frontend_scripts' );
	function sunnyjar_booked_frontend_scripts() {
		if (file_exists(sunnyjar_get_file_dir('css/plugin.booked.css')))
			sunnyjar_enqueue_style( 'sunnyjar-plugin.booked-style',  sunnyjar_get_file_url('css/plugin.booked.css'), array(), null );
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check in the required plugins
if ( !function_exists( 'sunnyjar_booked_importer_required_plugins' ) ) {
	//add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_booked_importer_required_plugins', 10, 2);
	function sunnyjar_booked_importer_required_plugins($not_installed='', $list='') {
		//if (in_array('booked', sunnyjar_storage_get('required_plugins')) && !sunnyjar_exists_booked() )
		if (sunnyjar_strpos($list, 'booked')!==false && !sunnyjar_exists_booked() )
			$not_installed .= '<br>' . esc_html__('Booked Appointments', 'sunnyjar');
		return $not_installed;
	}
}

// Set options for one-click importer
if ( !function_exists( 'sunnyjar_booked_importer_set_options' ) ) {
	//add_filter( 'sunnyjar_filter_importer_options',	'sunnyjar_booked_importer_set_options', 10, 1 );
	function sunnyjar_booked_importer_set_options($options=array()) {
		if (in_array('booked', sunnyjar_storage_get('required_plugins')) && sunnyjar_exists_booked()) {
			$options['additional_options'][] = 'booked_%';		// Add slugs to export options for this plugin
		}
		return $options;
	}
}

// Check if the row will be imported
if ( !function_exists( 'sunnyjar_booked_importer_check_row' ) ) {
	//add_filter('sunnyjar_filter_importer_import_row', 'sunnyjar_booked_importer_check_row', 9, 4);
	function sunnyjar_booked_importer_check_row($flag, $table, $row, $list) {
		if ($flag || strpos($list, 'booked')===false) return $flag;
		if ( sunnyjar_exists_booked() ) {
			if ($table == 'posts')
				$flag = $row['post_type']=='booked_appointments';
		}
		return $flag;
	}
}


// Lists
//------------------------------------------------------------------------

// Return booked calendars list, prepended inherit (if need)
if ( !function_exists( 'sunnyjar_get_list_booked_calendars' ) ) {
	function sunnyjar_get_list_booked_calendars($prepend_inherit=false) {
		return sunnyjar_exists_booked() ? sunnyjar_get_list_terms($prepend_inherit, 'booked_custom_calendars') : array();
	}
}



// Register plugin's shortcodes
//------------------------------------------------------------------------

// Register shortcode in the shortcodes list
if (!function_exists('sunnyjar_booked_reg_shortcodes')) {
	//add_filter('sunnyjar_action_shortcodes_list',	'sunnyjar_booked_reg_shortcodes');
	function sunnyjar_booked_reg_shortcodes() {
		if (sunnyjar_storage_isset('shortcodes')) {

			$booked_cals = sunnyjar_get_list_booked_calendars();

			sunnyjar_sc_map('booked-appointments', array(
				"title" => esc_html__("Booked Appointments", 'sunnyjar'),
				"desc" => esc_html__("Display the currently logged in user's upcoming appointments", 'sunnyjar'),
				"decorate" => true,
				"container" => false,
				"params" => array()
				)
			);

			sunnyjar_sc_map('booked-calendar', array(
				"title" => esc_html__("Booked Calendar", 'sunnyjar'),
				"desc" => esc_html__("Insert booked calendar", 'sunnyjar'),
				"decorate" => true,
				"container" => false,
				"params" => array(
					"calendar" => array(
						"title" => esc_html__("Calendar", 'sunnyjar'),
						"desc" => esc_html__("Select booked calendar to display", 'sunnyjar'),
						"value" => "0",
						"type" => "select",
						"options" => sunnyjar_array_merge(array(0 => esc_html__('- Select calendar -', 'sunnyjar')), $booked_cals)
					),
					"year" => array(
						"title" => esc_html__("Year", 'sunnyjar'),
						"desc" => esc_html__("Year to display on calendar by default", 'sunnyjar'),
						"value" => date("Y"),
						"min" => date("Y"),
						"max" => date("Y")+10,
						"type" => "spinner"
					),
					"month" => array(
						"title" => esc_html__("Month", 'sunnyjar'),
						"desc" => esc_html__("Month to display on calendar by default", 'sunnyjar'),
						"value" => date("m"),
						"min" => 1,
						"max" => 12,
						"type" => "spinner"
					)
				)
			));
		}
	}
}


// Register shortcode in the VC shortcodes list
if (!function_exists('sunnyjar_booked_reg_shortcodes_vc')) {
	//add_filter('sunnyjar_action_shortcodes_list_vc',	'sunnyjar_booked_reg_shortcodes_vc');
	function sunnyjar_booked_reg_shortcodes_vc() {

		$booked_cals = sunnyjar_get_list_booked_calendars();

		// Booked Appointments
		vc_map( array(
				"base" => "booked-appointments",
				"name" => esc_html__("Booked Appointments", 'sunnyjar'),
				"description" => esc_html__("Display the currently logged in user's upcoming appointments", 'sunnyjar'),
				"category" => esc_html__('Content', 'sunnyjar'),
				'icon' => 'icon_trx_booked',
				"class" => "trx_sc_single trx_sc_booked_appointments",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => false,
				"params" => array()
			) );
			
		class WPBakeryShortCode_Booked_Appointments extends SUNNYJAR_VC_ShortCodeSingle {}

		// Booked Calendar
		vc_map( array(
				"base" => "booked-calendar",
				"name" => esc_html__("Booked Calendar", 'sunnyjar'),
				"description" => esc_html__("Insert booked calendar", 'sunnyjar'),
				"category" => esc_html__('Content', 'sunnyjar'),
				'icon' => 'icon_trx_booked',
				"class" => "trx_sc_single trx_sc_booked_calendar",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "calendar",
						"heading" => esc_html__("Calendar", 'sunnyjar'),
						"description" => esc_html__("Select booked calendar to display", 'sunnyjar'),
						"admin_label" => true,
						"class" => "",
						"std" => "0",
						"value" => array_flip(sunnyjar_array_merge(array(0 => esc_html__('- Select calendar -', 'sunnyjar')), $booked_cals)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "year",
						"heading" => esc_html__("Year", 'sunnyjar'),
						"description" => esc_html__("Year to display on calendar by default", 'sunnyjar'),
						"admin_label" => true,
						"class" => "",
						"std" => date("Y"),
						"value" => date("Y"),
						"type" => "textfield"
					),
					array(
						"param_name" => "month",
						"heading" => esc_html__("Month", 'sunnyjar'),
						"description" => esc_html__("Month to display on calendar by default", 'sunnyjar'),
						"admin_label" => true,
						"class" => "",
						"std" => date("m"),
						"value" => date("m"),
						"type" => "textfield"
					)
				)
			) );
			
		class WPBakeryShortCode_Booked_Calendar extends SUNNYJAR_VC_ShortCodeSingle {}

	}
}
?>