<?php
/* Calculated fields form support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('sunnyjar_calcfields_form_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_calcfields_form_theme_setup', 1 );
	function sunnyjar_calcfields_form_theme_setup() {
		// Register shortcode in the shortcodes list
		if (sunnyjar_exists_calcfields_form()) {
			add_action('sunnyjar_action_shortcodes_list',				'sunnyjar_calcfields_form_reg_shortcodes');
			if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
				add_action('sunnyjar_action_shortcodes_list_vc',		'sunnyjar_calcfields_form_reg_shortcodes_vc');
			if (is_admin()) {
				add_filter( 'sunnyjar_filter_importer_options',			'sunnyjar_calcfields_form_importer_set_options', 10, 1 );
				add_action( 'sunnyjar_action_importer_params',			'sunnyjar_calcfields_form_importer_show_params', 10, 1 );
				add_action( 'sunnyjar_action_importer_import',			'sunnyjar_calcfields_form_importer_import', 10, 2 );
				add_action( 'sunnyjar_action_importer_import_fields',	'sunnyjar_calcfields_form_importer_import_fields', 10, 1 );
				add_action( 'sunnyjar_action_importer_export',			'sunnyjar_calcfields_form_importer_export', 10, 1 );
				add_action( 'sunnyjar_action_importer_export_fields',	'sunnyjar_calcfields_form_importer_export_fields', 10, 1 );
			}
			add_action('wp_enqueue_scripts', 							'sunnyjar_calcfields_form_frontend_scripts');
		}
		if (is_admin()) {
			add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_calcfields_form_importer_required_plugins', 10, 2 );
			add_filter( 'sunnyjar_filter_required_plugins',				'sunnyjar_calcfields_form_required_plugins' );
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'sunnyjar_exists_calcfields_form' ) ) {
	function sunnyjar_exists_calcfields_form() {
		return defined('CP_SCHEME');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'sunnyjar_calcfields_form_required_plugins' ) ) {
	//add_filter('sunnyjar_filter_required_plugins',	'sunnyjar_calcfields_form_required_plugins');
	function sunnyjar_calcfields_form_required_plugins($list=array()) {
		if (in_array('calcfields', sunnyjar_storage_get('required_plugins')))
			$list[] = array(
					'name' 		=> 'Calculated Fields Form',
					'slug' 		=> 'calculated-fields-form',
					'required' 	=> false
					);
		return $list;
	}
}

// Remove jquery_ui from frontend
if ( !function_exists( 'sunnyjar_calcfields_form_frontend_scripts' ) ) {
	//add_action('wp_enqueue_scripts', 'sunnyjar_calcfields_form_frontend_scripts');
	function sunnyjar_calcfields_form_frontend_scripts() {
		// Disable loading JQuery UI CSS
		//global $wp_styles, $wp_scripts;
		//$wp_styles->done[] = 'cpcff_jquery_ui';
	}
}


// One-click import support
//------------------------------------------------------------------------

// Check in the required plugins
if ( !function_exists( 'sunnyjar_calcfields_form_importer_required_plugins' ) ) {
	//add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_calcfields_form_importer_required_plugins', 10, 2 );
	function sunnyjar_calcfields_form_importer_required_plugins($not_installed='', $list='') {
		if (sunnyjar_strpos($list, 'calcfields')!==false && !sunnyjar_exists_calcfields_form() )
			$not_installed .= '<br>'.esc_html__('Calculated Fields Form', 'sunnyjar');
		return $not_installed;
	}
}

// Set options for one-click importer
if ( !function_exists( 'sunnyjar_calcfields_form_importer_set_options' ) ) {
	//add_filter( 'sunnyjar_filter_importer_options',	'sunnyjar_calcfields_form_importer_set_options', 10, 1 );
	function sunnyjar_calcfields_form_importer_set_options($options=array()) {
		if ( in_array('calcfields', sunnyjar_storage_get('required_plugins')) && sunnyjar_exists_calcfields_form() ) {
			$options['additional_options'][]	= 'CP_CFF_LOAD_SCRIPTS';				// Add slugs to export options of this plugin
			$options['additional_options'][]	= 'CP_CALCULATEDFIELDSF_USE_CACHE';
			$options['additional_options'][]	= 'CP_CALCULATEDFIELDSF_EXCLUDE_CRAWLERS';
			if (is_array($options['files']) && count($options['files']) > 0) {
				foreach ($options['files'] as $k => $v) {
					$options['files'][$k]['file_with_calcfields_form'] = str_replace('name.ext', 'calcfields_form.txt', $v['file_with_']);
				}
			}
		}
		return $options;
	}
}

// Add checkbox to the one-click importer
if ( !function_exists( 'sunnyjar_calcfields_form_importer_show_params' ) ) {
	//add_action( 'sunnyjar_action_importer_params',	'sunnyjar_calcfields_form_importer_show_params', 10, 1 );
	function sunnyjar_calcfields_form_importer_show_params($importer) {
		$importer->show_importer_params(array(
			'slug' => 'calcfields_form',
			'title' => esc_html__('Import Calculated Fields Form', 'sunnyjar'),
			'part' => 1
			));
	}
}

// Import posts
if ( !function_exists( 'sunnyjar_calcfields_form_importer_import' ) ) {
	//add_action( 'sunnyjar_action_importer_import',	'sunnyjar_calcfields_form_importer_import', 10, 2 );
	function sunnyjar_calcfields_form_importer_import($importer, $action) {
		if ( $action == 'import_calcfields_form' ) {
			$importer->response['start_from_id'] = 0;
			$importer->import_dump('calcfields_form', esc_html__('Calculated Fields Form', 'sunnyjar'));
		}
	}
}

// Display import progress
if ( !function_exists( 'sunnyjar_calcfields_form_importer_import_fields' ) ) {
	//add_action( 'sunnyjar_action_importer_import_fields',	'sunnyjar_calcfields_form_importer_import_fields', 10, 1 );
	function sunnyjar_calcfields_form_importer_import_fields($importer) {
		$importer->show_importer_fields(array(
			'slug' => 'calcfields_form',
			'title' => esc_html__('Calculated Fields Form', 'sunnyjar')
			));
	}
}

// Export posts
if ( !function_exists( 'sunnyjar_calcfields_form_importer_export' ) ) {
	//add_action( 'sunnyjar_action_importer_export',	'sunnyjar_calcfields_form_importer_export', 10, 1 );
	function sunnyjar_calcfields_form_importer_export($importer) {
		sunnyjar_fpc(sunnyjar_get_file_dir('core/core.importer/export/calcfields_form.txt'), serialize( array(
			CP_CALCULATEDFIELDSF_FORMS_TABLE => $importer->export_dump(CP_CALCULATEDFIELDSF_FORMS_TABLE)
			) )
		);
	}
}

// Display exported data in the fields
if ( !function_exists( 'sunnyjar_calcfields_form_importer_export_fields' ) ) {
	//add_action( 'sunnyjar_action_importer_export_fields',	'sunnyjar_calcfields_form_importer_export_fields', 10, 1 );
	function sunnyjar_calcfields_form_importer_export_fields($importer) {
		$importer->show_exporter_fields(array(
			'slug' => 'calcfields_form',
			'title' => esc_html__('Calculated Fields Form', 'sunnyjar')
			));
	}
}


// Lists
//------------------------------------------------------------------------

// Return Calculated forms list list, prepended inherit (if need)
if ( !function_exists( 'sunnyjar_get_list_calcfields_form' ) ) {
	function sunnyjar_get_list_calcfields_form($prepend_inherit=false) {
		if (($list = sunnyjar_storage_get('list_calcfields_form'))=='') {
			$list = array();
			if (sunnyjar_exists_calcfields_form()) {
				global $wpdb;
				$rows = $wpdb->get_results( "SELECT id, form_name FROM " . esc_sql($wpdb->prefix . CP_CALCULATEDFIELDSF_FORMS_TABLE) );
				if (is_array($rows) && count($rows) > 0) {
					foreach ($rows as $row) {
						$list[$row->id] = $row->form_name;
					}
				}
			}
			$list = apply_filters('sunnyjar_filter_list_calcfields_form', $list);
			if (sunnyjar_get_theme_setting('use_list_cache')) sunnyjar_storage_set('list_calcfields_form', $list); 
		}
		return $prepend_inherit ? sunnyjar_array_merge(array('inherit' => esc_html__("Inherit", 'sunnyjar')), $list) : $list;
	}
}



// Shortcodes
//------------------------------------------------------------------------

// Register shortcode in the shortcodes list
if (!function_exists('sunnyjar_calcfields_form_reg_shortcodes')) {
	//add_filter('sunnyjar_action_shortcodes_list',	'sunnyjar_calcfields_form_reg_shortcodes');
	function sunnyjar_calcfields_form_reg_shortcodes() {
		if (sunnyjar_storage_isset('shortcodes')) {

			$forms_list = sunnyjar_get_list_calcfields_form();

			sunnyjar_sc_map_after( 'trx_button', 'CP_CALCULATED_FIELDS', array(
					"title" => esc_html__("Calculated fields form", 'sunnyjar'),
					"desc" => esc_html__("Insert calculated fields form", 'sunnyjar'),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"id" => array(
							"title" => esc_html__("Form ID", 'sunnyjar'),
							"desc" => esc_html__("Select Form to insert into current page", 'sunnyjar'),
							"value" => "",
							"size" => "medium",
							"options" => $forms_list,
							"type" => "select"
							)
						)
					)
			);
		}
	}
}


// Register shortcode in the VC shortcodes list
if (!function_exists('sunnyjar_calcfields_form_reg_shortcodes_vc')) {
	//add_filter('sunnyjar_action_shortcodes_list_vc',	'sunnyjar_calcfields_form_reg_shortcodes_vc');
	function sunnyjar_calcfields_form_reg_shortcodes_vc() {

		$forms_list = sunnyjar_get_list_calcfields_form();

		// Calculated fields form
		vc_map( array(
				"base" => "CP_CALCULATED_FIELDS",
				"name" => esc_html__("Calculated fields form", 'sunnyjar'),
				"description" => esc_html__("Insert calculated fields form", 'sunnyjar'),
				"category" => esc_html__('Content', 'sunnyjar'),
				'icon' => 'icon_trx_calcfields',
				"class" => "trx_sc_single trx_sc_calcfields",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "id",
						"heading" => esc_html__("Form ID", 'sunnyjar'),
						"description" => esc_html__("Select Form to insert into current page", 'sunnyjar'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($forms_list),
						"type" => "dropdown"
					)
				)
			) );
			
		class WPBakeryShortCode_Cp_Calculated_Fields extends SUNNYJAR_VC_ShortCodeSingle {}

	}
}
?>