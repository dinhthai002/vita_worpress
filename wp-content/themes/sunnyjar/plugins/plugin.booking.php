<?php
/* Booking Calendar support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('sunnyjar_booking_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_booking_theme_setup', 1 );
	function sunnyjar_booking_theme_setup() {
		// Register shortcode in the shortcodes list
		if (sunnyjar_exists_booking()) {
			add_action('sunnyjar_action_add_styles',					'sunnyjar_booking_frontend_scripts');
			add_action('sunnyjar_action_shortcodes_list',				'sunnyjar_booking_reg_shortcodes');
			if (function_exists('sunnyjar_exists_visual_composer') && sunnyjar_exists_visual_composer())
				add_action('sunnyjar_action_shortcodes_list_vc',		'sunnyjar_booking_reg_shortcodes_vc');
			if (is_admin()) {
				add_filter( 'sunnyjar_filter_importer_options',			'sunnyjar_booking_importer_set_options' );
				add_action( 'sunnyjar_action_importer_params',			'sunnyjar_booking_importer_show_params', 10, 1 );
				add_action( 'sunnyjar_action_importer_import',			'sunnyjar_booking_importer_import', 10, 2 );
				add_action( 'sunnyjar_action_importer_import_fields',	'sunnyjar_booking_importer_import_fields', 10, 1 );
				add_action( 'sunnyjar_action_importer_export',			'sunnyjar_booking_importer_export', 10, 1 );
				add_action( 'sunnyjar_action_importer_export_fields',	'sunnyjar_booking_importer_export_fields', 10, 1 );
			}
		}
		if (is_admin()) {
			add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_booking_importer_required_plugins', 10, 2);
			add_filter( 'sunnyjar_filter_required_plugins',				'sunnyjar_booking_required_plugins' );
		}
	}
}


// Check if Booking Calendar installed and activated
if ( !function_exists( 'sunnyjar_exists_booking' ) ) {
	function sunnyjar_exists_booking() {
		return function_exists('wp_booking_start_session');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'sunnyjar_booking_required_plugins' ) ) {
	//add_filter('sunnyjar_filter_required_plugins',	'sunnyjar_booking_required_plugins');
	function sunnyjar_booking_required_plugins($list=array()) {
		if (in_array('booking', sunnyjar_storage_get('required_plugins'))) {
			$path = sunnyjar_get_file_dir('plugins/install/wp-booking-calendar.zip');
			if (file_exists($path)) {
				$list[] = array(
					'name' 		=> esc_html__('Booking Calendar', 'sunnyjar'),
					'slug' 		=> 'wp-booking-calendar',
					'source'	=> $path,
					'required' 	=> false
					);
			}
		}
		return $list;
	}
}

// Enqueue custom styles
if ( !function_exists( 'sunnyjar_booking_frontend_scripts' ) ) {
	//add_action( 'sunnyjar_action_add_styles', 'sunnyjar_booking_frontend_scripts' );
	function sunnyjar_booking_frontend_scripts() {
		if (file_exists(sunnyjar_get_file_dir('css/plugin.booking.css')))
			sunnyjar_enqueue_style( 'sunnyjar-plugin.booking-style',  sunnyjar_get_file_url('css/plugin.booking.css'), array(), null );
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check in the required plugins
if ( !function_exists( 'sunnyjar_booking_importer_required_plugins' ) ) {
	//add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_booking_importer_required_plugins', 10, 2);
	function sunnyjar_booking_importer_required_plugins($not_installed='', $list='') {
		if (sunnyjar_strpos($list, 'booking')!==false && !sunnyjar_exists_booking() )
			$not_installed .= '<br>' . esc_html__('Booking Calendar', 'sunnyjar');
		return $not_installed;
	}
}

// Set options for one-click importer
if ( !function_exists( 'sunnyjar_booking_importer_set_options' ) ) {
	//add_filter( 'sunnyjar_filter_importer_options',	'sunnyjar_booking_importer_set_options', 10, 1 );
	function sunnyjar_booking_importer_set_options($options=array()) {
		if ( in_array('booking', sunnyjar_storage_get('required_plugins')) && sunnyjar_exists_booking() ) {
			if (is_array($options['files']) && count($options['files']) > 0) {
				foreach ($options['files'] as $k => $v) {
					$options['files'][$k]['file_with_booking'] = str_replace('name.ext', 'booking.txt', $v['file_with_']);
				}
			}
		}
		return $options;
	}
}

// Add checkbox to the one-click importer
if ( !function_exists( 'sunnyjar_booking_importer_show_params' ) ) {
	//add_action( 'sunnyjar_action_importer_params',	'sunnyjar_booking_importer_show_params', 10, 1 );
	function sunnyjar_booking_importer_show_params($importer) {
		$importer->show_importer_params(array(
			'slug' => 'booking',
			'title' => esc_html__('Import Booking Calendar', 'sunnyjar'),
			'part' => 0
			));
	}
}

// Import posts
if ( !function_exists( 'sunnyjar_booking_importer_import' ) ) {
	//add_action( 'sunnyjar_action_importer_import',	'sunnyjar_booking_importer_import', 10, 2 );
	function sunnyjar_booking_importer_import($importer, $action) {
		if ( $action == 'import_booking' ) {
			$importer->response['start_from_id'] = 0;
			$importer->import_dump('booking', esc_html__('Booking Calendar', 'sunnyjar'));
		}
	}
}

// Display import progress
if ( !function_exists( 'sunnyjar_booking_importer_import_fields' ) ) {
	//add_action( 'sunnyjar_action_importer_import_fields',	'sunnyjar_booking_importer_import_fields', 10, 1 );
	function sunnyjar_booking_importer_import_fields($importer) {
		$importer->show_importer_fields(array(
			'slug' => 'booking',
			'title' => esc_html__('Booking Calendar', 'sunnyjar')
			));
	}
}

// Export posts
if ( !function_exists( 'sunnyjar_booking_importer_export' ) ) {
	//add_action( 'sunnyjar_action_importer_export',	'sunnyjar_booking_importer_export', 10, 1 );
	function sunnyjar_booking_importer_export($importer) {
		sunnyjar_fpc(sunnyjar_get_file_dir('core/core.importer/export/booking.txt'), serialize( array(
			"booking_calendars"		=> $importer->export_dump("booking_calendars"),
			"booking_categories"	=> $importer->export_dump("booking_categories"),
            "booking_config"		=> $importer->export_dump("booking_config"),
            "booking_reservation"	=> $importer->export_dump("booking_reservation"),
            "booking_slots"			=> $importer->export_dump("booking_slots")
            ) )
        );
	}
}

// Display exported data in the fields
if ( !function_exists( 'sunnyjar_booking_importer_export_fields' ) ) {
	//add_action( 'sunnyjar_action_importer_export_fields',	'sunnyjar_booking_importer_export_fields', 10, 1 );
	function sunnyjar_booking_importer_export_fields($importer) {
		$importer->show_exporter_fields(array(
			'slug' => 'booking',
			'title' => esc_html__('Booking', 'sunnyjar')
			));
	}
}


// Lists
//------------------------------------------------------------------------

// Return Booking categories list, prepended inherit (if need)
if ( !function_exists( 'sunnyjar_get_list_booking_categories' ) ) {
	function sunnyjar_get_list_booking_categories($prepend_inherit=false) {
		if (($list = sunnyjar_storage_get('list_booking_cats'))=='') {
			$list = array();
			if (sunnyjar_exists_booking()) {
				global $wpdb;
				$rows = $wpdb->get_results( "SELECT category_id, category_name FROM " . esc_sql($wpdb->prefix . 'booking_categories') );
				if (is_array($rows) && count($rows) > 0) {
					foreach ($rows as $row) {
						$list[$row->category_id] = $row->category_name;
					}
				}
			}
			$list = apply_filters('sunnyjar_filter_list_booking_categories', $list);
			if (sunnyjar_get_theme_setting('use_list_cache')) sunnyjar_storage_set('list_booking_cats', $list); 
		}
		return $prepend_inherit ? sunnyjar_array_merge(array('inherit' => esc_html__("Inherit", 'sunnyjar')), $list) : $list;
	}
}

// Return Booking calendars list, prepended inherit (if need)
if ( !function_exists( 'sunnyjar_get_list_booking_calendars' ) ) {
	function sunnyjar_get_list_booking_calendars($prepend_inherit=false) {
		if (($list = sunnyjar_storage_get('list_booking_calendars'))=='') {
			$list = array();
			if (sunnyjar_exists_booking()) {
				global $wpdb;
				$rows = $wpdb->get_results( "SELECT cl.calendar_id, cl.calendar_title, ct.category_name FROM " . esc_sql($wpdb->prefix . 'booking_calendars') . " AS cl"
												. " INNER JOIN " . esc_sql($wpdb->prefix . 'booking_categories') . " AS ct ON cl.category_id=ct.category_id"
										);
				if (is_array($rows) && count($rows) > 0) {
					foreach ($rows as $row) {
						$list[$row->calendar_id] = $row->calendar_title . ' (' . $row->category_name . ')';
					}
				}
			}
			$list = apply_filters('sunnyjar_filter_list_booking_calendars', $list);
			if (sunnyjar_get_theme_setting('use_list_cache')) sunnyjar_storage_set('list_booking_calendars', $list); 
		}
		return $prepend_inherit ? sunnyjar_array_merge(array('inherit' => esc_html__("Inherit", 'sunnyjar')), $list) : $list;
	}
}



// Shortcodes
//------------------------------------------------------------------------

// Register shortcode in the shortcodes list
if (!function_exists('sunnyjar_booking_reg_shortcodes')) {
	//add_filter('sunnyjar_action_shortcodes_list',	'sunnyjar_booking_reg_shortcodes');
	function sunnyjar_booking_reg_shortcodes() {
		if (sunnyjar_storage_isset('shortcodes')) {

			$booking_cats = sunnyjar_get_list_booking_categories();
			$booking_cals = sunnyjar_get_list_booking_calendars();

			sunnyjar_sc_map('wp_booking_calendar', array(
				"title" => esc_html__("Booking Calendar", 'sunnyjar'),
				"desc" => esc_html__("Insert Booking calendar", 'sunnyjar'),
				"decorate" => true,
				"container" => false,
				"params" => array(
					"category_id" => array(
						"title" => esc_html__("Category", 'sunnyjar'),
						"desc" => esc_html__("Select booking category", 'sunnyjar'),
						"value" => "",
						"type" => "select",
						"options" => sunnyjar_array_merge(array(0 => esc_html__('- Select category -', 'sunnyjar')), $booking_cats)
					),
					"calendar_id" => array(
						"title" => esc_html__("Calendar", 'sunnyjar'),
						"desc" => esc_html__("or select booking calendar (id category is empty)", 'sunnyjar'),
						"dependency" => array(
							'category_id' => array('empty', '0')
						),
						"value" => "",
						"type" => "select",
						"options" => sunnyjar_array_merge(array(0 => esc_html__('- Select calendar -', 'sunnyjar')), $booking_cals)
					)
				)
			));
		}
	}
}


// Register shortcode in the VC shortcodes list
if (!function_exists('sunnyjar_booking_reg_shortcodes_vc')) {
	//add_filter('sunnyjar_action_shortcodes_list_vc',	'sunnyjar_booking_reg_shortcodes_vc');
	function sunnyjar_booking_reg_shortcodes_vc() {

		$booking_cats = sunnyjar_get_list_booking_categories();
		$booking_cals = sunnyjar_get_list_booking_calendars();


		// Sunnyjar Donations form
		vc_map( array(
				"base" => "wp_booking_calendar",
				"name" => esc_html__("Booking Calendar", 'sunnyjar'),
				"description" => esc_html__("Insert Booking calendar", 'sunnyjar'),
				"category" => esc_html__('Content', 'sunnyjar'),
				'icon' => 'icon_trx_booking',
				"class" => "trx_sc_single trx_sc_booking",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "category_id",
						"heading" => esc_html__("Category", 'sunnyjar'),
						"description" => esc_html__("Select Booking category", 'sunnyjar'),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(sunnyjar_array_merge(array(0 => esc_html__('- Select category -', 'sunnyjar')), $booking_cats)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "calendar_id",
						"heading" => esc_html__("Calendar", 'sunnyjar'),
						"description" => esc_html__("Select Booking calendar", 'sunnyjar'),
						"admin_label" => true,
						'dependency' => array(
							'element' => 'category_id',
							'is_empty' => true
						),
						"class" => "",
						"value" => array_flip(sunnyjar_array_merge(array(0 => esc_html__('- Select calendar -', 'sunnyjar')), $booking_cals)),
						"type" => "dropdown"
					)
				)
			) );
			
		class WPBakeryShortCode_Wp_Booking_Calendar extends SUNNYJAR_VC_ShortCodeSingle {}

	}
}
?>