<?php
/* Visual Composer support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('sunnyjar_vc_extensions_bundle_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_vc_extensions_bundle_theme_setup', 1 );
	function sunnyjar_vc_extensions_bundle_theme_setup() {
		if (is_admin()) {
			add_filter( 'sunnyjar_filter_importer_required_plugins',		'sunnyjar_vc_extensions_bundle_importer_required_plugins', 10, 2 );
			add_filter( 'sunnyjar_filter_required_plugins',					'sunnyjar_vc_extensions_bundle_required_plugins' );
		}
	}
}

// Check if Visual Composer installed and activated
if ( !function_exists( 'sunnyjar_exists_vc_extensions_bundle' ) ) {
	function sunnyjar_exists_vc_extensions_bundle() {
		return class_exists('VC_Extensions_CQBundle');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'sunnyjar_vc_extensions_bundle_required_plugins' ) ) {
	//add_filter('sunnyjar_filter_required_plugins',	'sunnyjar_vc_extensions_bundle_required_plugins');
	function sunnyjar_vc_extensions_bundle_required_plugins($list=array()) {
		if (in_array('vc_extensions_bundle', sunnyjar_storage_get('required_plugins'))) {
			$path = sunnyjar_get_file_dir('plugins/install/vc-extensions-bundle.zip');
			if (file_exists($path)) {
				$list[] = array(
					'name' 		=> 'Visual Composer Extensions All In One',
					'slug' 		=> 'vc-extensions-bundle',
					'source'	=> $path,
					'required' 	=> false
				);
			}
		}
		return $list;
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check VC in the required plugins
if ( !function_exists( 'sunnyjar_vc_extensions_bundle_importer_required_plugins' ) ) {
	//add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_vc_extensions_bundle_importer_required_plugins', 10, 2 );
	function sunnyjar_vc_extensions_bundle_importer_required_plugins($not_installed='', $list='') {
		//if (in_array('visual_composer', sunnyjar_storage_get('required_plugins')) && !sunnyjar_exists_vc_extensions_bundle() && sunnyjar_get_value_gp('data_type')=='vc' )
		if (!sunnyjar_exists_vc_extensions_bundle() )		// && sunnyjar_strpos($list, 'visual_composer')!==false
			$not_installed .= '<br>Visual Composer Extensions All In One';
		return $not_installed;
	}
}
?>