<?php
/* Mega Main Menu support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('sunnyjar_megamenu_theme_setup')) {
	add_action( 'sunnyjar_action_before_init_theme', 'sunnyjar_megamenu_theme_setup', 1 );
	function sunnyjar_megamenu_theme_setup() {
		if (sunnyjar_exists_megamenu()) {
			if (is_admin()) {
				add_filter( 'sunnyjar_filter_importer_options',				'sunnyjar_megamenu_importer_set_options' );
			}
		}
		if (is_admin()) {
			add_filter( 'sunnyjar_filter_importer_required_plugins',		'sunnyjar_megamenu_importer_required_plugins', 10, 2 );
			add_filter( 'sunnyjar_filter_required_plugins',					'sunnyjar_megamenu_required_plugins' );
		}
	}
}

// Check if MegaMenu installed and activated
if ( !function_exists( 'sunnyjar_exists_megamenu' ) ) {
	function sunnyjar_exists_megamenu() {
		return class_exists('mega_main_init');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'sunnyjar_megamenu_required_plugins' ) ) {
	//add_filter('sunnyjar_filter_required_plugins',	'sunnyjar_megamenu_required_plugins');
	function sunnyjar_megamenu_required_plugins($list=array()) {
		if (in_array('mega_main_menu', sunnyjar_storage_get('required_plugins'))) {
			$path = sunnyjar_get_file_dir('plugins/install/mega_main_menu.zip');
			if (file_exists($path)) {
				$list[] = array(
					'name' 		=> esc_html__('Mega Main Menu', 'sunnyjar'),
					'slug' 		=> 'mega_main_menu',
					'source'	=> $path,
					'required' 	=> false
				);
			}
		}
		return $list;
	}
}



// One-click import support
//------------------------------------------------------------------------

// Check Mega Menu in the required plugins
if ( !function_exists( 'sunnyjar_megamenu_importer_required_plugins' ) ) {
	//add_filter( 'sunnyjar_filter_importer_required_plugins',	'sunnyjar_megamenu_importer_required_plugins', 10, 2 );
	function sunnyjar_megamenu_importer_required_plugins($not_installed='', $list='') {
		if (sunnyjar_strpos($list, 'mega_main_menu')!==false && !sunnyjar_exists_megamenu())
			$not_installed .= '<br>' . esc_html__('Mega Main Menu', 'sunnyjar');
		return $not_installed;
	}
}

// Set options for one-click importer
if ( !function_exists( 'sunnyjar_megamenu_importer_set_options' ) ) {
	//add_filter( 'sunnyjar_filter_importer_options',	'sunnyjar_megamenu_importer_set_options' );
	function sunnyjar_megamenu_importer_set_options($options=array()) {
		if ( in_array('mega_main_menu', sunnyjar_storage_get('required_plugins')) && sunnyjar_exists_megamenu() ) {
			// Add slugs to export options for this plugin
			$options['additional_options'][] = 'mega_main_menu_options';

		}
		return $options;
	}
}
?>